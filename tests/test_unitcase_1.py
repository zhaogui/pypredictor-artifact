#!/usr/bin/python
# _*_ coding:utf-8 _*_


class C:
    pass


def foo(a, b):
    '''
    a: [<anyvalue>]
    b: ['s']
    '''
    y = C()
    if a > 1:
        y.f = 'S'
    else:
        y.f = 2
    if isinstance(b, int):
        z = 'S'
    else:
        z = y.f
    w = z.lower()
    return w

print foo(2, 1)
