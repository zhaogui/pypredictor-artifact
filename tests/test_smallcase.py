#!/usr/bin/python
# _*_ coding:utf-8 _*_

from pypredictor.runs import run


# sname = 'test_pred_if'
sname = 'test_unitcase_1'
tmpdir = 'tmp'
scriptfilename = sname + '.py'
#timesuffix = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
recordfilename = tmpdir + '/' + sname + '-all-modules.txt'
resultfilename = tmpdir + '/' + sname + '-result.txt'
tracefilename = tmpdir + '/' + sname + '-trace.txt'
entryfuncname = 'foo'


run.runtracephase(projname=sname, scriptfile=scriptfilename,
                  entryfunc=entryfuncname, recordfile=recordfilename, 
                  resultfile=resultfilename, tmpdir=tmpdir)

run.runpredictphase(projname=sname, useslice=True)

