#!/usr/bin/python
# _*_ coding:utf-8 _*_

import __builtin__
import logging
import tracenodes as new
from ast import *


logger = logging.getLogger('pytracer')


class ExtAnalysisFrame(object):

    def __init__(self, aframe):
        self.aframe = aframe
        self.name2racyid = {}

    @property
    def filename(self):
        return self.aframe.filename

    @property
    def funcname(self):
        return self.aframe.funcname

    def oldlineno(self, nlno):
        return self.aframe.oldlineno(nlno)

    def new_id(self, name, value):
        assert isinstance(value, new.HeapObject), \
            'value must be instance of HeapObject'
        if name is None:
            name = self.aframe.new_name()
            nid = new.Id(name, value)
        else:
            nid = new.Id(self.aframe.new_name(name), value)
        self.name2racyid[name] = nid
        return nid

    def get_racyid(self, name):
        return self.name2racyid.get(name)

    def liveobject(self, name):
        if name in self.name2racyid:
            return self.name2racyid[name].value.liveobj
        return self.aframe.liveobject(name)


class ExtTraceMakerMixIn(object):

    def liveobject(self, name):
        return self.extaframe.liveobject(name)

    def new_tmpid(self, value):
        if not isinstance(value, new.HeapObject):
            value = new.HeapObject(value)
        alloc = new.Allocate(self.new_id(None, value), value)
        self.append_trace(alloc)
        return alloc.target

    def new_id(self, name, value):
        return self.extaframe.new_id(name, value)

    def get_id(self, name):
        racyid = self.extaframe.get_racyid(name)
        if racyid is None:
            racyid = self.extaframe.aframe.get_racyid(name)
        if racyid is None:
            value = new.HeapObject(self.liveobject(name))
            racyid = self.new_id(name, value)
            self.append_trace(new.Allocate(racyid, value))
        return racyid

    def new_allocate(self, target, value):
        return new.Allocate(self.new_id(target, value), value)

    def new_assign(self, target, value):
        val_id = self.get_id(value)
        return new.Assign(self.new_id(target, val_id.value), val_id)


class AssignTraceMakerMixIn(object):

    def default_make_assign(self, *_):
        return False

    def make_assign(self, target, value):   # must not be the target of a non-C function call
        method = 'make_{val}_assign_{tgt}'.format(val=value.__class__.__name__.lower(),
                                                  tgt=target.__class__.__name__.lower())
        maker = getattr(self, method, self.default_make_assign)
        return maker(target, value)

    def make_num_assign_name(self, target, value):
        self.append_trace(self.new_allocate(target.id, new.HeapObject(value.n)))
        return True

    def make_str_assign_name(self, target, value):
        self.append_trace(self.new_allocate(target.id, new.HeapObject(value.s)))
        return True

    def make_tuple_assign_name(self, target, value):
        try:
            val_ho = new.HeapObject(tuple(self.liveobject(elt.id) for elt in value.elts))
        except ValueError:
            return False
        self.append_trace(new.Allocate(self.new_id(target.id, val_ho), val_ho))
        for i, elt in enumerate(value.elts):
            self.append_trace(new.SubStore(self.get_id(target.id),
                                           self.new_tmpid(i),
                                           self.get_id(elt.id)))
        return True

    def make_list_assign_name(self, target, value):
        try:
            val_ho = new.HeapObject([self.liveobject(elt.id) for elt in value.elts])
        except ValueError:
            return False
        self.append_trace(new.Allocate(self.new_id(target.id, val_ho), val_ho))
        for i, elt in enumerate(value.elts):
            self.append_trace(new.SubStore(self.get_id(target.id),
                                           self.new_tmpid(i),
                                           self.get_id(elt.id)))
        return True

    def make_set_assign_name(self, target, value):
        try:
            val_ho = new.HeapObject({self.liveobject(elt.id) for elt in value.elts})
        except ValueError:
            return False
        self.append_trace(new.Allocate(self.new_id(target.id, val_ho), val_ho))
        return True

    def make_dict_assign_name(self, target, value):
        try:
            val_ho = new.HeapObject({self.liveobject(k.id): self.liveobject(v.id)
                                     for k, v in zip(value.keys, value.values)})
        except ValueError:
            return False
        self.append_trace(new.Allocate(self.new_id(target.id, val_ho), val_ho))
        for k, v in zip(value.keys, value.values):
            self.append_trace(new.SubStore(self.get_id(target.id),
                                           self.get_id(k.id),
                                           self.get_id(v.id)))
        return True

    def make_name_assign_name(self, target, value):
        if value.id == 'True':
            val_ho = new.HeapObject(True)
            self.append_trace(self.new_allocate(target.id, val_ho))
        elif value.id == 'False':
            val_ho = new.HeapObject(False)
            self.append_trace(self.new_allocate(target.id, val_ho))
        elif value.id == 'None':
            val_ho = new.HeapObject(None)
            self.append_trace(self.new_allocate(target.id, val_ho))
        else:
            try:
                assign = self.new_assign(target.id, value.id)
            except ValueError:
                return False
            self.append_trace(assign)
        return True

    def make_attribute_assign_name(self, target, value):
        try:
            val_id = self.get_id(value.value.id)
        except ValueError:
            return False
        try:
            val = getattr(val_id.value.liveobj, value.attr)
            self.append_trace(new.AttrLoad(self.new_id(target.id, new.HeapObject(val)),
                                           val_id, value.attr))
            return True
        except AttributeError:
            self.append_trace(new.AttrLoad(self.new_id(target.id, new.HeapObject(None)),
                                           val_id, value.attr))
            return False

    def make_subscript_assign_name(self, target, value):
        if isinstance(value.slice, Index):
            try:
                idx_id = self.get_id(value.slice.value.id)
            except ValueError:
                return False
            idx_val = idx_id.value.liveobj
            if not isinstance(idx_val, __builtin__.slice):
                try:
                    val_id = self.get_id(value.value.id)
                except ValueError:
                    return False
                val_val = val_id.value.liveobj
                try:
                    tgt_val = val_val[idx_val]
                    self.append_trace(new.SubLoad(self.new_id(target.id, new.HeapObject(tgt_val)),
                                      val_id, idx_id))
                    return True
                except (TypeError, KeyError, IndexError):
                    return False
        return False

    def make_call_assign_name(self, target, value):
        return False

    def make_binop_assign_name(self, target, value):
        return False

    def make_unaryop_assign_name(self, target, value):
        return False

    def make_compare_assign_name(self, target, value):
        return False

    def make_name_assign_attribute(self, target, value):
        # self.append_trace(new.AttrStore(self.get_id(target.value.id),
        #                                 target.attr, self.get_id(value.id)))
        # return True
        try:
            loc_id = self.get_id(target.value.id)
        except ValueError:
            return False
        try:
            val_id = self.get_id(value.id)
            self.append_trace(new.AttrStore(loc_id, target.attr, val_id))
            return True
        except ValueError:
            return False

    def make_name_assign_subscript(self, target, value):
        # if isinstance(target.slice, Index):
        #     idx = target.slice.value
        #     idx_val = self.liveobject(idx.id)
        #     if not isinstance(idx_val, __builtin__.slice):
        #         self.append_trace(new.SubStore(self.get_id(target.value.id),
        #                                        self.get_id(idx.id),
        #                                        self.get_id(value.id)))
        #         return True
        return False


class ExtTraceMaker(ExtTraceMakerMixIn, AssignTraceMakerMixIn):

    def __init__(self, aframe):
        self.extaframe = ExtAnalysisFrame(aframe)
        self.allresults = []
        self.results = []
        self._isdebug = logger.isEnabledFor(logging.DEBUG)

    def append_trace(self, trace):
        if trace:
            self.results.append(trace)

    def make(self, abranch, stmts):
        self.allresults = []
        for node in stmts:
            if self._isdebug:
                self.verbosestmt('extline', node.lineno, node,
                                 self.extaframe.funcname,
                                 self.extaframe.filename)
            self.results = []
            # make trace for current stmt
            method = 'make_' + node.__class__.__name__.lower()
            maker = getattr(self, method, self.default_make)
            cont = maker(node)
            self.bindinfo(self.results, node)
            if self._isdebug:
                self.verbosetrace(self.results)
            self.allresults.extend(self.results)
            if not cont:
                # means it cannot be fully resolved
                abranch.resolved = False
                abranch.set_local_extb_name2racyid(self.extaframe.name2racyid)
                return self.allresults
        else:
            # means it can be fully resolved
            abranch.resolved = True
            abranch.set_local_extb_name2racyid(self.extaframe.name2racyid)
            return self.allresults

    def bindinfo(self, events, node):
        for event in events:
            event.fromexec = False
            event.setinfo(self.extaframe.filename,
                          node, node.lineno,
                          self.extaframe.oldlineno(node.lineno))

    def default_make(self, node):
        return False

    def make_assign(self, node):
        return super(ExtTraceMaker, self).make_assign(node.targets[0], node.value)

    def verbosestmt(self, event, lineno, stmt, funcname, filename):
        logger.debug('[{event}] [{lno}] in <{funcname}>'
                     'in file <{filename}>'.format(event=event.upper(), lno=lineno,
                                                   funcname=funcname,
                                                   filename=filename))
        logger.debug('-----[STMT] ' + (dump(stmt) if stmt is not None else 'None'))

    def verbosetrace(self, trace):
        logger.debug('=====[TRACE] ' + ', '.join(repr(t) for t in trace))
