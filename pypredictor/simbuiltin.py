#!/usr/bin/python
# _*_ coding:utf-8 _*_

from __builtin__ import list, dict, tuple, set

# import threading as _threading
#
# _lock = _threading.Lock()


class list(list):
    """
    list() -> new empty list
    list(iterable) -> new list initialized from iterable's items
    """
    def count(self, value): # real signature unknown; restored from __doc__
        """ L.count(value) -> integer -- return number of occurrences of value """
        counter = 0
        for x in self:
            if x == value:
                counter += 1
        return counter

    def extend(self, iterable): # real signature unknown; restored from __doc__
        """ L.extend(iterable) -- extend list by appending elements from the iterable """
        for x in iterable:
            self.append(x)

    def index(self, value, start=0, stop=None): # real signature unknown; restored from __doc__
        """
        L.index(value, [start, [stop]]) -> integer -- return first index of value.
        Raises ValueError if the value is not present.
        """
        l = self[start:stop] if stop is not None else self[start:]
        i = start
        for x in l:
            if x == value:
                return i
            i += 1
        else:
            raise ValueError(str(value) + ' is not in the list')

    def insert(self, index, p_object): # real signature unknown; restored from __doc__
        """ L.insert(index, object) -- insert object before index """
        if index >= len(self):
            self.append(p_object)
            return
        self.append(self[-1])
        for i in reversed(range(index+1, len(self)-1)):
            self[i] = self[i-1]
        self[index] = p_object

    def pop(self, index=None): # real signature unknown; restored from __doc__
        """
        L.pop([index]) -> item -- remove and return item at index (default last).
        Raises IndexError if list is empty or index is out of range.
        """
        if index is None:
            return super(list, self).pop()
        else:
            for i in range(index, len(self)-1):
                self[i] = self[i+1]
        self.pop()

    def remove(self, value): # real signature unknown; restored from __doc__
        """
        L.remove(value) -- remove first occurrence of value.
        Raises ValueError if the value is not present.
        """
        if value not in self:
            raise ValueError
        idx = self.index(value)
        self.pop(idx)

    def reverse(self): # real signature unknown; restored from __doc__
        """ L.reverse() -- reverse *IN PLACE* """
        k = len(self) / 2 + 1
        for i in range(0, k):
            self[i], self[-i] = self[-i], self[i]

    def sort(self, cmp=None, key=None, reverse=False): # real signature unknown; restored from __doc__
        """
        L.sort(cmp=None, key=None, reverse=False) -- stable sort *IN PLACE*;
        cmp(x, y) -> -1, 0, 1
        """
        if cmp is None:
            cmp = lambda a, b: (a - b)
        for i in range(1, len(self)):
            j = i
            while (j > 0) and cmp(self[j], self[j-1]) and (not reversed):
                self[j], self[j-1] = self[j-1], self[j]
                j -= 1
    #
    # def __add__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__add__(y) <==> x+y """
    #     pass
    #
    # def __contains__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__contains__(y) <==> y in x """
    #     pass

    def __delitem__(self, y):  # real signature unknown; restored from __doc__
        """ x.__delitem__(y) <==> del x[y] """
        if isinstance(y, int):
            l = len(self)
            y = l + y if y < 0 else y
            if y < 0 or y >= l:
                raise IndexError('list index out of range')
            for i in range(y+1, len(self)):
                self[i-1] = self[i]
            self.pop()
        elif isinstance(y, slice):
            l = len(self)
            i = y.start if y.start is not None else 0
            j = y.stop if y.stop is not None else l
            s = y.step if y.step is not None else 1

            i = (0 if l + i < 0 else l + i) if i < 0 else (i if i < l else l)
            j = (0 if l + j < 0 else l + j) if j < 0 else (j if j < l else l)
            f = range(i, j, s)
            if not f:
                return
            m = min(i, j)
            new = [x for idx, x in zip(range(0, l), self) if (idx >= m) and (idx not in f)]
            for idx in range(m, l):
                self.pop()
            self.extend(new)


    def __delslice__(self, i, j): # real signature unknown; restored from __doc__
        """
        x.__delslice__(i, j) <==> del x[i:j]

                   Use of negative indices is not supported.
        """
        self.__delitem__(slice(i, j, 1))

    #
    # def __eq__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__eq__(y) <==> x==y """
    #     pass
    #
    # def __getattribute__(self, name): # real signature unknown; restored from __doc__
    #     """ x.__getattribute__('name') <==> x.name """
    #     pass
    #
    # def __getitem__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__getitem__(y) <==> x[y] """
    #     pass
    #
    def __getslice__(self, i, j): # real signature unknown; restored from __doc__
        """
        x.__getslice__(i, j) <==> x[i:j]

                   Use of negative indices is not supported.
        """
        #_lock.acquire()
        l = len(self)
        i = (0 if l+i < 0 else l + i) if i < 0 else i   # i = l if i < 0 else i
        j = l if l < j else j
        re = [self[idx] for idx in range(i, j)]
        #_lock.release()
        return re
    #
    # def __ge__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ge__(y) <==> x>=y """
    #     pass
    #
    # def __gt__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__gt__(y) <==> x>y """
    #     pass
    #
    # def __iadd__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__iadd__(y) <==> x+=y """
    #     pass
    #
    # def __imul__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__imul__(y) <==> x*=y """
    #     pass

    def __init__(self, seq=()): # known special case of list.__init__
        """
        list() -> new empty list
        list(iterable) -> new list initialized from iterable's items
        # (copied from class doc)
        """
        for x in seq:
            self.append(x)

    # def __iter__(self): # real signature unknown; restored from __doc__
    #     """ x.__iter__() <==> iter(x) """
    #     pass
    #
    # def __len__(self): # real signature unknown; restored from __doc__
    #     """ x.__len__() <==> len(x) """
    #     pass
    #
    # def __le__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__le__(y) <==> x<=y """
    #     pass
    #
    # def __lt__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__lt__(y) <==> x<y """
    #     pass
    #
    # def __mul__(self, n): # real signature unknown; restored from __doc__
    #     """ x.__mul__(n) <==> x*n """
    #     pass
    #
    # def __ne__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ne__(y) <==> x!=y """
    #     pass
    #
    # def __reversed__(self): # real signature unknown; restored from __doc__
    #     """ L.__reversed__() -- return a reverse iterator over the list """
    #     pass
    #
    # def __rmul__(self, n): # real signature unknown; restored from __doc__
    #     """ x.__rmul__(n) <==> n*x """
    #     l = list(self)
    #     i = 0
    #     while i < n:
    #         self.extend(l)
    #
    # def __setslice__(self, i, j, y): # real signature unknown; restored from __doc__
    #     """
    #     x.__setslice__(i, j, y) <==> x[i:j]=y
    #
    #                Use  of negative indices is not supported.
    #     """
    #     pass


class dict(dict):
    """
    dict() -> new empty dictionary
    dict(mapping) -> new dictionary initialized from a mapping object's
        (key, value) pairs
    dict(iterable) -> new dictionary initialized as if via:
        d = {}
        for k, v in iterable:
            d[k] = v
    dict(**kwargs) -> new dictionary initialized with the name=value pairs
        in the keyword argument list.  For example:  dict(one=1, two=2)
    """
    # def clear(self): # real signature unknown; restored from __doc__
    #     """ D.clear() -> None.  Remove all items from D. """
    #     pass

    def copy(self): # real signature unknown; restored from __doc__
        """ D.copy() -> a shallow copy of D """
        return dict(self)

    @staticmethod # known case
    def fromkeys(S, v=None): # real signature unknown; restored from __doc__
        """
        dict.fromkeys(S[,v]) -> New dict with keys from S and values equal to v.
        v defaults to None.
        """
        return {k: v for k in S}

    def get(self, k, d=None): # real signature unknown; restored from __doc__
        """ D.get(k[,d]) -> D[k] if k in D, else d.  d defaults to None. """
        return self[k] if k in self else d

    # def has_key(self, k): # real signature unknown; restored from __doc__
    #     """ D.has_key(k) -> True if D has a key k, else False """
    #     return False

    def items(self): # real signature unknown; restored from __doc__
        """ D.items() -> list of D's (key, value) pairs, as 2-tuples """
        return [(k, self[k]) for k in self]

    # def iteritems(self): # real signature unknown; restored from __doc__
    #     """ D.iteritems() -> an iterator over the (key, value) items of D """
    #     pass

    # def iterkeys(self): # real signature unknown; restored from __doc__
    #     """ D.iterkeys() -> an iterator over the keys of D """
    #     pass

    # def itervalues(self): # real signature unknown; restored from __doc__
    #     """ D.itervalues() -> an iterator over the values of D """
    #     pass

    def keys(self): # real signature unknown; restored from __doc__
        """ D.keys() -> list of D's keys """
        return [k for k in self]

    # def pop(self, k, d=None): # real signature unknown; restored from __doc__
    #     """
    #     D.pop(k[,d]) -> v, remove specified key and return the corresponding value.
    #     If key is not found, d is returned if given, otherwise KeyError is raised
    #     """
    #     pass

    # def popitem(self): # real signature unknown; restored from __doc__
    #     """
    #     D.popitem() -> (k, v), remove and return some (key, value) pair as a
    #     2-tuple; but raise KeyError if D is empty.
    #     """
    #     pass

    # def setdefault(self, k, d=None): # real signature unknown; restored from __doc__
    #     """ D.setdefault(k[,d]) -> D.get(k,d), also set D[k]=d if k not in D """
    #     pass

    def __update_impl(self, E=None, **F):
        if E is not None:
            if hasattr(E, 'keys'):
                for k in E:
                    self[k] = E[k]
            else:
                for k, v in E:
                    self[k] = v
        for k in F:
            self[k] = F[k]

    def update(self, E=None, **F): # known special case of dict.update
        """
        D.update([E, ]**F) -> None.  Update D from dict/iterable E and F.
        If E present and has a .keys() method, does:     for k in E: D[k] = E[k]
        If E present and lacks .keys() method, does:     for (k, v) in E: D[k] = v
        In either case, this is followed by: for k in F: D[k] = F[k]
        """
        self.__update_impl(E, **F)

    def values(self): # real signature unknown; restored from __doc__
        """ D.values() -> list of D's values """
        return [self[k] for k in self]

    # def viewitems(self): # real signature unknown; restored from __doc__
    #     """ D.viewitems() -> a set-like object providing a view on D's items """
    #     pass
    #
    # def viewkeys(self): # real signature unknown; restored from __doc__
    #     """ D.viewkeys() -> a set-like object providing a view on D's keys """
    #     pass
    #
    # def viewvalues(self): # real signature unknown; restored from __doc__
    #     """ D.viewvalues() -> an object providing a view on D's values """
    #     pass

    # def __cmp__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__cmp__(y) <==> cmp(x,y) """
    #     pass
    #
    # def __contains__(self, k): # real signature unknown; restored from __doc__
    #     """ D.__contains__(k) -> True if D has a key k, else False """
    #     return False
    #
    # def __delitem__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__delitem__(y) <==> del x[y] """
    #     pass
    #
    # def __eq__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__eq__(y) <==> x==y """
    #     pass
    #
    # def __getattribute__(self, name): # real signature unknown; restored from __doc__
    #     """ x.__getattribute__('name') <==> x.name """
    #     pass
    #
    # def __getitem__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__getitem__(y) <==> x[y] """
    #     pass
    #
    # def __ge__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ge__(y) <==> x>=y """
    #     pass
    #
    # def __gt__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__gt__(y) <==> x>y """
    #     pass

    def __init__(self, seq=None, **kwargs): # known special case of dict.__init__
        """
        dict() -> new empty dictionary
        dict(mapping) -> new dictionary initialized from a mapping object's
            (key, value) pairs
        dict(iterable) -> new dictionary initialized as if via:
            d = {}
            for k, v in iterable:
                d[k] = v
        dict(**kwargs) -> new dictionary initialized with the name=value pairs
            in the keyword argument list.  For example:  dict(one=1, two=2)
        # (copied from class doc)
        """
        self.__update_impl(seq, **kwargs)


    # def __iter__(self): # real signature unknown; restored from __doc__
    #     """ x.__iter__() <==> iter(x) """
    #     pass
    #
    # def __len__(self): # real signature unknown; restored from __doc__
    #     """ x.__len__() <==> len(x) """
    #     pass
    #
    # def __le__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__le__(y) <==> x<=y """
    #     pass
    #
    # def __lt__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__lt__(y) <==> x<y """
    #     pass
    #
    # @staticmethod # known case of __new__
    # def __new__(S, *more): # real signature unknown; restored from __doc__
    #     """ T.__new__(S, ...) -> a new object with type S, a subtype of T """
    #     pass
    #
    # def __ne__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ne__(y) <==> x!=y """
    #     pass
    #
    # def __repr__(self): # real signature unknown; restored from __doc__
    #     """ x.__repr__() <==> repr(x) """
    #     pass
    #
    # def __setitem__(self, i, y): # real signature unknown; restored from __doc__
    #     """ x.__setitem__(i, y) <==> x[i]=y """
    #     pass
    #
    # def __sizeof__(self): # real signature unknown; restored from __doc__
    #     """ D.__sizeof__() -> size of D in memory, in bytes """
    #     pass
    #
    # __hash__ = None


# class set(set):
#     """
#     set() -> new empty set object
#     set(iterable) -> new set object
#
#     Build an unordered collection of unique elements.
#     """
    # def add(self, *args, **kwargs): # real signature unknown
    #     """
    #     Add an element to a set.
    #
    #     This has no effect if the element is already present.
    #     """
    #     pass

    # def clear(self, *args, **kwargs): # real signature unknown
    #     """ Remove all elements from this set. """
    #     pass

    # def copy(self, *args, **kwargs): # real signature unknown
    #     """ Return a shallow copy of a set. """
    #     pass

    # def difference(self, *args, **kwargs): # real signature unknown
    #     """
    #     Return the difference of two or more sets as a new set.
    #
    #     (i.e. all elements that are in this set but not the others.)
    #     """
    #     pass
    #
    # def difference_update(self, *args, **kwargs): # real signature unknown
    #     """ Remove all elements of another set from this set. """
    #     pass

    # def discard(self, *args, **kwargs): # real signature unknown
    #     """
    #     Remove an element from a set if it is a member.
    #
    #     If the element is not a member, do nothing.
    #     """
    #     pass
    #
    # def intersection(self, *args, **kwargs): # real signature unknown
    #     """
    #     Return the intersection of two or more sets as a new set.
    #
    #     (i.e. elements that are common to all of the sets.)
    #     """
    #     pass

    # def intersection_update(self, *args, **kwargs): # real signature unknown
    #     """ Update a set with the intersection of itself and another. """
    #     pass
    #
    # def isdisjoint(self, *args, **kwargs): # real signature unknown
    #     """ Return True if two sets have a null intersection. """
    #     pass
    #
    # def issubset(self, *args, **kwargs): # real signature unknown
    #     """ Report whether another set contains this set. """
    #     pass
    #
    # def issuperset(self, *args, **kwargs): # real signature unknown
    #     """ Report whether this set contains another set. """
    #     pass
    #
    # def pop(self, *args, **kwargs): # real signature unknown
    #     """
    #     Remove and return an arbitrary set element.
    #     Raises KeyError if the set is empty.
    #     """
    #     pass
    #
    # def remove(self, *args, **kwargs): # real signature unknown
    #     """
    #     Remove an element from a set; it must be a member.
    #
    #     If the element is not a member, raise a KeyError.
    #     """
    #     pass
    #
    # def symmetric_difference(self, *args, **kwargs): # real signature unknown
    #     """
    #     Return the symmetric difference of two sets as a new set.
    #
    #     (i.e. all elements that are in exactly one of the sets.)
    #     """
    #     pass
    #
    # def symmetric_difference_update(self, *args, **kwargs): # real signature unknown
    #     """ Update a set with the symmetric difference of itself and another. """
    #     pass
    #
    # def union(self, *args, **kwargs): # real signature unknown
    #     """
    #     Return the union of sets as a new set.
    #
    #     (i.e. all elements that are in either set.)
    #     """
    #     pass
    #
    # def update(self, *args, **kwargs): # real signature unknown
    #     """ Update a set with the union of itself and others. """
    #     pass
    #
    # def __and__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__and__(y) <==> x&y """
    #     pass
    #
    # def __cmp__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__cmp__(y) <==> cmp(x,y) """
    #     pass
    #
    # def __contains__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__contains__(y) <==> y in x. """
    #     pass
    #
    # def __eq__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__eq__(y) <==> x==y """
    #     pass
    #
    # def __getattribute__(self, name): # real signature unknown; restored from __doc__
    #     """ x.__getattribute__('name') <==> x.name """
    #     pass
    #
    # def __ge__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ge__(y) <==> x>=y """
    #     pass
    #
    # def __gt__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__gt__(y) <==> x>y """
    #     pass
    #
    # def __iand__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__iand__(y) <==> x&=y """
    #     pass
    #
    # def __init__(self, seq=()): # known special case of set.__init__
    #     """
    #     set() -> new empty set object
    #     set(iterable) -> new set object
    #
    #     Build an unordered collection of unique elements.
    #     # (copied from class doc)
    #     """
    #     pass
    #
    # def __ior__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ior__(y) <==> x|=y """
    #     pass
    #
    # def __isub__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__isub__(y) <==> x-=y """
    #     pass
    #
    # def __iter__(self): # real signature unknown; restored from __doc__
    #     """ x.__iter__() <==> iter(x) """
    #     pass
    #
    # def __ixor__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ixor__(y) <==> x^=y """
    #     pass
    #
    # def __len__(self): # real signature unknown; restored from __doc__
    #     """ x.__len__() <==> len(x) """
    #     pass
    #
    # def __le__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__le__(y) <==> x<=y """
    #     pass
    #
    # def __lt__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__lt__(y) <==> x<y """
    #     pass
    #
    # @staticmethod # known case of __new__
    # def __new__(S, *more): # real signature unknown; restored from __doc__
    #     """ T.__new__(S, ...) -> a new object with type S, a subtype of T """
    #     pass
    #
    # def __ne__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ne__(y) <==> x!=y """
    #     pass
    #
    # def __or__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__or__(y) <==> x|y """
    #     pass
    #
    # def __rand__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__rand__(y) <==> y&x """
    #     pass
    #
    # def __reduce__(self, *args, **kwargs): # real signature unknown
    #     """ Return state information for pickling. """
    #     pass
    #
    # def __repr__(self): # real signature unknown; restored from __doc__
    #     """ x.__repr__() <==> repr(x) """
    #     pass
    #
    # def __ror__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__ror__(y) <==> y|x """
    #     pass
    #
    # def __rsub__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__rsub__(y) <==> y-x """
    #     pass
    #
    # def __rxor__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__rxor__(y) <==> y^x """
    #     pass
    #
    # def __sizeof__(self): # real signature unknown; restored from __doc__
    #     """ S.__sizeof__() -> size of S in memory, in bytes """
    #     pass
    #
    # def __sub__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__sub__(y) <==> x-y """
    #     pass
    #
    # def __xor__(self, y): # real signature unknown; restored from __doc__
    #     """ x.__xor__(y) <==> x^y """
    #     pass
    #
    # __hash__ = None
    pass


# class tuple(tuple):
#     """
#     tuple() -> empty tuple
#     tuple(iterable) -> tuple initialized from iterable's items
#
#     If the argument is a tuple, the return value is the same object.
#     """
#     # def count(self, value): # real signature unknown; restored from __doc__
#     #     """ T.count(value) -> integer -- return number of occurrences of value """
#     #     return 0
#
#     # def index(self, value, start=None, stop=None): # real signature unknown; restored from __doc__
#     #     """
#     #     T.index(value, [start, [stop]]) -> integer -- return first index of value.
#     #     Raises ValueError if the value is not present.
#     #     """
#     #     return 0
#
#     # def __add__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__add__(y) <==> x+y """
#     #     pass
#     #
#     # def __contains__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__contains__(y) <==> y in x """
#     #     pass
#     #
#     # def __eq__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__eq__(y) <==> x==y """
#     #     pass
#     #
#     # def __getattribute__(self, name): # real signature unknown; restored from __doc__
#     #     """ x.__getattribute__('name') <==> x.name """
#     #     pass
#     #
#     # def __getitem__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__getitem__(y) <==> x[y] """
#     #     pass
#     #
#     # def __getnewargs__(self, *args, **kwargs): # real signature unknown
#     #     pass
#     #
#     def __getslice__(self, i, j): # real signature unknown; restored from __doc__
#         """
#         x.__getslice__(i, j) <==> x[i:j]
#
#                    Use of negative indices is not supported.
#         """
#         l = len(self)
#         i = (0 if l+i < 0 else l + i) if i < 0 else i
#         j = l if l < j else j
#         return tuple([self[idx] for idx in range(i, j)])
#
#     #
#     # def __ge__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__ge__(y) <==> x>=y """
#     #     pass
#     #
#     # def __gt__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__gt__(y) <==> x>y """
#     #     pass
#     #
#     # def __hash__(self): # real signature unknown; restored from __doc__
#     #     """ x.__hash__() <==> hash(x) """
#     #     pass
#     #
#     # def __init__(self, seq=()): # known special case of tuple.__init__
#     #     """
#     #     tuple() -> empty tuple
#     #     tuple(iterable) -> tuple initialized from iterable's items
#     #
#     #     If the argument is a tuple, the return value is the same object.
#     #     # (copied from class doc)
#     #     """
#     #     pass
#     #
#     # def __iter__(self): # real signature unknown; restored from __doc__
#     #     """ x.__iter__() <==> iter(x) """
#     #     pass
#     #
#     # def __len__(self): # real signature unknown; restored from __doc__
#     #     """ x.__len__() <==> len(x) """
#     #     pass
#     #
#     # def __le__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__le__(y) <==> x<=y """
#     #     pass
#     #
#     # def __lt__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__lt__(y) <==> x<y """
#     #     pass
#     #
#     # def __mul__(self, n): # real signature unknown; restored from __doc__
#     #     """ x.__mul__(n) <==> x*n """
#     #     pass
#     #
#     # @staticmethod # known case of __new__
#     # def __new__(S, *more): # real signature unknown; restored from __doc__
#     #     """ T.__new__(S, ...) -> a new object with type S, a subtype of T """
#     #     pass
#     #
#     # def __ne__(self, y): # real signature unknown; restored from __doc__
#     #     """ x.__ne__(y) <==> x!=y """
#     #     pass
#     #
#     # def __repr__(self): # real signature unknown; restored from __doc__
#     #     """ x.__repr__() <==> repr(x) """
#     #     pass
#     #
#     # def __rmul__(self, n): # real signature unknown; restored from __doc__
#     #     """ x.__rmul__(n) <==> n*x """
#     #     pass
#     #
#     # def __sizeof__(self): # real signature unknown; restored from __doc__
#     #     """ T.__sizeof__() -- size of T in memory, in bytes """
#     #     pass
#     pass


# __list__, __dict__, __set__, __tuple__ = list, dict, set, tuple


if __name__ == '__main__':
    x = list([1,2,3,4])
    x.insert(0, 5)
    print x

    #print x.index(5, 11, 18)
    print x[1: -1]
    print x[11:12]
#     l = list([1, 1, 5, 5])
#     del l[1:2]
#     print l
#     l = list([1, 2, 3, 4, 5])
#     del l[-1]
#     print l
#
#     l = list([1, 2, 3, 4, 5])
#     del l[0:3]
#     print l
#
#     l = list([1, 2, 3, 4, 5])
#     del l[4:0:-1]
#     print l
#
#     l = list([1, 2, 3, 4, 5])
#     del l[-100:100:2]
#     print l
