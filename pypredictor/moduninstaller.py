#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import os.path
import logging

logger = logging.getLogger('pytracer')


def uninstall(recordfile):
    with open(recordfile) as rf:
        pyfnamelist = rf.read().splitlines()
        if '' in pyfnamelist:
            pyfnamelist.remove('')
    all_nonbuiltin_modules = {}
    for mname, mod in sys.modules.items():
        if hasattr(mod, '__file__'):
            mfilename = mod.__file__
            mfilename = os.path.abspath(mfilename)
            if mfilename.endswith('pyc') or mfilename.endswith('pyo'):
                mfilename = mfilename[:-1]
            if mfilename not in all_nonbuiltin_modules:
                all_nonbuiltin_modules[mfilename] = {mname}
            else:
                all_nonbuiltin_modules[mfilename].add(mname)
    for pyfname in pyfnamelist:
        if pyfname in all_nonbuiltin_modules:
            for mname in all_nonbuiltin_modules[pyfname]:
                del sys.modules[mname]
                logger.info('uninstall module <{mname}>: <{filename}> done'.format(mname=mname,
                                                                                   filename=pyfname))
        else:
            logger.info('module %r not in sys.modules' % pyfname)
    # for pyfname, mnamelist in all_nonbuiltin_modules.iteritems():
    #     for mname in mnamelist:
    #         del sys.modules[mname]
    #         logger.info('uninstall module <{mname}>: <{filename}> done'.format(mname=mname,
    #                                                                            filename=pyfname))


def reinstall(recordfile):
    with open(recordfile) as rf:
        pyfnamelist = rf.read().splitlines()
        if '' in pyfnamelist:
            pyfnamelist.remove('')
    all_nonbuiltin_modules = {}
    for mname, mod in sys.modules.items():
        if hasattr(mod, '__file__'):
            mfilename = mod.__file__
            mfilename = os.path.abspath(mfilename)
            if mfilename.endswith('pyc') or mfilename.endswith('pyo'):
                mfilename = mfilename[:-1]
            if mfilename not in all_nonbuiltin_modules:
                all_nonbuiltin_modules[mfilename] = {(mname, mod)}
            else:
                all_nonbuiltin_modules[mfilename].add((mname, mod))
    for pyfname in pyfnamelist:
        if pyfname in all_nonbuiltin_modules:
            for mname, _ in all_nonbuiltin_modules[pyfname]:
                del sys.modules[mname]
                logger.info('uninstall module <{mname}>: '
                            '<{filename}> done'.format(mname=mname,
                                                       filename=pyfname))
        else:
            logger.info('module %r not in sys.modules' % pyfname)
    for pyfname in pyfnamelist:
        if pyfname in all_nonbuiltin_modules:
            for mname, mod in all_nonbuiltin_modules[pyfname]:
                try:
                    __import__(mname)
                except ImportError:
                    logger.warning('cannot import module <{mname}>:'
                                   ' <{filename}>'.format(mname=mname,
                                                          filename=pyfname))
                else:
                    logger.info('reinstall module <{mname}>:'
                                ' <{filename}> done'.format(mname=mname,
                                                            filename=pyfname))
