#!/usr/bin/python
# _*_ coding:utf-8 _*_
from pypredictor import tracenodes as new

from utils import getsourcefile, getliveobject
from linetracemaker import LineTraceMaker
from excepttracemaker import ExceptTraceMaker
from calltracemaker import EntryCallTraceMaker, CallTraceMaker
from returntracemaker import ReturnTraceMaker


class NameCreator(object):
    suffix_count = 1L

    @classmethod
    def new_name(cls, name=None):
        cls.suffix_count += 1
        if name is None:
            return 'tmp_#' + str(cls.suffix_count)
        else:
            return name + '_#' + str(cls.suffix_count)

    @classmethod
    def new_osig(cls, osig):
        racyosig = 'TA' + cls.new_name(osig)
        return racyosig

    @staticmethod
    def basename(name):
        return name.split('_#')[0]

    @staticmethod
    def attrsig(value, attr):
        return value.symstr + '.' + attr

    @staticmethod
    def subsig(value, index):
        if isinstance(index.liveobj, (str, int)):
            substr = str(index.liveobj).replace(':', '@@@@')
        else:
            substr = index.valstr
        return value.symstr + '.$' + substr


class AnalysisFrame(object):

    def __init__(self, back, eframe, l2s, n2o):
        self.back = back
        self.eframe = eframe
        self.lineno2stmt = l2s
        self.nl2ol = n2o
        self.currlineno = -1
        self.name2racyid = {}
        self.branchstack = []

    @property
    def funcname(self):
        return self.eframe.f_code.co_name

    @property
    def filename(self):
        return getsourcefile(self.eframe.f_code.co_filename)

    @property
    def currstmt(self):
        return self.lineno2stmt.get(self.currlineno)

    @property
    def curroldlineno(self):
        return self.nl2ol.get(self.currlineno, '-1')

    def oldlineno(self, lno):
        return self.nl2ol.get(lno, '-1')

    def liveobject(self, name):
        try:
            return getliveobject(self.eframe, name)
        except ValueError:
            raise ValueError(self.errorinfo('can not find the '
                                            'live object of <{}>'.format(name)))

    @staticmethod
    def new_name(name=None):
        return NameCreator.new_name(name)

    def new_id(self, name=None, heapobj=None):
        if heapobj is not None:
            assert isinstance(heapobj, new.HeapObject), \
                '{} must be instance of heapobject'.format(heapobj)
            if name is None:  # new temple name
                name = self.new_name()
                nid = new.Id(name, heapobj)
            else:
                nid = new.Id(self.new_name(name), heapobj)

        else:
            assert name is not None, '{} must not be None'.format(name)
            nid = new.Id(self.new_name(name), new.HeapObject(self.liveobject(name)))
        self.update_id(name, nid)
        return nid

    def update_id(self, name, nid):
        if self.branchstack:
            self.branchstack[-1].update_local_racyid(name, nid)
        else:
            self.name2racyid[name] = nid

    def get_racyid(self, name):
        for branch in self.branchstack[::-1]:
            if branch.exist_local_name(name):
                return branch.get_local_racyid(name)
        return self.name2racyid.get(name)

    def has_branch(self):
        return len(self.branchstack) == 0

    def topbranch(self):
        return self.branchstack[-1] if self.branchstack else None

    def pop_branch(self):
        return self.branchstack.pop() if self.branchstack else None

    def push_branch(self, branch):
        self.branchstack.append(branch)

    def errorinfo(self, msg):
        return ('AnalysisFrame- {0} [in <{1}> '
                'file: <{2}>]'.format(msg, self.funcname, self.filename))


class TraceMaker(object):
    def __init__(self, backaframe, eframe, l2s, n2o, entry=False):
        self.aframe = AnalysisFrame(backaframe, eframe, l2s, n2o)
        if entry:
            self.calltmaker = EntryCallTraceMaker(self.aframe)
        else:
            self.calltmaker = CallTraceMaker(self.aframe)
        self.rettmaker = ReturnTraceMaker(self.aframe)
        self.linetmaker = LineTraceMaker(self.aframe)
        self.exptmaker = ExceptTraceMaker(self.aframe)

    def make_call(self):
        return self.calltmaker.make()

    def make_line(self):
        return self.linetmaker.make()

    def make_return(self):
        return self.rettmaker.make()

    def make_except(self):
        return self.exptmaker.make()







