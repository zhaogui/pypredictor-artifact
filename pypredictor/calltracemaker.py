#!/usr/bin/python
# _*_ coding:utf-8 _*_

import yaml
import logging
from ast import *
from inspect import getdoc
from collections import Iterable

import tracenodes as new
from pypredictor.utils import *
from basetracemaker import BaseTraceMaker


logger = logging.getLogger('pytracer')


class CallTraceMakerMixIn(object):
    def get_backaframe_id(self, name):
        racyid = self.backaframe.get_racyid(name)
        if racyid is None:
            val = self.backaframe.liveobject(name)
            racyid = self.backaframe.new_id(name, new.HeapObject(val))
            self.append_trace(new.Allocate(racyid, racyid.value))
        return racyid

    def new_allocate(self, tname, value=None):
        target = self.aframe.new_id(tname, value)
        return new.Allocate(target, target.value)

    def errorinfo(self, msg):
        return (' CallTrace- {0} [in <{1}> file: '
                '<{2}>]'.format(msg,
                                self.backaframe.funcname
                                if self.backaframe else None,
                                self.backaframe.filename
                                if self.backaframe else None))


class CallTraceMaker(BaseTraceMaker, CallTraceMakerMixIn):
    _isdebug = logger.isEnabledFor(logging.DEBUG)

    def make(self):
        self.results = []
        callsite = self.backaframe.currstmt
        if self.check_ifoutofbranch(self.backaframe):
            self.post_branchend(self.backaframe)
        arginfo = inspect.getargvalues(self.curreframe)
        method = 'make_' + callsite.__class__.__name__.lower()
        maker = getattr(self, method, self.default_make)
        maker(arginfo, callsite)
        self.bindinfo(self.results, self.backaframe)
        return self.results

    def default_make(self, arginfo, *_):
        callee_arginfo = arginfo
        callee_args = callee_arginfo.args
        callee_vararg = callee_arginfo.varargs
        callee_kwarg = callee_arginfo.keywords
        callee_locals = callee_arginfo.locals
        for param in callee_args:
            value = new.HeapObject(callee_locals.get(param))
            self.append_trace(self.new_allocate(param, value))
        if callee_vararg is not None:
            value = new.HeapObject(callee_locals.get(callee_vararg))
            self.append_trace(self.new_allocate(callee_vararg, value))
        if callee_kwarg is not None:
            value = new.HeapObject(callee_locals.get(callee_kwarg))
            self.append_trace(self.new_allocate(callee_kwarg, value))

    def make_assign(self, arginfo, callsite):
        try:
            self.visit(callsite.value, arginfo)
        except NotImplementedError:
            if self._isdebug:
                logger.debug(self.errorinfo('not implement this '
                                            'kind of call {0}'.format(dump(callsite))))
            self.default_make(arginfo, callsite)

    # implement it in the future
    def make_augassign(self, arginfo, callsite):
        self.default_make(arginfo, callsite)

    def make_expr(self, arginfo, callsite):
        try:
            self.visit(callsite.value, arginfo)
        except NotImplementedError:
            if self._isdebug:
                logger.debug(self.errorinfo('not implement this '
                                            'kind of call {0}'.format(dump(callsite))))
            self.default_make(arginfo, callsite)

    def make_import(self, *_):
        pass    # we simply ignore it since there is no parameter transmissions

    def make_importfrom(self, *_):
        pass    # # we simply ignore it since there is no parameter transmissions

    def make_classdef(self, *_):
        pass    # we simply ignore it since there is no parameter transmissions

    def make_with(self, arginfo, callsite):
        assert arginfo.args
        # self = O
        tname = arginfo.args[0]
        tval = arginfo.locals.get(arginfo.args[0])
        self.append_trace(new.MethodInvoke(self.get_id(tname), '__enter__'))
        self.append_trace(self.new_allocate(tname, new.HeapObject(tval)))

    def visit(self, node, arginfo):
        """Visit a node."""
        method = 'visit_' + node.__class__.__name__.lower()
        visitor = getattr(self, method, self.default_visit)
        visitor(node, arginfo)

    def default_visit(self, node, arginfo):
        raise NotImplementedError

    def can_transmit_params(self, callable_obj):
        if is_function(callable_obj):
            return callable_obj.func_code is self.curreframe.f_code
        elif is_bind_method(callable_obj) or \
                is_unbound_method(callable_obj):
            return hasattr(callable_obj, 'im_func') and \
                hasattr(callable_obj.im_func, 'func_code') and \
                callable_obj.im_func.func_code is self.curreframe.f_code
        elif is_class(callable_obj) or \
                is_type(callable_obj) or \
                is_instance(callable_obj):
            return True
        else:
            return False

    def visit_call(self, call, arginfo):
        if isinstance(call.func, Name):
            funcname = call.func.id
            callable_obj = self.backaframe.liveobject(funcname)
            func_id = self.get_backaframe_id(funcname)
            if is_bind_method(callable_obj):
                # TODO: this is hard to handle, will do it in the future
                pass
            else:
                self.append_trace(new.FunctionCall(func_id))
        else:  # attribute
            recename = call.func.value.id
            rece_obj = self.backaframe.liveobject(recename)
            attr = call.func.attr
            rece_id = self.get_backaframe_id(recename)
            try:
                callable_obj = getattr(rece_obj, attr)
                self.append_trace(new.MethodInvoke(rece_id, attr))
            except AttributeError:
                if attr.startswith('__') and (not attr.endswith('__')):
                    transattr = '_' + rece_obj.__class__.__name__ + attr
                    callable_obj = getattr(rece_obj, transattr)
                    self.append_trace(new.MethodInvoke(rece_id, transattr))
                else:
                    raise
        # logger.debug('++++++{0}'.format(callable_obj))
        if not self.can_transmit_params(callable_obj):
            return self.default_make(arginfo, call)
        # logger.debug('-----{0}'.format(self.can_transmit_params(callable_obj)))
        caller_args = [arg.id for arg in call.args]
        caller_keywords = [(kw.arg, kw.value.id) for kw in call.keywords]
        caller_starargs = call.starargs.id if call.starargs is not None else None
        caller_kwargs = call.kwargs.id if call.kwargs is not None else None
        callee_arginfo = arginfo
        callee_args = callee_arginfo.args
        callee_vararg = callee_arginfo.varargs
        callee_kwarg = callee_arginfo.keywords
        callee_locals = callee_arginfo.locals

        # in python 2.x types.MethodType and types.UnboundMethodType are the same
        #if isinstance(callable_obj, (types.MethodType, types.ClassType, types.TypeType)):
        if is_class(callable_obj) or \
            is_type(callable_obj) or \
            is_instance(callable_obj) or \
                is_bind_method(callable_obj):
            # case of method with no explicit 'self' arg,
            # which is included in *varargs instead.
            if len(callee_args) >= 1:
                self_ = callee_args[0]
                self.append_trace(self.new_allocate(self_, new.HeapObject(callee_locals.get(self_))))
                callee_args = callee_args[1:]
            else:
                logger.warning(self.errorinfo('instancemethod with no explicit self/cls arg'))

        rest_ce_args = list(callee_args)
        for arg, param in zip(caller_args, callee_args):
            arg_id = self.get_backaframe_id(arg)
            param_val = callee_locals.get(param)
            param_id = self.aframe.new_id(param, new.HeapObject(param_val))
            self.append_trace(new.Assign(param_id, arg_id))
            rest_ce_args.remove(param)

        unmatched_keywords = []
        for k, v in caller_keywords:
            if k in callee_args:
                arg_id = self.get_backaframe_id(v)
                param_val = callee_locals.get(k)
                param_id = self.aframe.new_id(k, new.HeapObject(param_val))
                self.append_trace(new.Assign(param_id, arg_id))
                if k in rest_ce_args:
                    rest_ce_args.remove(k)
            else:
                unmatched_keywords.append((k, v))

        # fix code: specify the parameter transmition, do not confirm
        # callee_args - caller_args - caller_keywords
        rest_ce_args_afterstarargs = list(rest_ce_args)
        starargs_val = self.backaframe.liveobject(caller_starargs) if caller_starargs else []
        starargs_index = -1
        for i, (param, stararg_val) in enumerate(zip(rest_ce_args, starargs_val)):
            param_val = callee_locals.get(param)
            param_id = self.new_id(param, new.HeapObject(param_val))
            starargs_id = self.get_backaframe_id(caller_starargs)
            if stararg_val is param_val:
                self.append_trace(new.SubLoad(param_id, starargs_id, self.new_tmpid(i)))
            else:
                self.append_trace(new.Allocate(param_id, new.HeapObject(param_val)))
            rest_ce_args_afterstarargs.remove(param)
            starargs_index = i  # record the last index that has been consumed.
        # fix code: specify the parameter transmition, do not confirm
        # callee_args - caller_args - caller_keywords - caller_starargs
        rest_ce_args_afterstarargs_kwargs = list(rest_ce_args_afterstarargs)
        for param in rest_ce_args_afterstarargs:
            if caller_kwargs is not None:
                param_val = callee_locals.get(param)
                param_id = self.new_id(param, new.HeapObject(param_val))
                kwargs_val = self.backaframe.liveobject(caller_kwargs)
                kwargs_id = self.get_backaframe_id(caller_kwargs)
                if param in kwargs_val:
                    self.append_trace(new.SubLoad(param_id, kwargs_id, self.new_tmpid(param)))
                    rest_ce_args_afterstarargs_kwargs.remove(param)

        # callee_args - caller_args - caller_keywords - caller_starargs - caller_kwargs
        rest_ce_args = rest_ce_args_afterstarargs_kwargs
        for param in rest_ce_args:
            param_val = callee_locals.get(param)
            self.append_trace(self.new_allocate(param, new.HeapObject(param_val)))

        # callee_vararg
        if callee_vararg is not None:
            # vararg = []
            param_val = callee_locals.get(callee_vararg)
            param_id = self.get_id(callee_vararg, new.HeapObject(param_val))   # callee_vararg id
            # results.append(self.new_allocate(callee_vararg, param_val))
            # vararg[i] = arg[j]
            i, j, e = 0, len(callee_args), len(caller_args)
            while j < e:
                arg_val = self.backaframe.liveobject(caller_args[j])
                arg_id = self.get_backaframe_id(caller_args[j])
                if arg_val is param_val[i]:
                    self.append_trace(
                        new.SubStore(param_id, self.new_tmpid(i), arg_id))
                    j += 1
                else:
                    self.append_trace(new.SubStore(param_id,
                                                   self.new_tmpid(i),
                                                   self.new_tmpid(param_val[i])))
                i += 1

            # vararg[i] = starargs[j]
            if caller_starargs is not None:
                starargs_val = self.backaframe.liveobject(caller_starargs)
                starargs_id = self.get_backaframe_id(caller_starargs)
                # j should start from next one of the last consume index
                # Fix: call: join(*x)  =>  def join(a, *b)
                j, e = (starargs_index + 1), len(starargs_val)
                while j < e:
                    if starargs_val[j] is param_val[i]:
                        tmpid = self.new_id(None, new.HeapObject(starargs_val[j]))
                        self.append_trace(new.SubLoad(tmpid, starargs_id, self.new_tmpid(j)))
                        self.append_trace(new.SubStore(param_id, self.new_tmpid(i), tmpid))
                        j += 1
                    else:
                        tmpid = self.new_tmpid(param_val[i])
                    self.append_trace(new.SubStore(param_id, self.new_tmpid(i), tmpid))
                    i += 1

        # callee_kwarg
        if callee_kwarg is not None:
            param_val = callee_locals.get(callee_kwarg)
            param_id = self.get_id(callee_kwarg, new.HeapObject(param_val))
            # kwarg = {}
            # results.append(self.new_allocate(callee_kwarg, param_val))
            # kwarg[k] = v
            for k, v in unmatched_keywords:
                arg_id = self.get_backaframe_id(v)
                self.append_trace(new.SubStore(param_id, self.new_tmpid(k), arg_id))
            # callee_kwarg[k] = caller_kwargs[k]
            if caller_kwargs is not None:
                kwargs_val = self.backaframe.liveobject(caller_kwargs)
                kwargs_id = self.get_backaframe_id(caller_kwargs)
                for k in kwargs_val.keys():
                    if k in param_val:
                        tmpid = self.new_id(None, new.HeapObject(kwargs_val[k]))
                        self.append_trace(new.SubLoad(tmpid, kwargs_id, self.new_tmpid(k)))
                        self.append_trace(new.SubStore(param_id, self.new_tmpid(k), tmpid))

    #================================================================
    # The following styles of call will be implemented in the future.
    #================================================================
    def visit_unary(self, call, info):
        raise NotImplementedError

    def visit_binop(self, call, arginfo):
        raise NotImplementedError

    def visit_compare(self, call, arginfo):
        raise NotImplementedError

    def visit_attribute(self, call, arginfo):
        raise NotImplementedError

    def visit_name(self, call, arginfo):
        raise NotImplementedError

    def visit_subscript(self, call, arginfo):
        raise NotImplementedError


class EntryCallTraceMaker(CallTraceMaker):

    def __init__(self, aframe):
        super(EntryCallTraceMaker, self).__init__(aframe)
        self.argspec = self._parsedocstr()

    def _parsedocstr(self):
        if self.aframe.funcname == '<module>':
            return {}
        docstr = self.getdocstr()
        if docstr:
            argsspec = yaml.load(docstr)
        else:
            argsspec = {}
        # import sys
        # print >>sys.stderr, argsspec, docstr
        assert isinstance(argsspec, dict), 'argument specification be type of dict'
        return {k: v if isinstance(v, list) else [v]
                for k, v in argsspec.iteritems()}

    def getdocstr(self):
        callsite = self.backaframe.currstmt
        assert isinstance(callsite, Assign) or \
            (isinstance(callsite, Expr) and
             isinstance(callsite.value, Call)), \
            'cannot handle the callsite of entry function'
        call = callsite.value
        if isinstance(call.func, Name):
            funcname = call.func.id
            callable_obj = self.backaframe.liveobject(funcname)
        else:  # attribute
            recename = call.func.value.id
            rece_obj = self.backaframe.liveobject(recename)
            attr = call.func.attr
            try:
                callable_obj = getattr(rece_obj, attr)
            except AttributeError:
                if attr.startswith('__') and (not attr.endswith('__')):
                    callable_obj = getattr(rece_obj, '_' + rece_obj.__class__.__name__ + attr)
                else:
                    raise
        return getdoc(callable_obj)

    def is_anyvalue(self, arg):
        return self.argspec.get(arg, '') == ['<anyvalue>']

    def has_relaxspec(self, arg):
        return arg in self.argspec

    def getvaluelist(self, arg):
        def getvalues(values):
            res = []
            for val in values:
                if isinstance(val, str) and val.startswith('$'):
                    res.append(self.backaframe.liveobject(val[1:]))
                elif (not isinstance(val, str)) and isinstance(val, Iterable):
                    res.append(getvalues(val))
                else:
                    res.append(val)
            return res
        return getvalues(self.argspec.get(arg, []))

    def visit_call(self, call, arginfo):
        callee_arginfo = arginfo
        callee_args = callee_arginfo.args
        callee_vararg = callee_arginfo.varargs
        callee_kwarg = callee_arginfo.keywords
        callee_locals = callee_arginfo.locals
        for param in callee_args:
            value = new.HeapObject(callee_locals.get(param))
            target = self.aframe.new_id(param, value)
            if self.is_anyvalue(param):
                # use symbolic form to represent
                value.set_assymbolic()
                self.append_trace(new.InfiniteRelaxAllocate(target, value))
            elif self.has_relaxspec(param):
                valuelist = self.getvaluelist(param)
                valuelist.insert(0, callee_locals.get(param))
                values = [new.HeapObject(v) for v in valuelist]
                self.append_trace(new.FiniteRelaxAllocate(target, values))
            else:
                self.append_trace(new.Allocate(target, value))
        if callee_vararg is not None:
            value = new.HeapObject(callee_locals.get(callee_vararg))
            target = self.aframe.new_id(callee_vararg, value)
            self.append_trace(new.Allocate(target, value))
        if callee_kwarg is not None:
            value = new.HeapObject(callee_locals.get(callee_kwarg))
            target = self.aframe.new_id(callee_kwarg, value)
            self.append_trace(self.new_allocate(target, value))
