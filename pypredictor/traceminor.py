#!/usr/bin/python
# _*_ coding:utf-8 _*_

__author__ = 'root'
from dbengine import TraceDBReader


def count_extevents(projname, outdir='', gid=0):
    with TraceDBReader(projname, outdir) as reader:
        tanker = reader.read_all()
        count = 0
        for dbevent in tanker.gid2event.itervalues():
            count += dbevent['ext']
            cgid = int(dbevent.get('gid', -1))
            if gid <= cgid:
                return count

def count_calls(projname, outdir='', gid=0):
    with TraceDBReader(projname, outdir) as reader:
        tanker = reader.read_all()
        count = 0
        count2 = 0
        count3 = 0
        for dbevent in tanker.gid2event.itervalues():
            if dbevent['type'] in (8, 9):
                if 'hasattr' in dbevent['inst'] or \
                                'getattr' in dbevent['inst'] or \
                                '__import__' in dbevent['inst'] or \
                                'setattr' in dbevent['inst']:
                    count2 += 1
                if 'eval' in dbevent['inst']:
                    count3 += 1
            # if esig == 'methodinvoke' or esig == 'functioncall':
                print dbevent
                count += 1
                cgid = int(dbevent.get('gid', -1))
                # if gid <= cgid:
                #     return count, count2
        return count, count2, count3

