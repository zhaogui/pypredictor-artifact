#!/usr/bin/python
# _*_ coding:utf-8 _*_

import logging
import tracenodes as new
from ast import Name, Attribute, Or


logger = logging.getLogger('pytracer')


class BuiltinFuncCallMixIn(object):
    def tuple_items(self, tuple_val):
        results = []
        for t in tuple_val:
            if isinstance(t, tuple):
                results.extend(self.tuple_items(t))
            else:
                results.append(t)
        return results

    def isinstance_call(self, target, call):
        assert target is not None
        args = call.args
        assert len(args) == 2, 'Unexpected length of isinstance call'
        val_val, type_val = self.liveobject(args[0].id), self.liveobject(args[1].id)
        if isinstance(type_val, tuple):
            try:
                pre_tmpval, pre_tmpid = False, self.new_tmpid(False)
                for t in self.tuple_items(type_val):
                    # y_n = isinstance(x, t_n)
                    # z_n = z_n-1 or y_n
                    new_val = isinstance(val_val, t)
                    tmp_tgtid = self.new_id(None, new.HeapObject(new_val))
                    self.append_trace(new.SubTypeAssume(tmp_tgtid, self.get_id(args[0]),
                                                       self.new_tmpid(new.HeapObject(t))))
                    new_tmpid = self.new_id(None, new.HeapObject(pre_tmpval or new_val))
                    self.append_trace(new.BopAssign(new_tmpid, pre_tmpid, tmp_tgtid, Or()))
                    pre_tmpid = new_tmpid
                self.append_trace(new.Assign(self.new_id(target), pre_tmpid))
            except Exception as e:
                import sys
                print >>sys.stderr, sys.exc_traceback()
        else:
            self.append_trace(new.SubTypeAssume(self.new_id(target),
                                                self.get_id(args[0]),
                                                self.get_id(args[1])))

    def hasattr_call(self, target, call):
        assert target is not None
        args = call.args
        assert len(args) == 2, 'Unexpected length of hasattr call'
        self.append_trace(new.HasAttrAssume(self.new_id(target),
                                            self.liveobject(args[1].id),
                                            self.get_id(args[0])))

    def getattr_call(self, target, call):
        args = call.args
        if not (len(args) == 2 or len(args) == 3):
            raise NotImplementedError
        obj_arg, attr_arg = args[0].id, args[1].id
        obj_val, attr_val = self.liveobject(obj_arg), self.liveobject(attr_arg)
        self.append_trace(new.Allocate(self.new_id(attr_arg), new.HeapObject(attr_val)))
        if len(args) == 2:
            self.append_trace(new.AttrLoad(self.new_id(target),
                                           self.get_id(obj_arg), attr_val))
        else:
            if hasattr(obj_val, attr_val):
                self.append_trace(new.AttrLoad(self.new_id(target),
                                               self.get_id(obj_arg), attr_val))
            else:
                self.append_trace(new.Assign(self.new_id(target),
                                             self.get_id(args[2])))

    def setattr_call(self, target, call):
        args = call.args
        if len(args) != 3:
            raise NotImplementedError
        obj_arg, attr_arg, val_arg = args[0].id, args[1].id, args[2].id
        attr_val = self.liveobject(attr_arg)
        self.append_trace(new.Allocate(self.new_id(attr_arg), new.HeapObject(attr_val)))
        self.append_trace(new.AttrStore(self.get_id(obj_arg), attr_val, self.get_id(val_arg)))


class BuiltinMthdCallMixIn(object):
    def list_append(self, listobj, _, call):
        self.append_trace(new.SubStore(self.get_id(call.func.value)
                                       if isinstance(call.func, Attribute)
                                       else self.new_tmpid(listobj),
                                       self.new_tmpid(len(listobj)-1),
                                       self.get_id(call.args[0])))


class BuiltinCallTraceMaker(BuiltinFuncCallMixIn, BuiltinMthdCallMixIn):

    def default_model(self, *_):
        raise NotImplementedError('does not model this kind of builtin-call')

    def make_builtin_call(self, target, call):
        # with open('bicall.txt', 'a') as f:
        #     print >>f, call.func.id if isinstance(call.func, Name) else call.func.value.id + '.' + call.func.attr
        func = call.func
        if isinstance(func, Name):
            bifunc_or_mthd = self.liveobject(func.id)
        else:  # attribute
            rece_obj = self.liveobject(func.value.id)
            attr = func.attr
            bifunc_or_mthd = getattr(rece_obj, attr)
        if hasattr(bifunc_or_mthd, '__self__') and bifunc_or_mthd.__self__ is not None:
            self.make_builtinmethod_call(mthdobj=bifunc_or_mthd,
                                         target=target, call=call)
        else:
            self.make_builtinfunciton_call(funcobj=bifunc_or_mthd,
                                           target=target, call=call)

    def make_builtinfunciton_call(self, funcobj, target, call):
        try:
            try:
                model = getattr(self, funcobj.__name__+'_call', self.default_model)
            except AttributeError:
                logger.warning(str(funcobj) + ' has no __name__, passed...')
                return None
            return model(target, call)
        except AssertionError:
            pass

    def make_builtinmethod_call(self, mthdobj, target, call):
        try:
            if hasattr(mthdobj.__self__, '__class__'):  # assume __self__ is an instance
                model_name = mthdobj.__self__.__class__.__name__ + '_' + mthdobj.__name__
                model = getattr(self, model_name, self.default_model)
                return model(mthdobj.__self__, target, call)
            else:
                logger.warning('the __self__ of method {} has '
                               'no __class__'.format(mthdobj.__name__))
                return self.default_model(target)
        except AssertionError:
            pass

