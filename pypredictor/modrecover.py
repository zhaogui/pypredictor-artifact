__author__ = 'gui'

from os import unlink, listdir
from os.path import join, isfile, splitext, exists
import logging

from utils import encode_filename


__all__ = ['recover']

logger = logging.getLogger('pyrecover')


def remove_pycfiles(pyfnamelist):
    try:
        for pfname in pyfnamelist:
            pycname = pfname + 'c'
            if exists(pycname):
                unlink(pycname)
                logger.debug('remove pyc file <{0}> done'.format(pycname))
    except IOError as e:
        logger.error("I/O error({0}): {1}".format(e.errno, e.strerror))


def remove_cachepycfiles(pyfnamelist=(), tmpdir='tmp'):
    try:
        cachepycdir = join(tmpdir, 'cachepyc')
        if pyfnamelist:
            cachepycfiles = [join(cachepycdir, encode_filename(filename))
                                for filename in pyfnamelist]
        else:
            cachepycfiles = listdir(cachepycdir)
        for pyc in cachepycfiles:
            unlink(pyc)
    except IOError as e:
        logger.error("I/O error({0}): {1}".format(e.errno, e.strerror))


def remove_pklfiles(pyfnamelist=(), tmpdir='tmp'):
    try:
        cachepkldir = join(tmpdir, 'cachepkl')
        if pyfnamelist:
            pklfiles = [join(cachepkldir,
                             splitext(encode_filename(filename))[0]+'.pkl')
                        for filename in pyfnamelist]
        else:
            pklfiles = [pkl for pkl in listdir(tmpdir)
                        if isfile(join(tmpdir, pkl)) and splitext(pkl)[1] == '.pkl']
        # import pdb
        # pdb.set_trace()
        for pkl in pklfiles:
            unlink(pkl)
    except IOError as e:
        logger.error("I/O error({0}): {1}".format(e.errno, e.strerror))


def recover(recordfile, tmpdir='tmp', removecache=False):
    with open(recordfile) as rfile:
        pyfnamelist = rfile.read().splitlines()
        if '' in pyfnamelist:
            pyfnamelist.remove('')
    remove_pycfiles(pyfnamelist)
    if removecache:
        remove_cachepycfiles(pyfnamelist, tmpdir)
        remove_pklfiles(pyfnamelist, tmpdir)
