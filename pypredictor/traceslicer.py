#!/usr/bin/python
# _*_ coding:utf-8 _*_


class TraceSlicer(object):
    def __init__(self, var2def, branch2extend, branch2execend):
        self.var2def = var2def
        self.branch2extend = branch2extend
        self.branch2execend = branch2execend
        self.datadep = {}
        self.ctrldep = {}
        self.event2slice = {}

    def slice(self, event):
        slicedevents = set()
        processed = set()
        worklist = [x for x in event.getuses()]
        for e in event.getctrls():
            slicedevents.add(e)
            if e in self.branch2extend:
                slicedevents.add(self.branch2extend[e])
            if e in self.branch2execend:
                slicedevents.add(self.branch2execend[e])
            worklist.extend(e.getuses())

        while worklist:
            var = worklist.pop()
            processed.add(var)
            defevent = self.var2def[var]
            slicedevents.add(defevent)
            uses = defevent.getuses()
            for e in defevent.getctrls():
                slicedevents.add(e)
                if e in self.branch2extend:
                    slicedevents.add(self.branch2extend[e])
                if e in self.branch2execend:
                    slicedevents.add(self.branch2execend[e])
                uses.extend(e.getuses())
            for usevar in uses:
                if (usevar not in processed) and (usevar not in worklist):
                    worklist.append(usevar)
        events = list(slicedevents)
        events.sort(key=lambda e: e.gid)
        return events

    def fastslice(self, event):
        if event in self.event2slice:
            return self.event2slice[event]
        slicedevents = set()
        depevents = [e for e in event.getctrls()]
        depevents.extend(self.var2def[var] for var in event.getuses()
                         if var in self.var2def)
        slicedevents.update(depevents)
        for e in depevents:
            slicedevents.update(self.fastslice(e))
        brends = set()
        for e in slicedevents:
            if e.isbranch:
                if e in self.branch2extend:
                    brends.add(self.branch2extend[e])
                if e in self.branch2execend:
                    brends.add(self.branch2execend[e])
        slicedevents.update(brends)
        slicedevents = list(slicedevents)
        slicedevents.sort(key=lambda e: e.gid)
        self.event2slice[event] = slicedevents
        return slicedevents
