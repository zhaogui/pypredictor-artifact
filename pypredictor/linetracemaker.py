#!/usr/bin/python
# _*_ coding:utf-8 _*_

import __builtin__
import logging
from ast import *

import tracenodes as new
from basetracemaker import BaseTraceMaker
from builtintracemaker import BuiltinCallTraceMaker


logger = logging.getLogger('pytracer')


class LineTraceMakerMixIn(object):
    def new_allocate(self, tname, value=None):
        if isinstance(tname, Name):
            tname = tname.id
        target = self.aframe.new_id(tname, value)
        return new.Allocate(target, target.value)

    def new_assign(self, tname, vname):
        if isinstance(tname, Name):
            tname = tname.id
        if isinstance(vname, Name):
            vname = vname.id
        return new.Assign(self.new_id(tname), self.get_id(vname))

    def new_attrload(self, tname, vname, attr):
        if isinstance(tname, Name):
            tname = tname.id
        if isinstance(vname, Name):
            vname = vname.id
        return new.AttrLoad(self.new_id(tname), self.get_id(vname), attr)

    def new_attrstore(self, tname, attr, vname):
        if isinstance(tname, Name):
            tname = tname.id
        if isinstance(vname, Name):
            vname = vname.id
        return new.AttrStore(self.get_id(tname), attr, self.get_id(vname))

    def new_subload(self, tname, vname, idx):
        if isinstance(tname, Name):
            tname = tname.id
        if isinstance(vname, Name):
            vname = vname.id
        if isinstance(idx, Name):
            idx = idx.id
        tgt_id = self.new_id(tname)
        return new.SubLoad(tgt_id, self.get_id(vname), self.get_id(idx))

    def new_substore(self, tname, idx, tvalue):
        if isinstance(tname, Name):
            tname = tname.id
        if isinstance(tvalue, Name):
            tvalue = tvalue.id
        if isinstance(idx, Name):
            idx = idx.id
        return new.SubStore(self.get_id(tname), self.get_id(idx), self.get_id(tvalue))

    def new_bopassign(self, tname, vname1, vname2, bop):
        if isinstance(tname, Name):
            tname = tname.id
        if isinstance(vname1, Name):
            vname1 = vname1.id
        if isinstance(vname2, Name):
            vname2 = vname2.id
        return new.BopAssign(self.new_id(tname), self.get_id(vname1), self.get_id(vname2), bop)

    def new_uopassign(self, tname, vname, uop):
        if isinstance(tname, Name):
            tname = tname.id
        if isinstance(vname, Name):
            vname = vname.id
        return new.UopAssign(self.new_id(tname), self.get_id(vname), uop)

    def errorinfo(self, msg):
        return ('LineTrace- {0} [in <{1}> '
                'file: <{2}>]'.format(msg,
                                      self.aframe.funcname
                                      if self.aframe else None,
                                      self.aframe.filename
                                      if self.aframe else None))


class AssignTraceMakerMixIn(object):

    def default_make_assign(self, target, _):
        self.append_trace(self.new_allocate(target))

    def make_assign(self, target, value):   # must not be the target of a non-C function call
        method = 'make_{val}_assign_{tgt}'.format(val=value.__class__.__name__.lower(),
                                                  tgt=target.__class__.__name__.lower())
        maker = getattr(self, method, self.default_make_assign)
        maker(target, value)

    def make_tuple_assign_name(self, target, value):
        self.append_trace(self.new_allocate(target))
        for i, elt in enumerate(value.elts):
            self.append_trace(new.SubStore(self.get_id(target.id),
                                           self.new_tmpid(i),
                                           self.get_id(elt.id)))

    def make_list_assign_name(self, target, value):
        self.append_trace(self.new_allocate(target))
        for i, elt in enumerate(value.elts):
            self.append_trace(new.SubStore(self.get_id(target.id),
                                           self.new_tmpid(i),
                                           self.get_id(elt.id)))

    def make_set_assign_name(self, target, value):
        self.default_make_assign(target, value)

    def make_dict_assign_name(self, target, value):
        self.append_trace(self.new_allocate(target))
        for k, v in zip(value.keys, value.values):
            self.append_trace(self.new_substore(target, k, v))

    def make_name_assign_name(self, target, value):
        if value.id in ('True', 'False', 'None'):
            self.append_trace(self.new_allocate(target))
        else:
            self.append_trace(self.new_assign(target, value))

    def make_attribute_assign_name(self, target, value):
        self.append_trace(self.new_attrload(target, value.value, value.attr))

    def make_subscript_assign_name(self, target, value):
        if isinstance(value.slice, Index):
            idx = value.slice.value
            idx_val = self.aframe.liveobject(idx.id)
            if not isinstance(idx_val, __builtin__.slice):
                self.append_trace(self.new_subload(target, value.value, idx))
                return
        tgt_val = self.aframe.liveobject(target.id)
        self.append_trace(self.new_allocate(target))
        for idx, item in enumerate(tgt_val):
            self.append_trace(new.SubStore(self.get_id(target.id),
                                           self.new_tmpid(idx),
                                           self.new_tmpid(item)))

    def make_call_assign_name(self, target, value):
        self.make_call(target, value)

    def make_binop_assign_name(self, target, value):
        left_val, right_val = self.liveobject(value.left.id), self.liveobject(value.right.id)
        if isinstance(left_val, int) and \
                isinstance(right_val, int) and \
                new.BinOperator.acceptable(value.op):
            self.append_trace(self.new_bopassign(target, value.left, value.right, value.op))
        else:
            self.default_make_assign(target, value)

    def make_unaryop_assign_name(self, target, value):
        val = self.liveobject(value.operand.id)
        if isinstance(val, int) and new.UnaryOperator.acceptable(value.op):
            self.append_trace(self.new_uopassign(target, value.operand, value.op))
        else:
            self.default_make_assign(target, value)

    def make_compare_assign_name(self, target, value):
        isinttpye = isinstance(self.liveobject(value.left.id), int) \
            and isinstance(self.liveobject(value.comparators[0].id), int)
        if isinttpye or (isinstance(value.ops[0], (Eq, NotEq, Is, IsNot))):
            if new.BinOperator.acceptable(value.ops[0]):
                self.append_trace(self.new_bopassign(target, value.left,
                                                     value.comparators[0],
                                                     value.ops[0]))
        else:
            self.default_make_assign(target, value)

    def make_name_assign_attribute(self, target, value):
        self.append_trace(self.new_attrstore(target.value, target.attr, value))

    def make_name_assign_subscript(self, target, value):
        if isinstance(target.slice, Index):
            idx = target.slice.value
            idx_val = self.liveobject(idx.id)
            if not isinstance(idx_val, __builtin__.slice):
                self.append_trace(self.new_substore(target.value, idx, value))
                return
        logger.debug(self.errorinfo('does not handle assign subscript store slice'
                                    ' {0}={1}'.format(dump(target), dump(value))))


class LineTraceMaker(BaseTraceMaker,
                     LineTraceMakerMixIn,
                     AssignTraceMakerMixIn,
                     BuiltinCallTraceMaker):

    def make(self):
        self.results = []
        node = self.aframe.currstmt
        # check if this trace is out of the last branch
        while self.check_ifoutofbranch(self.aframe):
            self.post_branchend(self.aframe)

        # make trace for current stmt
        method = 'make_' + node.__class__.__name__.lower()
        if 'exec' in method:
            with open('exec.txt', 'a') as f:
                print >>f, 1
        maker = getattr(self, method, self.default_make)
        maker(node)
        self.bindinfo(self.results, self.aframe)
        return self.results

    def default_make(self, node):
        raise NotImplementedError(self.errorinfo('not implement this kind of '
                                                 'line make: {}'.format(dump(node))))

    # Statements
    def make_assign(self, node):
        super(LineTraceMaker, self).make_assign(node.targets[0], node.value)

    def make_augassign(self, node):
        # Need more specialization???
        # Assume the augassign does not change the type
        # return new.AugAssignStmt(target=self.visit(node.target),
        # value=self.visit(node.target),
        #                          op=new.Operator.make(node.op))
        tgt_id = node.target.id
        tgt_val = self.liveobject(tgt_id)
        augop = '__i{op}__'.format(op=str(new.BinOperator.make(node.op)).lower())
        if hasattr(tgt_val, augop):
            self.append_trace(self.new_allocate(tgt_id, new.HeapObject(tgt_val)))
        else:
            if isinstance(tgt_val, int) and new.BinOperator.acceptable(node.op):
                val0_id = '$' + node.target.id  # it's an intrinsic feature when transforming ast
                val1_id = node.value.id
                self.append_trace(self.new_bopassign(tgt_id, val0_id, val1_id, node.op))
            else:
                self.append_trace(self.new_allocate(tgt_id, new.HeapObject(tgt_val)))
        # raise RuntimeError(self.errorinfo('Should not come into this path '
        #                                   '<AugAssign> {0}'.format(dump(node))))

    def make_import(self, node):
        if node.names[0].asname:
            name = node.names[0].asname
        else:
            name = node.names[0].name
        tgt_id = str(name).split('.')[0]
        self.append_trace(self.new_allocate(tgt_id))

    def make_importfrom(self, node):
        for alias_ in node.names:
            name, asname = alias_.name, alias_.asname
            if name == '*':
                return self._handle_import_from_star(node)
            else:
                iname = asname if asname is not None else name
                self.append_trace(self.new_allocate(iname))

    def _handle_import_from_star(self, node):
        # handle it in the future
        pass

    def make_expr(self, node):
        if isinstance(node.value, Call):
            self.make_call_expr(node)
        else:
            self.make_other_expr(node)

    def make_call_expr(self, node):
        # this call must has not source code
        self.make_call(None, node.value)

    def make_call(self, target, call):
        func = call.func
        if isinstance(func, Name):
            self.append_trace(new.FunctionCall(self.get_id(func.id)))
        else:
            self.append_trace(new.MethodInvoke(self.get_id(func.value.id), func.attr))
        try:
            self.make_builtin_call(target, call)
        except NotImplementedError:
            if target is not None:
                self.append_trace(self.new_allocate(target))

    def make_other_expr(self, node):
        logger.debug(self.errorinfo('ignore the trace of {0}'.format(dump(node))))

    def make_functiondef(self, node):
        self.append_trace(self.new_allocate(node.name))

    def make_classdef(self, node):
        self.append_trace(self.new_allocate(node.name))

    def make_test(self, test_id, test_val):
        if test_val is None:
            tgt = self.new_id(None, new.HeapObject(False))
            tmp_tgt = self.new_id(None, new.HeapObject(True))
            self.append_trace(new.TypeAssume(tmp_tgt, self.get_id(test_id),
                                             self.new_tmpid(new.HeapObject(type(None)))))
            self.append_trace(new.UopAssign(tgt, tmp_tgt, Not()))
            branch = new.Branch(tgt, False)
        elif isinstance(test_val, (int, long)):
            branch = new.Branch(self.get_id(test_id), test_val)
        elif isinstance(test_val, str):  # for the future
            branch = new.Branch(self.new_tmpid(bool(test_val)), bool(test_val))
        # elif type(test_val) in (tuple, list, dict):
        #     tgt = self.new_id(None, new.HeapObject(bool(test_val)))
        #     self.append_trace(new.BopAssign(tgt, self.get_id(test_id),
        #                                     self.new_tmpid(new.HeapObject(test_val.__class__())), NotEq()))
        #     branch = new.Branch(tgt, False)
        elif hasattr(test_val, '__iter__'):  # for the future
            branch = new.Branch(self.new_tmpid(bool(test_val)), bool(test_val))
        else:  # for the future
            # TODO: need handle object with __nonzero__ method
            tgt = self.new_id(None, new.HeapObject(bool(test_val)))
            self.append_trace(new.BopAssign(tgt, self.get_id(test_id),
                                            self.new_tmpid(new.HeapObject(0)), NotEq()))
            branch = new.Branch(tgt, bool(test_val))
        return branch

    def make_if(self, node):
        branch = self.make_test(node.test.id, self.liveobject(node.test.id))
        self.append_trace(branch)

    def make_for(self, node):
        index_name = node.target.elts[0]
        tgt_name = node.target.elts[1]
        iter_name = node.iter.args[0]
        iterable = self.liveobject(iter_name.id)
        if iterable:
            if isinstance(self.liveobject(iter_name.id), (list, tuple)):    # do not include the 'set'
                # for ($1, $2) in enumerate(x):
                self.append_trace(self.new_allocate(index_name))
                self.append_trace(self.new_subload(tgt_name, iter_name, index_name))
            else:
                try:
                    self.append_trace(self.new_allocate(index_name))
                    self.append_trace(self.new_allocate(tgt_name))
                except ValueError:
                    pass

    def make_while(self, node):     # hard code, handle in the future
        branch = self.make_test(node.test.id, self.liveobject(node.test.id))
        self.append_trace(new.Allocate(branch.test, branch.test.value))

    def make_with(self, node):
        if node.optional_vars:
            self.append_trace(self.new_allocate(node.optional_vars))

    def make_excepthandler(self, node):
        try:
            if node.name is not None:
                self.append_trace(self.new_allocate(node.name))
        except ValueError:
            logger.debug(self.errorinfo('cannot find the value of '
                                        'except name <{0}>'.format(node.name)))
            return None

    def make_assert(self, node):
        pass

    def make_return(self, node):   # ignore
        pass

    def make_global(self, node):
        pass

    def make_delete(self, node):  # ignore
        pass

    def make_nonlocal(self, node):
        pass

    def make_tryfinally(self, node):
        pass

    def make_tryexcept(self, node):
        pass

    def make_raise(self, node):    # ignore
        pass

    def make_print(self, node):    # ignore
        pass

    def make_exec(self, node):     # ignore
        pass

    def make_pass(self, node):     # ignore
        pass

    def make_break(self, node):    # ignore
        pass

    def make_continue(self, node):     # ignore
        pass
