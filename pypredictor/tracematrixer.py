#!/usr/bin/python
# _*_ coding:utf-8 _*_

import logging
from StringIO import StringIO
from tracenodes import TraceType


logger = logging.getLogger('pytracer')


class TraceMatrixer(object):
    def __init__(self, tanker):
        self.tanker = tanker
        self.starttime = 0
        self.endtime = 0
        self.info = StringIO()

    def run(self):
        self.matrix_modules()
        self.matrix_events()
        self.matrix_time()
        return self.info.getvalue()

    def matrix_time(self):
        self.info.write('Tracing Time: {0}\n'.format(self.endtime - self.starttime))

    def matrix_modules(self):
        mod_num = len(self.tanker.mname2idx)
        self.info.write('Module number: {0}\n'.format(mod_num))

    def matrix_events(self):
        event_num = len(self.tanker.gid2event)
        # logger.info('The total events number: {0}'.format(event_num))
        alloc_num = ass_num = 0
        attrl_num = attrs_num = 0
        subl_num = subs_num = 0
        branch_num = phi_num = 0
        mthdik_num = funccall_num = 0
        ext_num = 0
        for event in self.tanker.gid2event.itervalues():
            ext_num += event['ext']
            tracetype = TraceType.geteventsig(event['type']).lower()
            if tracetype == 'allocate':
                alloc_num += 1
            elif tracetype == 'assign':
                ass_num += 1
            elif tracetype == 'attrload':
                attrl_num += 1
            elif tracetype == 'attrstore':
                attrs_num += 1
            elif tracetype == 'subload':
                subl_num += 1
            elif tracetype == 'substore':
                subs_num += 1
            elif tracetype == 'branch':
                branch_num += 1
            elif tracetype == 'phifunction':
                phi_num += 1
            elif tracetype == 'methodinvoke':
                mthdik_num += 1
            elif tracetype == 'functioncall':
                funccall_num += 1
        self.info.write('Attr Read Number: {0}\n'.format(attrl_num + subl_num))
        self.info.write('Attr Write Number: {0}\n'.format(attrs_num + subs_num))
        self.info.write('Predicates Number: {0}\n'.format(branch_num))
        self.info.write('Calls Number: {0}\n'.format(funccall_num + mthdik_num))
        self.info.write('Executed Events Number: {0}\n'.format(event_num - ext_num))


# class TraceMatrixer(object):
#     def __init__(self, tanker):
#         self.tanker = tanker
#
#     def run(self):
#         logger.info('============Trace Matrix============')
#         self.matrix_modules()
#         self.matrix_stmts()
#         self.matrix_events()
#         self.matrix_ptosets()
#         self.matrix_types()
#         self.matrix_attrsets()
#         self.matrix_attrs()
#         logger.info('=====================================')
#
#     def matrix_events(self):
#         event_num = len(self.tanker.gid2event)
#         logger.info('The total events number: {0}'.format(event_num))
#         alloc_num = ass_num = 0
#         attrl_num = attrs_num = 0
#         subl_num = subs_num = 0
#         branch_num = phi_num = 0
#         mthdik_num = funccall_num = 0
#         ext_num = 0
#         for event in self.tanker.gid2event.itervalues():
#             ext_num += event['ext']
#             tracetype = TraceType.geteventsig(event['type']).lower()
#             if tracetype == 'allocate':
#                 alloc_num += 1
#             elif tracetype == 'assign':
#                 ass_num += 1
#             elif tracetype == 'attrload':
#                 attrl_num += 1
#             elif tracetype == 'attrstore':
#                 attrs_num += 1
#             elif tracetype == 'subload':
#                 subl_num += 1
#             elif tracetype == 'substore':
#                 subs_num += 1
#             elif tracetype == 'branch':
#                 branch_num += 1
#             elif tracetype == 'phifunction':
#                 phi_num += 1
#             elif tracetype == 'methodinvoke':
#                 mthdik_num += 1
#             elif tracetype == 'functioncall':
#                 funccall_num += 1
#         logger.info('ExtTrace number: {0}'.format(ext_num))
#         logger.info('ExecTrace number: {0}'.format(event_num - ext_num))
#         logger.info('-------------------------')
#         logger.info('Allocate number: {0}'.format(alloc_num))
#         logger.info('Assign number: {0}'.format(ass_num))
#         logger.info('AttrLoad number: {0}'.format(attrl_num))
#         logger.info('AttrStore number: {0}'.format(attrs_num))
#         logger.info('SubLoad number: {0}'.format(subl_num))
#         logger.info('SubStore number: {0}'.format(subs_num))
#         logger.info('Branch number: {0}'.format(branch_num))
#         logger.info('PhiFunction number: {0}'.format(phi_num))
#         logger.info('MethodInvoke number: {0}'.format(mthdik_num))
#         logger.info('FunctionCall number: {0}'.format(funccall_num))
#
#     def matrix_ptosets(self):
#         var_num = len(self.tanker.var2ptosymset)
#         val_num = 0
#         max_setsize = 1
#         for pset in self.tanker.var2ptosymset.itervalues():
#             val_num += len(pset)
#             max_setsize = max(max_setsize, len(pset))
#         logger.info('Total Variable number: {0}'.format(var_num))
#         logger.info('Max size of PtoSet: {0}'.format(max_setsize))
#         logger.info('Average size of PtoSet: {0}'.format(float(val_num)/var_num))
#
#     def matrix_types(self):
#         type_num = len(self.tanker.type2idx)
#         logger.info('Type number: {0}'.format(type_num))
#
#     def matrix_attrsets(self):
#         attrset_num = len(self.tanker.id2attrset)
#         logger.info('Attr Set number: {0}'.format(attrset_num))
#
#     def matrix_attrs(self):
#         attr_num = len(self.tanker.attr2aid)
#         logger.info('Attr number: {0}'.format(attr_num))
#
#     def matrix_modules(self):
#         mod_num = len(self.tanker.mname2idx)
#         logger.info('Module number: {0}'.format(mod_num))
#
#     def matrix_stmts(self):
#         stmt_num = len(self.tanker.stmt2idx)
#         logger.info('Stmt number: {0}'.format(stmt_num))
