#!/usr/bin/python
# _*_ coding:utf-8 _*_

import inspect
from os.path import abspath, sep, join
from types import *
from threading import Thread


def encode_filename(filename):
    return str(filename).replace(sep, '#')


def getliveobject(frame, name):
    if name in frame.f_locals:
        return frame.f_locals[name]
    elif name in frame.f_globals:
        return frame.f_globals[name]
    else:
        builtins = frame.f_globals['__builtins__']
        if isinstance(builtins, dict) and (name in builtins):
            return builtins[name]
        elif isinstance(builtins, ModuleType) and hasattr(builtins, name):
            return getattr(frame.f_globals['__builtins__'], name)
    raise ValueError('cannot find the value of {name}'
                     ' in frame of {frame}'.format(name=name, frame=frame.f_code.co_name))


def getsourcefile(object_):
    if isinstance(object_, str):
        if str(object_[-4:]).lower() in ('.pyc', '.pyo'):
            return abspath(object_[:-4] + '.py')
        else:
            return abspath(object_)
    if inspect.isframe(object_):
        filename = inspect.getsourcefile(object_)
        return abspath(filename) if filename is not None else None
    else:
        return None


def is_function(callable_obj):
    return isinstance(callable_obj, FunctionType)


def is_bind_method(callable_obj):
    return isinstance(callable_obj, MethodType) \
        and (hasattr(callable_obj, '__self__') and (callable_obj.__self__ is not None))


def is_unbound_method(callable_obj):
    return isinstance(callable_obj, MethodType) \
        and (hasattr(callable_obj, '__self__') and (callable_obj.__self__ is None))


def is_class(callable_obj):
    return isinstance(callable_obj, ClassType)


def is_type(callable_obj):
    return isinstance(callable_obj, TypeType)


def is_instance(callable_obj):
    return isinstance(callable_obj, InstanceType) or \
           (hasattr(callable_obj, '__class__') and
            (not isinstance(callable_obj, FunctionType))
            and callable_obj.__class__.__name__ not in ('type', 'instancemethod'))


libpaths = {'lib-root': 'lib/python2.7',
            'lib-distpkgs': 'lib/python2.7/dist-packages',
            'lib-sitepkgs': 'lib/python2.7/site-packages'}


builtintypes = (bool, buffer, complex, dict, file, float, int,
                list, long, list, object, slice, str, tuple, unicode, xrange,
                NoneType, BuiltinMethodType, CodeType, DictProxyType,
                FrameType, GeneratorType, GetSetDescriptorType, LambdaType,
                MemberDescriptorType, MethodType, ModuleType,
                NotImplementedType, TracebackType,  UnboundMethodType)

# builtintypes = (BooleanType, BufferType, BuiltinFunctionType, BuiltinMethodType,
#                 ClassType, CodeType, ComplexType, DictProxyType, DictType,
#                 DictionaryType, EllipsisType, FileType, FloatType,
#                 FrameType, FunctionType, GeneratorType, GetSetDescriptorType,
#                 InstanceType, IntType, LambdaType, ListType, LongType,
#                 MemberDescriptorType, MethodType, ModuleType, NoneType,
#                 NotImplementedType, ObjectType, SliceType, StringType,
#                 StringTypes, TracebackType, TupleType, TypeType,
#                 UnboundMethodType, UnicodeType, XRangeType)


class Task(Thread):
    def __init__(self, queue, taskfunc):
        Thread.__init__(self)
        self.queue = queue
        self.taskfunc = taskfunc

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            taskparams = self.queue.get()
            self.taskfunc(taskparams)
            self.queue.task_done()

from collections import namedtuple
NewASTInfo = namedtuple('NewASTInfo', 'filename lineno2stmt newlineno2oldlineno')


preset_ignores = [
    join(libpaths['lib-root'], 're.py'),
    join(libpaths['lib-root'], 'hmac.py'),
    join(libpaths['lib-root'], 'threading.py'),
    join(libpaths['lib-root'], 'logging/'),
    join(libpaths['lib-root'], '_abcoll.py'),
    join(libpaths['lib-root'], '_weakrefset.py'),
    join(libpaths['lib-root'], 'sre_parse.py'),
    join(libpaths['lib-root'], 'sre_compile.py'),
    join(libpaths['lib-root'], 'collections.py'),
    join(libpaths['lib-root'], 'multiprocessing/'),
    join(libpaths['lib-root'], 'shlex.py'),
    join(libpaths['lib-root'], 'textwrap.py'),
    join(libpaths['lib-root'], 'encodings/'),
    join(libpaths['lib-sitepkgs'], 'six-1.8.0-py2.7.egg/six.py'),
    join(libpaths['lib-sitepkgs'], 'paramiko/'),
    join(libpaths['lib-sitepkgs'], 'yaml/'),
    join(libpaths['lib-sitepkgs'], 'chardet/'),
    join(libpaths['lib-sitepkgs'], 'Crypto/'),
    join(libpaths['lib-sitepkgs'], 'M2Crypto/'),
    join(libpaths['lib-sitepkgs'], 'zmq/'),
    join(libpaths['lib-sitepkgs'], 'pygments/lexers/'),
    join(libpaths['lib-root'], 'glob.py'),
    join(libpaths['lib-root'], 'fnmatch.py'),
    join(libpaths['lib-sitepkgs'], 'werkzeug/local.py'),
    join(libpaths['lib-sitepkgs'], 'werkzeug/datastructures.py'),
    join(libpaths['lib-sitepkgs'], 'jinja2'),
]
