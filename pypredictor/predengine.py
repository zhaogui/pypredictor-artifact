#!/usr/bin/python
# _*_ coding:utf-8 _*_


import time
import logging
from os.path import join
from multiprocessing import Process, Queue
from z3 import Solver, If, And, Not, Bool, Implies

from tracematrixer import TraceMatrixer
from z3encoder import Z3Encoder
from instparser import BranchInst, ExtBranchEndInst, ExecBranchEndInst
from tracenodes import TraceType
from dbengine import TraceDBReader


logger = logging.getLogger('pypredictor')
dlogger = logging.getLogger('z3consdumper')


def predict(projname, outdir='output', useslice=False, lazyencoding=False, start=0, stop=None, dump_trace_log=False):
    with TraceDBReader(projname, outdir) as reader:
        tanker = reader.read_all()
        matrixer = TraceMatrixer(tanker)
        trace_info = matrixer.run()
        if dump_trace_log:
            logger.info('---------trace matrix-----------')
            with open(join(outdir, 'TraceMatrix_' + projname + '.txt'), 'w') as f:
                for line in filter(bool, trace_info.split('\n')):
                    print >>f, line
                    logger.info(line)
            logger.info('--------------------------------')
        z3engine = PredictiveEngine(projname, outdir, tanker, useslice, lazyencoding)
        # starttime = time.time()
        z3engine.run(start, stop)
        # stoptime = time.time()
        # logger.info('the whole elapsed time: {0}s...'.format(stoptime - starttime))


class TypeInconsError(Exception):
    def __init__(self, message, solution):
        self.solution = solution
        super(TypeInconsError, self).__init__(message + ': ' + str(self.solution))


class PredictiveEngine(object):

    checktypes = (TraceType.ATTRLOAD, TraceType.SUBLOAD, TraceType.METHODINVOKE)
    branchkinds = (TraceType.BRANCH, TraceType.EXTBRANCHEND, TraceType.EXECBRANCHEND)

    def __init__(self, projname, outdir, tracetanker, useslice=False, lazyencoding=True):
        self.projname = projname
        self.outdir = outdir
        self.tanker = tracetanker
        self.z3encoder = Z3Encoder(tracetanker)
        self.event2cons = {}
        # for slice use
        self.useslice = useslice
        self.branch2extend = {}
        self.branch2execend = {}
        self.var2def = {}
        self.event2slice = {}
        # for matrix
        self.ext_event_counter = 0
        self.cons_counter = 0
        # for constructing branch stack
        self.branchstack = []
        # if encode the event lazily
        self.lazyencoding = lazyencoding
        # just for debugging
        self.dumpcons = False
        self._isdebug = logger.isEnabledFor(logging.DEBUG)
        # dlogger.isEnabledFor(logging.DEBUG)
        self._isdebugcons = False

    def run(self, start=0, stop=None):
        stop = stop if stop is not None else len(self.tanker.gid2event)
        logger.info('>>>>>>start checking...')
        starttime = time.time()
        for event in self.tanker.gid2event.itervalues():
            if start <= event.gid <= stop:
                try:
                    self.flowthrough(event)
                except TypeInconsError as error:
                    logger.info(error.message)
                    break
        stoptime = time.time()
        logger.info('Total Elapsed Time: {0}sec...'.format(stoptime - starttime))
        with open(join(self.outdir, 'PredictMatrix_' + self.projname + '.txt'), 'a') as f:
            print >>f, 'Total Elapsed Time: {0}sec...'.format(stoptime - starttime)

    def shoudcheck(self, event):
        if not event.type in self.checktypes:
            return False
        inextbranch = self.branchstack and (self.branchstack[-1].currbranch == 0)
        # only points to one possible object and in execbranch,
        # then no need to check
        return (len(event.inst.receivers) > 1) or inextbranch

    def flowthrough(self, event):
        if event.ext == 1:
            self.ext_event_counter += 1
        # ctrl and data dependence
        if self.branchstack:
            event.setctrls([self.branchstack[-1].branch])
        for var in event.getdefs():
            self.var2def[var] = event
        # maintain the branch stack
        esig = event.geteventsig().lower()
        if event.type in self.branchkinds:
            flowthrough = getattr(self, 'flowthrough_' + esig)
            flowthrough(event)
        # check the assertion if needed
        elif self.shoudcheck(event):
            self.apply_check(event)
        # if not use slice, we then encode the event,
        # otherwise we encode it lazily.
        if (not self.useslice) or (not self.lazyencoding):
            if self._isdebug:
                logger.debug('flow through[' + str(event.gid) +
                             ']<' + esig + '> ' + str(event))
            self.encode(event)
            if self._isdebug and event.gid == 100000:
                logger.debug('Reaching the Preset number of event.')
                logger.debug('------------------------------------------')
                import sys
                sys.exit(1)

    def apply_check(self, event):
        starttime = time.time()
        cons = self.pkgcons(event)
        r, m = self.solve(cons)
        stoptime = time.time()
        logger.info('Current Events Number: {0}'.format(event.gid))
        logger.info('Extended Events Number: {0}'.format(self.ext_event_counter))
        logger.info('Encoded Constraints Number: {0}'.format(self.cons_counter))
        logger.info('Solving Time: {0}sec...'.format(stoptime - starttime))
        if r:
            with open(join(self.outdir, 'PredictMatrix_' + self.projname + '.txt'), 'w') as f:
                print >>f, 'Current Events Number : {0}'.format(event.gid)
                print >>f, 'Extended Events Number: {0}'.format(self.ext_event_counter)
                print >>f, 'Encoded Constraints Number: {0}'.format(self.cons_counter)
                print >>f, 'Solving Time: {0}sec...'.format(stoptime - starttime)
            raise TypeInconsError('Status: <Inconsistency>',
                                  self.getsolution(m))
        else:
            logger.info('Status: <Pass>')
            logger.info('-----------------------------')

    def pkgcons(self, event):
        def appendcons(cons):
            if not isinstance(cons, list):
                cons = [cons]
            if branchstack:
                racybranch = branchstack[-1]
                if racybranch.currbranch:
                    racybranch.execbcons.extend(cons)
                else:
                    racybranch.extbcons.extend(cons)
            else:
                constraints.extend(cons)
            cons_num[0] += len(cons)
        if self.useslice:
            segevents = self.fastslice(event)
            if self.dumpcons:
                with open('sevents', 'w') as sefile:
                    for e in segevents:
                        print >>sefile, e
            if self.lazyencoding:
                self.encode_events(segevents)
        else:
            segevents = self.pkgevents(event)
        checkcons = self.encode_check(event)
        cons_num = [0]
        # constraints = list(self.initial_z3cons)
        constraints = list(self.z3encoder.typevalcons)
        branchstack = []
        if self._isdebugcons:
            dlogger.debug('=============Sliced Trace===========')
            for se in segevents:
                dlogger.debug('\t{0}'.format(se))
                dlogger.debug('\tEncoding: {}'.format([str(c).replace('\n', '').replace('  ', '')
                                                       for c in self.getcons(se)]))
            dlogger.debug('====================================')
        for se in segevents:
            if isinstance(se.inst, BranchInst):
                branchstack.append(AnalysisBranch(se))
            elif isinstance(se.inst, ExtBranchEndInst):
                assert branchstack, 'the branch stack should not be empty'
                racyanalbranch = branchstack[-1]
                assert racyanalbranch.branch.inst.bid == se.inst.bid, 'branch dismatch'
                racyanalbranch.currbranch = 1
            elif isinstance(se.inst, ExecBranchEndInst):
                assert branchstack, 'the branch stack should not be empty'
                analbranch = branchstack.pop()
                assert analbranch.branch.inst.bid == se.inst.bid, 'branch dismatch'
                testcons = self.event2cons[analbranch.branch][0]
                if analbranch.branch.inst.resolved:
                    appendcons(If(testcons, And(*analbranch.execbcons),
                                  And(*analbranch.extbcons) if analbranch.extbcons else True))
                else:
                    appendcons(And(testcons, *analbranch.execbcons))
            else:
                appendcons(self.getcons(se))
        appendcons(checkcons)
        for analbranch in branchstack:
            testcons = self.event2cons[analbranch.branch][0]
            if analbranch.currbranch:
                if analbranch.execbcons:
                    constraints.append(testcons)
                    constraints.extend(analbranch.execbcons)
            else:
                if analbranch.extbcons:
                    constraints.append(Not(testcons))
                    constraints.extend(analbranch.extbcons)
        # logger.info('-------------------------')
        if self.dumpcons:
            logger.info('Sliced Contraints number: {0}'.format(cons_num[0]))
            with open('cons', 'w') as f:
                print >>f, '\n'.join([str(c).replace('\n', '').replace('  ', '')
                                      for c in constraints])
        return constraints

    def pkgevents(self, event):
        nowevents = self.event2cons.keys()
        nowevents.sort(key=lambda e: e.gid)
        events = []
        for e in nowevents:
            if e.gid < event.gid:
                events.append(e)
            else:
                break
        events.sort(key=lambda e: e.gid)
        # logging.debug('++++++++++++++++++++++events number: {0}'.format(len(events)))
        return events

    def encode_events(self, events):
        for e in events:
            if e not in self.event2cons:
                self.encode(e)

    def encode(self, event):
        cons = self.z3encoder.encode(event)
        self.event2cons[event] = cons
        self.cons_counter += len(cons)
        if self._isdebugcons:
            dlogger.debug('\t[Enoding] {0} ---> {1}'.format(str(event.inst),
                [str(c).replace('\n', '').replace('  ', '') for c in cons]))

    def encode_check(self, event):
        cons = self.z3encoder.encode_check(event)
        self.cons_counter += len(cons)
        if self._isdebugcons:
            dlogger.debug('\t[Encoding Check] {0} ===>{1}'.format(str(event.inst),
                [str(c).replace('\n', '').replace('  ', '') for c in cons]))
        return cons

    def getcons(self, event):
        return self.event2cons.get(event, [])

    @staticmethod
    def timedsolve(cons, needmodel=True, timeout=None):
        def wrappedsolve(c, m, q):
            q.put(PredictiveEngine.solve(c, m))
        queue = Queue()
        p = Process(target=wrappedsolve, args=(cons, needmodel, queue))
        p.start()
        p.join(timeout=timeout)
        if p.is_alive():
            p.terminate()
            return [-1, None]
        return queue.get()

    def solve(self, cons, needmodel=True):
        z3solver = Solver()
        if self._isdebugcons:
            z3solver.set(unsat_core=True)
            namedcons = self._tagcons(cons)
            z3solver.add(*namedcons.values())
            r = z3solver.check(*map(lambda x: Bool(x), namedcons.keys()))
            if str(r) == 'sat':
                # import sys
                # for x in cons:
                #     print >>sys.stderr, str(x).replace('\n', '').replace('  ', '')
                return True, z3solver.model() if needmodel else None
            dlogger.debug('\n'.join([str(c).replace('\n', '').replace('  ', '')
                                    for c in self._unsat_core(z3solver, namedcons)]))
        else:
            z3solver.add(*cons)
            r = z3solver.check()
            if str(r) == 'sat':
                return True, z3solver.model() if needmodel else None
        # logger.info('<check-pass>')
        return False, None

    def _tagcons(self, cons):
        namedcons = {}
        for idx, con in enumerate(cons):
            label = 'PPPP' + str(idx)
            namedcons[label] = Implies(Bool(label), con)
        return namedcons

    def _unsat_core(self, solver, namedcons):
        core = map(lambda x: namedcons.get(str(x)), solver.unsat_core())
        return core

    def getsolution(self, z3model):
        # for x in z3model:
        #     if '\n' not in str(z3model[x]):
        #         print x, str(z3model[x]).replace('\n', ' ')
        # print z3model[Int('O23._cookies_#871')]
        # print z3model[Int('resp_#9723')]
        # print z3model[self.z3encoder.rosig2z3osigvar['TAO1358_#9963']]
        # print z3model[Int('$179_#993')]
        # print z3model[Int('$72_#3200')]

        # print z3model[self.z3encoder.getz3attrsetvar('TAO52_#77')]
        # print z3model[Int('$95_#131')]
        # from z3 import Int
        # print z3model[Int('policy_#1871')]

        sol = {}
        for var in self.z3encoder.infiniteinputz3vars:
            sol[str(var)] = z3model[var]
        for var, val, idx in self.z3encoder.finiteinputz3vars:
            if val == int(str(z3model[var])):
                sol[str(var)] = 'input candidate [' + str(idx) + ']'
        return sol

    def fastslice(self, event):
        if event in self.event2slice:
            return self.event2slice[event]
        slicedevents = set()
        depevents = [e for e in event.getctrls()]
        depevents.extend(self.var2def[var] for var in event.getuses()
                         if var in self.var2def)
        slicedevents.update(depevents)
        for e in depevents:
            slicedevents.update(self.fastslice(e))
        brends = set()
        for e in slicedevents:
            if e.isbranch:
                if e in self.branch2extend:
                    brends.add(self.branch2extend[e])
                if e in self.branch2execend:
                    brends.add(self.branch2execend[e])
        slicedevents.update(brends)
        slicedevents = list(slicedevents)
        slicedevents.sort(key=lambda e: e.gid)
        self.event2slice[event] = slicedevents
        return slicedevents

    def flowthrough_branch(self, event):
        if self._isdebug:
            logger.debug('Including branch<{0}>...'.format(event.inst.bid))
        self.branchstack.append(AnalysisBranch(event))

    def flowthrough_extbranchend(self, event):
        assert self.branchstack, 'the branch stack should not be empty'
        racyanalbranch = self.branchstack[-1]
        assert racyanalbranch.branch.inst.bid == event.inst.bid, 'branch dismatch'
        racyanalbranch.currbranch = 1
        self.branch2extend[racyanalbranch.branch] = event

    def flowthrough_execbranchend(self, event):
        assert self.branchstack, 'the branch stack should not be empty'
        racyanalbranch = self.branchstack[-1]
        assert racyanalbranch.branch.inst.bid == event.inst.bid, \
            'branch dismatch: ' + str(racyanalbranch.branch.inst.bid) + ', ' + str(event.inst.bid)
        self.branch2execend[racyanalbranch.branch] = event
        self.branchstack.pop()


class AnalysisBranch(object):
    def __init__(self, branch):
        self.branch = branch  # event
        self.execbcons = []
        self.extbcons = []
        # 1 represents current branch is the executed branch
        # else means in the extended branch
        self.currbranch = 0
