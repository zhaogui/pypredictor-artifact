#!/usr/bin/python
# _*_ coding:utf-8 _*_

import logging
from z3 import *

from tracenodes import TraceType


logger = logging.getLogger('pypredictor')


class Z3Encoder(object):
    # symzncbase = 1000000000000000
    symzncbase = 100000000

    def __init__(self, tkr):
        self.tanker = tkr
        self.tid2z3tval = {}
        self.var2z3var = {}
        self.var2z3typevar = {}
        self.rosig2z3osigvar = {}
        self.infiniteinputz3vars = []
        self.finiteinputz3vars = []
        self.z3solver = Solver()
        self.z3mro = K(IntSort(), False)
        # self.maxtid = max(self.tanker.type2idx.itervalues())
        self.maxtid = 1000
        self.constraints = []
        self.typevalcons = []

    def appendcons(self, *cons):
        self.constraints.extend(cons)

    def getz3val(self, val):
        if isinstance(val, int):
            return val
        else:
            if val.startswith('O'):
                return int(val[1:]) + self.symzncbase
            else:
                return int(val)

    def newz3var(self, var, z3type=Int):
        z3var = z3type(var)
        self.var2z3var[var] = z3var
        return z3var

    def getz3var(self, var):
        return self.var2z3var[var]

    def getornewtypeval(self, tid):
        if tid in self.tid2z3tval:
            return self.tid2z3tval[tid]
        for tsig, xtid in self.tanker.type2idx.iteritems():
            if tid == xtid:
                z3tval = Int('THVAL' + tsig)
                self.tid2z3tval[tid] = z3tval
                self.typevalcons.append(z3tval == tid)
                z3mro = Store(self.z3mro, self.z3hashtwo(tid, tid), True)
                if tid in self.tanker.tid2mro:
                    for stid in self.tanker.tid2mro[tid]:
                        z3mro = Store(z3mro, self.z3hashtwo(tid, stid), True)
                self.z3mro = z3mro
                return z3tval
        raise RuntimeError('{0} should not miss in tracetanker'.format(tid))

    def newz3typevar(self, var):
        tvar = Int('TH' + var)
        self.var2z3typevar[var] = tvar
        return tvar

    def getz3typevar(self, var):
        return self.var2z3typevar[var]

    def z3hashtwo(self, var1, var2):
        # map to an element of M * M matrix
        return (var1 - 1) * self.maxtid + var2

    def z3subtype(self, var1, var2):
        return self.z3mro[self.z3hashtwo(var1, var2)] == True

    def newz3attrset(self, attrsid):
        aset = self.tanker.id2attrset[attrsid]
        z3set = K(IntSort(), False)
        for aid in aset:
            z3set = Store(z3set, aid, True)
        return z3set

    def newz3attrsetvar(self, osig):
        z3array = Array(osig, IntSort(), BoolSort())
        self.rosig2z3osigvar[osig] = z3array
        return z3array

    def transz3attrset(self, oldsig, attr):
        z3oldattrsvar = self.getz3attrsetvar(oldsig)
        aid = self.tanker.attr2aid[attr]
        z3set = Store(z3oldattrsvar, aid, True)
        return z3set

    def getz3attrsetvar(self, osig):
        return self.rosig2z3osigvar[osig]

    def asserthasattr(self, attr, racyosig):
        # if racyosig in self.rosig2z3osigvar:
        #     z3attrsvar = self.getz3attrsetvar(racyosig)
        # else:
        #     logger.info('cannot find the z3 attrset var of ' + racyosig)
        #     z3attrsvar = self.newz3attrsetvar(racyosig)
        z3attrsvar = self.getz3attrsetvar(racyosig)
        aid = self.tanker.attr2aid[attr]
        return z3attrsvar[aid] == True

    @staticmethod
    def isarithop(op):
        return op in ('add', 'sub', 'mul', 'div', 'mod', 'pow', 'pos', 'neg')

    @staticmethod
    def iscompop(op):
        return op in ('eq', 'ne', 'lt', 'le', 'gt', 'ge')

    @staticmethod
    def isboolop(op):
        return op in ('and', 'or', 'not')

    def encode_typevals(self):
        self.constraints = []
        # initialize all the type value
        for tsig, tid in self.tanker.type2idx.iteritems():
            self.tid2z3tval[tid] = Int('THVAL' + tsig)
            self.appendcons(Int('THVAL' + tsig) == tid)
        z3mro = K(IntSort(), False)
        # add the subtype relations between these types
        for tid, mro in self.tanker.tid2mro.iteritems():
            z3mro = Store(z3mro, self.z3hashtwo(tid, tid), True)
            for stid in mro:
                z3mro = Store(z3mro, self.z3hashtwo(tid, stid), True)
        self.appendcons(self.z3mro == z3mro)
        return self.constraints

    def encode(self, event):
        esig = TraceType.geteventsig(event.type).lower()
        encodemthd = getattr(self, 'encode_' + esig, self.default_encode)
        return encodemthd(event.inst)

    def encode_check(self, event):
        esig = TraceType.geteventsig(event.type).lower()
        encodemthd = getattr(self, 'encode_' + esig + 'check')
        return encodemthd(event.inst)

    def default_encode(self, _):
        return ()

    def encode_allocate(self, inst):
        # y=1,o1_1
        self.constraints = []
        tgt = inst.target
        value, racyosig = inst.value
        tid, ctid, valsig, osig, attrsetid = self.tanker.var2eval[tgt]
        self.appendcons(self.newz3var(tgt) == self.getz3val(valsig))
        self.appendcons(self.newz3typevar(tgt) == self.getornewtypeval(tid))
        self.appendcons(self.newz3attrsetvar(racyosig) == self.newz3attrset(attrsetid))
        return self.constraints

    def encode_infiniterelaxallocate(self, inst):
        # y=o1,o1_1
        self.constraints = []
        tgt = inst.target
        value, racyosig = inst.value
        tid, ctid, valsig, osig, attrsetid = self.tanker.var2eval[tgt]
        self.appendcons(self.newz3var(tgt) == self.newz3var(osig))
        self.appendcons(self.newz3typevar(tgt) == self.getornewtypeval(tid))
        self.appendcons(self.newz3attrsetvar(racyosig) == self.newz3attrset(attrsetid))
        self.infiniteinputz3vars.append(self.getz3var(tgt))
        return self.constraints

    def encode_finiterelaxallocate(self, inst):
        # y=o1,o1_1|2,o2_1|...
        self.constraints = []
        tgt = inst.target
        pset = self.tanker.var2ptosymset[tgt]
        cons = []
        z3tgt = self.newz3var(tgt)

        for idx, item in enumerate(inst.values):
            valsig1, racyosig = item
            for tid, ctid, valsig2, osig, attrsetid in pset:
                if str(valsig1) == str(valsig2):
                    z3valsig = self.getz3val(valsig1)
                    self.finiteinputz3vars.append((z3tgt, z3valsig, idx))
                    cons.append(And(z3tgt == z3valsig,
                                    self.newz3typevar(tgt) == self.getornewtypeval(tid),
                                    self.newz3attrsetvar(racyosig) == self.newz3attrset(attrsetid)))
        self.appendcons(Or(*cons))
        return self.constraints

    def encode_assign(self, inst):
        # y=x
        self.constraints = []
        tgt, val = inst.target, inst.value
        self.appendcons(self.newz3var(tgt) == self.getz3var(val),
                        self.newz3typevar(tgt) == self.getz3typevar(val))
        return self.constraints

    def encode_attrload(self, inst):
        # y=x.f|x=o,o1?y1=o.f|...
        self.constraints = []
        hpass_asts = []
        for (vsig, rosig), value in inst.base2value.iteritems():
            if value != '':
                base_z3condi = self.newz3var(inst.basevar) == self.getz3val(vsig)
                try:
                    hasattr_z3condi = self.asserthasattr(inst.attr, rosig)
                except KeyError:    # cannot find the racyosig, means it comes from an infeasible path
                    logger.info('cannot find the corresponding racy object signature: ' + rosig)
                    continue
                ass_z3varcondi = self.newz3var(inst.target) == self.getz3var(value)
                ass_z3typecondi = self.newz3typevar(inst.target) == self.getz3typevar(value)
                hpass_asts.append(And(base_z3condi, hasattr_z3condi, ass_z3varcondi, ass_z3typecondi))
            else:
                # TODO: add assertion of the attribute set has the attribute f
                pass
        if hpass_asts:
            self.appendcons(Or(*hpass_asts) if len(hpass_asts) > 1 else hpass_asts[0])
        return self.constraints

    def encode_attrstore(self, inst):
        # y.f=x|y=o?o.f2=x:o.f1,o1->o2|...
        self.constraints = []
        hpass_asts = []
        items = zip(inst.base2target.keys(), inst.base2target.values(),
                    inst.base2preval.values(), inst.base2newosig.values())
        for (basevsig, baserosig), target, preval, newosig in items:
            try:
                z3condi = (self.newz3var(inst.basevar) == self.getz3val(basevsig))
                z3attrsetvar = self.newz3attrsetvar(newosig)
                z3attrcons1 = (z3attrsetvar == self.transz3attrset(baserosig, inst.attr))
                z3attrcons2 = (z3attrsetvar == self.getz3attrsetvar(baserosig))
                z3assenc_t = (self.newz3var(target) == self.getz3var(inst.value),
                              self.newz3typevar(target) == self.getz3typevar(inst.value))
                if preval != '':
                    z3assenc_f = (self.newz3var(target) == self.getz3var(preval),
                                  self.newz3typevar(target) == self.getz3typevar(preval))
                else:
                    z3assenc_f = [True]
                hpass_asts.append((z3condi, And(z3attrcons1, *z3assenc_t), And(z3attrcons2, *z3assenc_f)))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: '
                            '{0}, {1}, {2}'.format(baserosig, inst.value, preval))
                continue
        if len(hpass_asts) == 1:
            self.appendcons(And(hpass_asts[0][0], hpass_asts[0][1]))
        else:
            for z3condi, t_z3condi, f_z3condi in hpass_asts:
                self.appendcons(If(z3condi, t_z3condi, f_z3condi))
        return self.constraints

    def encode_subload(self, inst):
        # x=y[0]|y=o,o1?y1=o_0_1|...
        self.constraints = []
        hpass_asts = []
        for (vsig, rosig), value in inst.base2value.iteritems():
            if value != '':
                base_z3condi = self.newz3var(inst.basevar) == self.getz3val(vsig)
                try:
                    hasattr_z3condi = self.asserthasattr('__getitem__', rosig)
                    ass_z3varcondi = self.newz3var(inst.target) == self.getz3var(value)
                    ass_z3typecondi = self.newz3typevar(inst.target) == self.getz3typevar(value)
                except KeyError:
                    logger.info('cannot find the corresponding racy object signature: ' + rosig)
                    continue
                hpass_asts.append(And(base_z3condi, hasattr_z3condi, ass_z3varcondi, ass_z3typecondi))
            else:
                # TODO: add assertion of having attribute __getitem__
                pass
        self.appendcons(Or(*hpass_asts) if len(hpass_asts) > 1 else hpass_asts[0])
        return self.constraints

    def encode_substore(self, inst):
        #x[0]=y|x=o,o1?o_0_2=y:o_0_1|...
        self.constraints = []
        hpass_asts = []
        items = zip(inst.base2target.keys(), inst.base2target.values(), inst.base2preval.values())
        for (basevsig, baserosig), target, preval in items:
            try:
                z3condi = (self.newz3var(inst.basevar) == self.getz3val(basevsig))
                z3attrcons = self.asserthasattr('__getitem__', baserosig)
                z3assenc_t = (self.newz3var(target) == self.getz3var(inst.value),
                              self.newz3typevar(target) == self.getz3typevar(inst.value))
                if preval != '':
                    z3assenc_f = (self.newz3var(target) == self.getz3var(preval),
                                  self.newz3typevar(target) == self.getz3typevar(preval))
                else:
                    z3assenc_f = [True]
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: '
                            '{0}, {1}, {2}'.format(baserosig, inst.value, preval))
                continue
            hpass_asts.append((z3condi, And(z3attrcons, *z3assenc_t), And(*z3assenc_f)))

        if len(hpass_asts) == 1:
            self.appendcons(And(hpass_asts[0][0], hpass_asts[0][1]))
        else:
            for z3condi, t_z3condi, f_z3condi in hpass_asts:
                self.appendcons(If(z3condi, t_z3condi, f_z3condi))
        return self.constraints

    def encode_bopassign(self, inst):
        # z=x+y or z = x>y
        self.constraints = []
        target, value1, op, value2 = inst.target, inst.value1, inst.op, inst.value2
        tid = self.tanker.var2eval[target][0]
        left_z3var = self.newz3var(target)
        left_z3typevar = self.newz3typevar(target)
        if self.isboolop(op):
            if str(op) == 'or':
                right_z3condi = Or(self.getz3var(value1) == 1, self.getz3var(value2) == 1)
            else:
                right_z3condi = And(self.getz3var(value1) == 1, self.getz3var(value2) == 1)
        else:
            opsig = '__{}__'.format(str(op).lower())
            if hasattr(ArithRef, opsig):
                opmthd = getattr(ArithRef, opsig)
                right_z3condi = opmthd(self.getz3var(value1), self.getz3var(value2))
            else:
                raise AttributeError('cannot find the operation of {}'.format(opsig))
        if self.isarithop(op):
            ass_z3cons = (left_z3var == right_z3condi)
            ass_z3typecons = (left_z3typevar == self.getornewtypeval(tid))
            self.appendcons(And(ass_z3cons, ass_z3typecons))
        elif self.iscompop(op) or self.isboolop(op):
            tass_z3cons = (left_z3var == 1)
            fass_z3cons = (left_z3var == 0)
            ass_z3typecons = (left_z3typevar == self.getornewtypeval(tid))
            self.appendcons(If(right_z3condi,
                               And(tass_z3cons, ass_z3typecons),
                               And(fass_z3cons, ass_z3typecons)))
        return self.constraints

    def encode_uopassign(self, inst):
        # y=-x or y=not x
        self.constraints = []
        target, op, value = inst.target, inst.op, inst.value
        tid = self.tanker.var2eval[target][0]
        left_z3var = self.newz3var(target)
        left_z3typevar = self.newz3typevar(target)

        opsig = '__{}__'.format(str(op).lower())
        if self.isarithop(op):
            if hasattr(ArithRef, opsig):
                opmthd = getattr(ArithRef, opsig)
                right_z3condi = opmthd(self.getz3var(value))
            else:
                raise AttributeError('cannot find the operation of {}'.format(opsig))
            self.appendcons(And(left_z3var == right_z3condi,
                                left_z3typevar == self.getornewtypeval(tid)))
        else:
            right_z3condi = (self.getz3var(value) == 0)
            tass_z3cons = (left_z3var == 1)
            fass_z3cons = (left_z3var == 0)
            ass_z3typecons = (left_z3typevar == self.getornewtypeval(tid))
            self.appendcons(If(right_z3condi,
                               And(tass_z3cons, ass_z3typecons),
                               And(fass_z3cons, ass_z3typecons)))
        return self.constraints

    def encode_methodinvoke(self, inst):
        # call x.f|1,o1,o1_1|0,o2,o2_2|...
        self.constraints = []
        base, attr, basevalues = inst.basevar, inst.attr, inst.basevalues
        cons = []
        for flag, basevalue in basevalues:
            if flag:
                cons.append(self.newz3var(base) == self.getz3val(basevalue[0]))
        # osym = self.tanker.var2eval[base][2]
        # self.appendcons(self.newz3var(base) == self.getz3val(osym))
        # print inst, inst.basevalues
        if cons:
            self.appendcons(Or(*cons) if len(cons) > 1 else cons[0])
        return self.constraints

    def encode_functioncall(self, inst):
        #call f|o1,o1_1,|o2,o2_2...
        self.constraints = []
        osym = self.tanker.var2eval[inst.func][2]
        self.appendcons(self.newz3var(inst.func) == self.getz3val(osym))
        return self.constraints

    def encode_typeassume(self, inst):
        # t=x int
        self.constraints = []
        target, value, typevar = inst.target, inst.value, inst.type
        ctid = self.tanker.var2eval[typevar][1]
        assert ctid != -1, 'cannot find the type id of var ' + typevar
        test_z3condi = (self.getz3typevar(value) == self.getornewtypeval(ctid))
        tgt_z3var = self.newz3var(target)
        tass_z3condi, fass_z3condi = (tgt_z3var == 1), (tgt_z3var == 0)
        tgt_tid = self.tanker.var2eval[target][0]
        ass_z3typecondi = (self.newz3typevar(target) == self.getornewtypeval(tgt_tid))
        self.appendcons(If(test_z3condi, And(tass_z3condi, ass_z3typecondi),
                           And(fass_z3condi, ass_z3typecondi)))
        return self.constraints

    def encode_subtypeassume(self, inst):
        # t=x int
        self.constraints = []
        target, value, typevar = inst.target, inst.value, inst.type
        ctid = self.tanker.var2eval[typevar][1]
        assert ctid != -1, 'cannot find the type id of var ' + typevar
        test_z3condi = self.z3subtype(self.getz3typevar(value), self.getornewtypeval(ctid))
        tgt_z3var = self.newz3var(target)
        tass_z3condi, fass_z3condi = (tgt_z3var == 1), (tgt_z3var == 0)
        tgt_tid = self.tanker.var2eval[target][0]
        ass_z3typecondi = (self.newz3typevar(target) == self.getornewtypeval(tgt_tid))
        self.appendcons(If(test_z3condi,
                           And(tass_z3condi, ass_z3typecondi),
                           And(fass_z3condi, ass_z3typecondi)))
        return self.constraints

    def encode_hasattrassume(self, inst):
        #t='f' x|o1->o1_1, o2->o2_1...
        self.constraints = []
        target, attr, valuevar = inst.target, inst.attr, inst.valuevar

        tgt_z3var = self.newz3var(target)
        tgt_tid = self.tanker.var2eval[target][0]
        ass_z3typecondi = (self.newz3typevar(target) == self.getornewtypeval(tgt_tid))
        z3cons = []
        for osig, racyosig in inst.values:
            base_z3condi = self.newz3var(valuevar) == self.getz3val(osig)
            try:
                hasattr_z3condi = self.asserthasattr(attr, racyosig)
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: ' + racyosig)
                continue
            z3cons.append(And(base_z3condi, hasattr_z3condi))

        tass_z3condi, fass_z3condi = (tgt_z3var == 1), (tgt_z3var == 0)
        self.appendcons(If(Or(*z3cons) if len(z3cons) > 1 else z3cons[0],
                           And(tass_z3condi, ass_z3typecondi),
                           And(fass_z3condi, ass_z3typecondi)))
        return self.constraints

    def encode_phifunction(self, inst):
        # 1|x?x3=x1:x2 or 0|o_3=o_1:o_2
        self.constraints = []
        vardef, condi, ass_left = inst.isvardef, inst.test, inst.target
        tass_right, fass_right = inst.tvalue, inst.fvalue
        test_z3condi = self.getz3var(condi) != 0
        if int(vardef) == 1:
            try:
                true_z3condi = And(self.newz3var(ass_left) == self.getz3var(tass_right),
                                   self.newz3typevar(ass_left) == self.getz3typevar(tass_right))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: '
                            '{0}, {1}'.format(condi, tass_right))
                try:
                    false_z3condi = And(self.newz3var(ass_left) == self.getz3var(fass_right),
                                        self.newz3typevar(ass_left) == self.getz3typevar(fass_right))
                    self.appendcons(And(Not(test_z3condi), false_z3condi))
                    return self.constraints
                except KeyError:
                    logger.info('cannot find the corresponding racy object signature: '
                                '{0}, {1}, {2}'.format(condi, tass_right, fass_right))
                    return self.constraints
            try:
                false_z3condi = And(self.newz3var(ass_left) == self.getz3var(fass_right),
                                    self.newz3typevar(ass_left) == self.getz3typevar(fass_right))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: '
                            '{0}, {1}'.format(condi, fass_right))
                self.appendcons(And(test_z3condi, true_z3condi))
                return self.constraints

            self.appendcons(If(test_z3condi, true_z3condi, false_z3condi))
            return self.constraints
        else:
            z3attrsetvar = self.newz3attrsetvar(ass_left)
            try:
                true_z3condi = (z3attrsetvar == self.getz3attrsetvar(tass_right))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: '
                            '{0}, {1}'.format(condi, tass_right))
                try:
                    false_z3condi = (z3attrsetvar == self.getz3attrsetvar(fass_right))
                    self.appendcons(And(Not(test_z3condi), false_z3condi))
                    return self.constraints
                except KeyError:
                    logger.info('cannot find the corresponding racy object signature: '
                                '{0}, {1}, {2}'.format(condi, tass_right, fass_right))
                    return self.constraints
            try:
                false_z3condi = (z3attrsetvar == self.getz3attrsetvar(fass_right))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: '
                            '{0}, {1}'.format(condi, fass_right))
                self.appendcons(And(test_z3condi, true_z3condi))
                return self.constraints

            self.appendcons(If(test_z3condi, true_z3condi, false_z3condi))
            return self.constraints

    def encode_branch(self, inst):
        self.constraints = []
        # Branch<1>|x|1|1
        test, execb = inst.test, inst.execbranch
        if int(execb):
            self.appendcons(self.getz3var(test) != 0)
        else:
            self.appendcons(self.getz3var(test) == 0)
        return self.constraints

    # ----------------------------------------------
    # encode the checking constraints
    def encode_attrloadcheck(self, inst):
        # y=x.f|x=o,o1?y1=o.f|...
        self.constraints = []
        hpass_asts = []
        for vsig, rosig in inst.base2value:
            base_z3condi = self.newz3var(inst.basevar) == self.getz3val(vsig)
            try:
                nothasattr_z3condi = Not(self.asserthasattr(inst.attr, rosig))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: ' + rosig)
                continue
            hpass_asts.append(And(base_z3condi, nothasattr_z3condi))
        self.appendcons(Or(*hpass_asts) if len(hpass_asts) > 1 else hpass_asts[0])
        return self.constraints

    def encode_subloadcheck(self, inst):
        # x=y[0]|y=o,o1?y1=o_0_1|...
        self.constraints = []
        hpass_asts = []
        for (vsig, rosig), value in inst.base2value.iteritems():
            base_z3condi = self.newz3var(inst.basevar) == self.getz3val(vsig)
            try:
                nothasattr_z3condi = Not(self.asserthasattr('__getitem__', rosig))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: ' + rosig)
                continue
            hpass_asts.append(And(base_z3condi, nothasattr_z3condi))
        self.appendcons(Or(*hpass_asts) if len(hpass_asts) > 1 else hpass_asts[0])
        return self.constraints

    def encode_substorecheck(self, inst):
        #x[0]=y|x=o,o1?o_0_2=y:o_0_1|...
        self.constraints = []
        hpass_asts = []
        items = zip(inst.base2target.keys(), inst.base2target.values(), inst.base2preval.values())
        for (basevsig, baserosig), target, preval in items:
            base_z3condi = self.newz3var(inst.basevar) == self.getz3val(basevsig)
            try:
                nothasattr_z3condi = Not(self.asserthasattr('__setitem__', baserosig))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: ' + baserosig)
                continue
            hpass_asts.append(And(base_z3condi, nothasattr_z3condi))
        self.appendcons(Or(*hpass_asts) if len(hpass_asts) > 1 else hpass_asts[0])
        return self.constraints

    def encode_methodinvokecheck(self, inst):
        # call x.f|o1,o1_1|o2,o2_2|...
        self.constraints = []
        cons = []
        for flag, (osym, rosig) in inst.basevalues:
            base_z3condi = self.newz3var(inst.basevar) == self.getz3val(osym)
            try:
                nothasattr_z3condi = Not(self.asserthasattr(inst.attr, rosig))
            except KeyError:
                logger.info('cannot find the corresponding racy object signature: ' + rosig)
                continue
            cons.append(And(base_z3condi, nothasattr_z3condi))
        self.appendcons(Or(*cons) if len(cons) > 1 else cons[0])
        return self.constraints

    def encode_branchcheck(self, inst):
        self.constraints = []
        test, execb = inst.test, inst.execbranch
        self.appendcons(self.getz3var(test) != int(execb))
        return self.constraints

    # def encode_functioncallcheck(self, inextbranch, inst):
    #     #call f|o1,o1_1,|o2,o2_2...
    #     return []