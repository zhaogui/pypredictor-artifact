#!/usr/bin/python
# _*_ coding:utf-8 _*_


import tracenodes as new
from ast import Name


class BaseTraceMaker(object):
    def __init__(self, aframe):
        self.aframe = aframe
        self.backaframe = aframe.back
        self.curreframe = aframe.eframe
        self.results = []

    def liveobject(self, name):
        if isinstance(name, Name):
            name = name.id
        return self.aframe.liveobject(name)

    def new_id(self, name, value=None):
        if isinstance(name, Name):
            name = name.id
        return self.aframe.new_id(name, value)

    def new_tmpid(self, value):
        if not isinstance(value, new.HeapObject):
            value = new.HeapObject(value)
        alloc = new.Allocate(self.new_id(None, value), value)
        self.append_trace(alloc)
        return alloc.target

    def get_id(self, name, value=None):
        if isinstance(name, Name):
            name = name.id
        racyid = self.aframe.get_racyid(name)
        if racyid is None:
            if value is None:
                value = new.HeapObject(self.liveobject(name))
            else:
                assert isinstance(value, new.HeapObject), \
                    '{} must be instance of heapobject'.format(value)
            racyid = self.new_id(name, value)
            self.append_trace(new.Allocate(racyid, value))
        return racyid

    def append_trace(self, trace):
        if trace:
            # self.encode_heap(trace)
            self.results.append(trace)

    def bindinfo(self, traces, aframe):
        for trace in traces:
            trace.setinfo(aframe.filename, aframe.currstmt,
                          aframe.currlineno, aframe.curroldlineno)

    def errorinfo(self, msg):
        return ('TraceMaker- {0} [in <{1}> '
                'file: <{2}>]'.format(msg,
                                      self.aframe.funcname
                                      if self.aframe else None,
                                      self.aframe.filename
                                      if self.aframe else None))

    @staticmethod
    def check_ifoutofbranch(aframe):
        # if aframe.branchstack:
        #     import sys
        #     print >>sys.stderr, aframe.currstmt.lineno, aframe.branchstack[-1].laststmtlineno
        return aframe.branchstack and \
            aframe.currstmt.lineno > aframe.branchstack[-1].laststmtlineno

    def post_branchend(self, aframe):
        racyb_branch = aframe.branchstack.pop()
        self.append_trace(new.ExecBranchEnd(racyb_branch))
        execbname2racyid = racyb_branch.local_execb_name2racyid
        extbname2racyid = racyb_branch.local_extb_name2racyid
        if racyb_branch.resolved:
            execnames = set(execbname2racyid.iterkeys())
            extnames = set(extbname2racyid.iterkeys())
            commnames = execnames.intersection(extnames)
            subexecnames = execnames - commnames
            subextnames = extnames - commnames
            # for common names (appear in both exec and extend branches)
            for name in commnames:
                execid = execbname2racyid[name]
                extid = extbname2racyid[name]
                if racyb_branch.execbranch:
                    phi = new.PhiFunction(aframe.new_id(name),
                                          execid, extid, racyb_branch.test)
                else:
                    phi = new.PhiFunction(aframe.new_id(name),
                                          extid, execid, racyb_branch.test)
                self.append_trace(phi)
            for name in subexecnames:
                newid = execbname2racyid[name]
                oldid = aframe.get_racyid(name)
                if oldid:
                    if racyb_branch.execbranch:
                        phi = new.PhiFunction(aframe.new_id(name),
                                              newid, oldid, racyb_branch.test)
                    else:
                        phi = new.PhiFunction(aframe.new_id(name),
                                              oldid, newid, racyb_branch.test)
                    self.append_trace(phi)
            for name in subextnames:
                newid = extbname2racyid[name]
                oldid = aframe.get_racyid(name)
                if oldid:
                    if racyb_branch.execbranch:
                        phi = new.PhiFunction(aframe.new_id(name),
                                              oldid, newid, racyb_branch.test)
                    else:
                        phi = new.PhiFunction(aframe.new_id(name),
                                              newid, oldid, racyb_branch.test)
                    self.append_trace(phi)
        else:
            for name in execbname2racyid:
                aframe.update_id(name, execbname2racyid[name])