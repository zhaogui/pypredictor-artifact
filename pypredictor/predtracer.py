#!/usr/bin/python
# _*_ coding:utf-8 _*_

import time
import logging
from os import makedirs
from os.path import exists, expanduser, join, realpath
from dbengine import TraceDBWriter
from tracematrixer import TraceMatrixer
from traceanalyzer import TraceAnalyzer
from dbengine import TraceTanker

logger = logging.getLogger('pytracer')


class PredictiveTracer(object):
    def __init__(self, appname, outdir='output'):
        self.appname = appname
        self.outdir = outdir if outdir else expanduser('~')
        if not exists(self.outdir):
            makedirs(self.outdir)
        self.tracetanker = TraceTanker()
        self.writer = TraceDBWriter(appname, self.tracetanker, self.outdir)
        self.matrixer = TraceMatrixer(self.tracetanker)
        self.traceanalyzer = TraceAnalyzer(self.tracetanker)

    def starttiming(self):
        self.matrixer.starttime = time.time()

    def stoptiming(self):
        self.matrixer.endtime = time.time()

    def __enter__(self):
        self.writer.__enter__()
        return self

    def __exit__(self, ex_type, *_):
        if not ex_type:
            info = self.matrixer.run()
            logger.info('---------tracing matrix-----------')
            for line in filter(bool, info.split('\n')):
                logger.info(line)
            logger.info('----------------------------------')
            with open(join(self.outdir, 'TraceMatrix_' + self.appname + '.txt'), 'w')as f:
                print >>f, info
            self.writer.__exit__(ex_type, *_)

    def flowthrough(self, why, aframe, events):
        aevents = self.traceanalyzer.flowthrough(why, aframe, events)
        self.tracetanker.save_events(aevents)
