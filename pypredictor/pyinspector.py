#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys


try:
    import threading
except ImportError:
    _settrace = sys.settrace

    def _unsettrace():
        sys.settrace(None)
else:
    def _settrace(func):
        threading.settrace(func)
        sys.settrace(func)

    def _unsettrace():
        sys.settrace(None)
        threading.settrace(None)


class PyInspector(object):
    def __init__(self, analyzer):
        self.analyzer = analyzer

    def inspect_call(self, frame, event, arg):
        dolocalinspt = self.analyzer.handle_call(frame, event, arg)
        return self.inspect_all if dolocalinspt else None

    def inspect_line(self, frame, event, arg):
        self.analyzer.handle_line(frame, event, arg)
        return self.inspect_all

    def inspect_return(self, frame, event, arg):
        self.analyzer.handle_return(frame, event, arg)
        return self.inspect_all

    def inspect_except(self, frame, event, arg):
        self.analyzer.handle_except(frame, event, arg)
        return self.inspect_all

    def inspect_all(self, frame, event, arg):
        if event == 'call':
            r = self.inspect_call(frame, event, arg)
        elif event == 'line':
            r = self.inspect_line(frame, event, arg)
        elif event == 'return':
            r = self.inspect_return(frame, event, arg)
        elif event == 'exception':
            r = self.inspect_except(frame, event, arg)
        else:
            raise RuntimeError('Unhandled type of event: ' + event)
        return r

    def __enter__(self):
        self.analyzer.preset()
        _settrace(self.inspect_all)
        return self

    def __exit__(self, *_):
        _unsettrace()
        self.analyzer.postset()