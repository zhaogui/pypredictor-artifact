#!/usr/bin/python
# _*_ coding:utf-8 _*_

import __builtin__
import sys
import inspect
import os
import datetime
import time
import logging

import oribuiltin
from utils import getsourcefile, preset_ignores


__all__ = ['record']

logger = logging.getLogger('pyrecorder')

try:
    import threading
except ImportError:
    _settrace = sys.settrace

    def _unsettrace():
        sys.settrace(None)
else:
    def _settrace(func):
        threading.settrace(func)
        sys.settrace(func)

    def _unsettrace():
        sys.settrace(None)
        threading.settrace(None)


class ModuleRecorder(object):
    def __init__(self, ignores=None, output=sys.stdout):
        self.modules = set()
        self.ignores = self.init_ignores(ignores or [])
        self.notignorecache = set()
        self.ignorecache = set()
        self.ignoreframes = set()
        self.init_ignores(ignores)
        self.output = output

    def init_ignores(self, ignores):
        return filter(bool, set(ignores + preset_ignores))

    def trace_all(self, frame, event, arg):
        if event == 'call':
            self.trace_call(frame)
        elif event == 'return':
            self.trace_return(frame)

    def trace_call(self, frame):
        filename = getsourcefile(frame)
        if filename:
            if self.check_ignore(filename, frame.f_code.co_name):
                self._unsetimport()
                self.ignoreframes.add(frame)
            elif frame.f_back and (frame.f_back in self.ignoreframes):
                self._unsetimport()
                self.ignoreframes.add(frame)
            elif not os.path.exists(filename):  # fixed: when loading .egg archive file.
                logger.warning('cannot find the module {0}'.format(filename))
                self._unsetimport()
                self.ignoreframes.add(frame)
            else:
                self.modules.add(filename)
        else:
            logger.debug('cannot locate the file {0} '
                         'called by <{1}> in line[{2}] file <{3}>'.format(frame.f_code.co_filename,
                                                                          frame.f_back.f_code.co_name,
                                                                          frame.f_back.f_lineno,
                                                                          frame.f_back.f_code.co_filename))

    def trace_return(self, frame):
        if frame in self.ignoreframes:
            self._setimport()
            self.ignoreframes.remove(frame)

    def check_ignore(self, filename, funcname):
        fullfuncname = (filename + '@' + funcname)
        if fullfuncname in self.notignorecache:
            return False
        if fullfuncname in self.ignorecache:
            return True
        for ignore in self.ignores:
            if ignore in fullfuncname:
                self.ignorecache.add(fullfuncname)
                return True
        self.notignorecache.add(fullfuncname)
        return False

    def _setimport(self):
        def newimport(name, globals={}, locals={}, fromlist=[], level=-1):
            module = oribuiltin.__import__(name, globals, locals, fromlist, level)
            try:
                _unsettrace()
                if module is not None:
                    filename = inspect.getsourcefile(module)
                    if filename is not None:
                        fullpath = os.path.abspath(filename)
                        if not self.check_ignore(fullpath, '<module>'):
                            self.modules.add(fullpath)
            except TypeError:
                pass
            finally:
                _settrace(self.trace_all)
            # if hasattr(module, '__file__'):
            #     filename = module.__file__
            #     if str(filename[-4:]).lower() in ('.pyc', '.pyo'):
            #         filename = filename[:-4] + '.py'
            #     self.modules.add(filename)
            return module
        __builtin__.__import__ = newimport

    def _unsetimport(self):
        __builtin__.__import__ = oribuiltin.__import__

    def __enter__(self):
        self._setimport()
        self.start = time.time()
        _settrace(self.trace_all)
        return self

    def __exit__(self, *_):
        _unsettrace()
        self.elapsed = time.time() - self.start
        self._unsetimport()
        currfile = os.path.abspath(__file__)
        try:
            self.modules.remove(currfile[:-1] if currfile.endswith('c') else currfile)
        except KeyError:
            pass
        self.modules.add(os.path.join(os.path.dirname(currfile), 'simbuiltin.py'))
        self.modules = {os.path.abspath(m) for m in self.modules}
        for m in self.modules:
            print >>self.output, os.path.abspath(m)


def record(script_filepath, output_filepath, ignores):
    if not os.path.exists(os.path.dirname(output_filepath)):
        os.makedirs(os.path.dirname(output_filepath))
    try:
        with open(output_filepath, 'w') as output:
            with open(script_filepath) as fp:
                code = compile(fp.read(), script_filepath, 'exec')
            # try to emulate __main__ namespace as much as possible
            globs = {
                '__file__': script_filepath,
                '__name__': '__main__',
                '__package__': None,
                '__cached__': None,
            }
            with ModuleRecorder(ignores, output) as recorder:
                exec code in globs, globs
            logger.info('the total module number: {0}'.format(len(recorder.modules)))
            logger.info('record finished, time elapsed: {}'.format(recorder.elapsed))
    except IOError, err:
        logger.error("Cannot run file {0} because: {1}".format(script_filepath, err))
        sys.exit(1)
    except SystemExit:
        pass


if __name__ == '__main__':
    timesuffix = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    rfilename = 'tmp/all-modules' + timesuffix +'.txt'
    record('tests/modrecoder_test.py', rfilename)





