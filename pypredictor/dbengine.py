#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sqlite3
import os.path
import logging
import threading
import ast
from time import sleep

from instparser import InstParser
from analysisnodes import AnalysisEvent


logger = logging.getLogger('pytracer')


class TraceTanker(object):
    thid = mid = sid = gid = tid = aid = attrsetid = 1L

    def __init__(self):
        self.var2eval = {}      # variable to value
        self.var2ptosymset = {} # variable to points-to set(symbol objects)
        self.thd2idx = {}       # thread signature to index
        self.mname2idx = {}     # module name to index
        self.stmt2idx = {}      # syntax statement to index
        self.type2idx = {}      # type signature to index
        self.tid2mro = {}           # type index to mro
        self.biinsttid2attrsetid = {}   # cache for speeding up save attrs of builtin type instances
        self.bitypetid2attrsetid = {}   # cache for speeding up saving attrs of builtin types
        self.tid2attrsetids = {}    # type id to attribute-set indice
        self.id2attrset = {}        # index to attribute-set
        self.attr2aid = {}          # attribute signature to index
        self.gid2event = {}         # gid to event

    # -------------------------------------------------------------------
    # save methods
    def save_thread(self, thsig):
        # thsig -> thid
        if thsig in self.thd2idx:
            return self.thd2idx[thsig]
        thid = self.__class__.thid
        self.__class__.thid += 1
        self.thd2idx[thsig] = thid
        return thid

    def save_mod(self, fname):
        # mname -> mid
        if fname in self.mname2idx:
            return self.mname2idx[fname]
        mid = self.__class__.mid
        self.__class__.mid += 1
        self.mname2idx[fname] = mid
        return mid

    def save_stmt(self, mname, ol, nl, stmt):
        # mid|ol|nl|stmt -> sid
        mid = self.save_mod(mname)
        spack = (mid, ol, nl, stmt)
        if spack in self.stmt2idx:
            return self.stmt2idx[spack]
        sid = self.__class__.sid
        self.__class__.sid += 1
        self.stmt2idx[spack] = sid
        return sid

    def save_events(self, events):
        for t in events:
            self.save_event(t)

    def save_event(self, event):
        # gid -> gid, thid, sid, inst, type
        self.gid2event[event.gid] = event.to_dict()
        # self.execute("INSERT INTO {tname} "
        #              "VALUES ({gid}, {thid}, {sid}, "
        #              "'{taddr}', '{tval}', {ttid}, "
        #              "'{op}', "
        #              "'{opd1addr}', '{opd1val}', {opd1tid}, "
        #              "'{opd2addr}', '{opd2val}', {opd2tid}, "
        #              "{type})".format(tname=self.tracetabname, gid=gid, thid=0, sid=sid, **trace), False)

    def save_mro(self, tid, mro):
        supertids = [self.save_self(t) for t in mro]
        if tid not in self.tid2mro:
            self.tid2mro[tid] = supertids

    def save_self(self, value):
        tsig = '<{tname}|{toid}>'.format(tname=value.name, toid=value.oid)
        if tsig not in self.type2idx:
            tid = self.__class__.tid
            self.__class__.tid += 1
            self.type2idx[tsig] = tid
            self.save_mro(tid, value.mro)
        else:
            tid = self.type2idx[tsig]
        return tid

    def save_type(self, value):
        # tsig -> mro, tid
        tsig = '<{tname}|{toid}>'.format(tname=value.tname, toid=value.typeid)
        if tsig not in self.type2idx:
            tid = self.__class__.tid
            self.__class__.tid += 1
            self.type2idx[tsig] = tid
            self.save_mro(tid, value.mro)
        else:
            tid = self.type2idx[tsig]
        return tid

    def save_attrset(self, tid, value, istype):
        # attrsetid -> 0|1|2|...
        if istype:
            if tid in self.bitypetid2attrsetid:
                return self.bitypetid2attrsetid[tid]
        else:
            if tid in self.biinsttid2attrsetid:
                return self.biinsttid2attrsetid[tid]

        taids = self.save_attrs(value.attrs)
        if tid in self.tid2attrsetids:
            for attrsetid in self.tid2attrsetids[tid]:
                if taids == self.id2attrset[attrsetid]:
                    return attrsetid
            else:
                attrsetid = self.__class__.attrsetid
                self.__class__.attrsetid += 1
                self.id2attrset[attrsetid] = taids
                self.tid2attrsetids[tid].add(attrsetid)
                return attrsetid
        else:
            attrsetid = self.__class__.attrsetid
            self.__class__.attrsetid += 1
            self.id2attrset[attrsetid] = taids
            self.tid2attrsetids[tid] = {attrsetid}
            if value.isbuiltintypeinst:
                self.biinsttid2attrsetid[tid] = attrsetid
            elif value.isbuiltintype:
                self.bitypetid2attrsetid[tid] = attrsetid
            return attrsetid

    def get_allvalsig(self, value):
        ctid = ''
        if value.istype:
            ctid = self.save_self(value)
            tid = self.save_type(value)
            attrsid = self.save_attrset(ctid, value, True)
        else:
            tid = self.save_type(value)
            attrsid = self.save_attrset(tid, value, False)
        # valsig = '{tid},{ctid},' \
        #          '{val},{attrsid}'.format(ctid=ctid, tid=tid, attrsid=attrsid,
        #                                   val=value.symstr if sym else value.valstr)
        # valsig = ','.join(x for x in (str(tid), str(ctid),
        #                               value.valstr, value.symstr,
        #                               str(attrsid)) if x != '')
        # return valsig
        return tid, ctid, value.valstr, value.symstr, attrsid

    def save_var(self, var):
        # var -> tid, [ctid,] valsig, osig, attrsetid
        self.var2eval[var.name] = self.get_allvalsig(var.value)

    def save_attrs(self, attrs):
        # attrsig -> aid
        aids = set()
        for attr in attrs:
            if attr in self.attr2aid:
                aids.add(self.attr2aid[attr])
            else:
                aid = self.__class__.aid
                self.__class__.aid += 1
                self.attr2aid[attr] = aid
                aids.add(aid)
        return aids

    def save_ptoset(self, var, ptoset):
        # var -> tid,[ctid,] valsig, osig, attrsetid|tid,[ctid,] valsig, osig, attrsetid|...
        symset = {self.get_allvalsig(val) for val in ptoset}
        if var in self.var2ptosymset:
            self.var2ptosymset[var].update(symset)
        else:
            self.var2ptosymset[var] = symset

    def get_thid_sid(self, event):
        currthd = threading.currentThread()
        thsig = '<{thname}|{thident}>'.format(thname=currthd.getName(),
                                              thident=currthd.ident)
        thid = self.save_thread(thsig)
        if event.stmt is not None:
            sid = self.save_stmt(event.filename, event.olineno, event.nlineno, event.stmt)
        else:
            sid = -1
        return thid, sid


class DBEngine(object):
    def __init__(self, appname, tanker, outdir):
        self.appname = str(appname).replace('.', '_')
        self.tanker = tanker
        self.outdir = outdir if outdir else os.path.expanduser('~')
        self.dbname = 'PyTracer_' + self.appname + '.db'
        self.thdtabname = 'thd_' + self.appname
        self.modtabname = 'module_' + self.appname
        self.stmttabname = 'stmt_' + self.appname
        self.valtabname = 'val_' + self.appname
        self.psettabname = 'pset_' + self.appname
        self.attrsettabname = 'attrset_' + self.appname
        self.eventtabname = 'event_' + self.appname
        self.typetabname = 'type_' + self.appname
        self.attrtabname = 'attr_' + self.appname
        self.conn = None
        self.cur = None

    def connect_db(self, replace=True):
        dbfile = os.path.join(self.outdir, self.dbname)
        if replace and os.path.exists(dbfile):
            os.unlink(dbfile)
        self.conn = sqlite3.connect(dbfile)
        self.cur = self.conn.cursor()

    def close_db(self):
        if self.conn:
            self.conn.close()

    def execute(self, cmd):
        # logger.info(cmd)
        self.cur.execute(cmd)

    def executescript(self, script):
        self.cur.executescript(script)
        self.conn.commit()

    def commit(self):
        self.conn.commit()

    def fetchall(self):
        return self.cur.fetchall()


class TraceDBWriter(DBEngine):

    def __enter__(self):
        self.connect_db()
        self.create_tables()
        return self

    def __exit__(self, *_):
        while threading.activeCount() > 10:
            # for th in threading.enumerate():
            #     if th.is_alive():
            #         logger.info('alive thread: {0}'.format(th))
            logger.info('waiting for all the child threads '
                        'terminate(liveThreads{0})...'.format(threading.enumerate()))
            sleep(1)
        self.commit_all()
        self.close_db()

    def create_tables(self):
        self.create_thdtable()
        self.create_modtable()
        self.create_stmttable()
        self.create_typetable()
        self.create_valtable()
        self.create_psettable()
        self.create_attrtable()
        self.create_attrsettable()
        self.create_eventtable()
        self.commit()

    def create_thdtable(self):
        self.execute("""
        CREATE TABLE {thdtname}(
        THSIG TEXT,
        THID INTEGER PRIMARY KEY
        )
        """.format(thdtname=self.thdtabname))

    def create_modtable(self):
        self.execute("""
        CREATE TABLE {modtname}(
        FNAME TEXT,
        MID INTEGER PRIMARY KEY
        )
        """.format(modtname=self.modtabname))

    def create_stmttable(self):
        self.execute("""
        CREATE TABLE {stmttname}(
        SSIG TEXT ,
        SID INTEGER PRIMARY KEY
        )
        """.format(stmttname=self.stmttabname))

    def create_eventtable(self):
        self.execute("""
        CREATE TABLE {tracetname}(
        GID INTEGER PRIMARY KEY, THID INTEGER, SID INTEGER,
        INST TEXT, TYPE INTEGER, EXT INTEGER
        )
        """.format(tracetname=self.eventtabname))
        #FOREIGN KEY(SID) REFERENCES {stmttname}(SID)

    def create_typetable(self):
        self.execute("""
        CREATE TABLE {typetname}(
        TSIG TEXT,
        MRO TEXT,
        TID INTEGER PRIMARY KEY
        )
        """.format(typetname=self.typetabname))

    def create_valtable(self):
        self.execute("""
        CREATE TABLE {valtname}(
        VAR TEXT,
        VAL TEXT
        )
        """.format(valtname=self.valtabname))

    def create_psettable(self):
        self.execute("""
        CREATE TABLE {psettname}(
        VAR TEXT,
        PSET TEXT
        )
        """.format(psettname=self.psettabname))

    def create_attrsettable(self):
        self.execute("""
        CREATE TABLE {attrsettname}(
        ASET TEXT,
        ASID INTEGER PRIMARY KEY
        )
        """.format(attrsettname=self.attrsettabname))

    def create_attrtable(self):
        self.execute("""
        CREATE TABLE {attrtname}(
        ATTR TEXT,
        AID INTEGER
        )
        """.format(attrtname=self.attrtabname))

    def commit_all(self):
        # logger.info('dump to files...')
        # self.dump_to_file()
        self.execute('PRAGMA synchronous = OFF;')
        logger.info('commit threads into the database...')
        self.commit_thds()
        logger.info('commit modules into the database...')
        self.commit_mods()
        logger.info('commit stmts into the database...')
        self.commit_stmts()
        logger.info('commit types into the database...')
        self.commit_types()
        logger.info('commit values into the database...')
        self.commit_vals()
        logger.info('commit ptosets into the database...')
        self.commit_psets()
        logger.info('commit attrs into the database...')
        self.commit_attrs()
        logger.info('commit attrset into the database...')
        self.commit_attrsets()
        logger.info('commit events into the database...')
        self.commit_events()


    def commit_thds(self):
        for thsig in self.tanker.thd2idx:
            thid = self.tanker.thd2idx[thsig]
            self.execute("INSERT INTO {tname} "
                         "VALUES ('{thsig}', {thid})".format(tname=self.thdtabname,
                                                             thsig=thsig,
                                                             thid=thid))
        self.commit()

    def commit_mods(self):
        for fname in self.tanker.mname2idx:
            mid = self.tanker.mname2idx[fname]
            self.execute("INSERT INTO {tname} "
                         "VALUES ('{fname}', {mid})".format(tname=self.modtabname,
                                                            fname=fname,
                                                            mid=mid))
        self.commit()

    def commit_stmts(self):
        for mid, ol, nl, stmt in self.tanker.stmt2idx:
            ssig = '{mid}|{ol}|{nl}|{stmt}'.format(mid=mid,
                                                   ol=ol,
                                                   nl=nl,
                                                   stmt=str(ast.dump(stmt)).replace("'", "''").replace('|', '**'))
            sid = self.tanker.stmt2idx[(mid, ol, nl, stmt)]
            self.execute("INSERT INTO {tname} "
                         "VALUES ('{ssig}', {sid})".format(tname=self.stmttabname,
                                                           ssig=ssig,
                                                           sid=sid))
        self.commit()

    def commit_vals(self):
        for i, var in enumerate(self.tanker.var2eval):
            val = ','.join(str(x) for x in self.tanker.var2eval[var] if x != '').replace("'", "''")
            self.execute("INSERT INTO {tname} VALUES "
                         "('{var}', '{val}')".format(tname=self.valtabname,
                                                     var=var,
                                                     val=val))
            if i % 1000 == 0:
                logger.debug('commit vals<' + str(i) + '>...')
                self.commit()
        self.commit()

    def commit_psets(self):
        # for i, var in enumerate(self.tanker.var2ptoset):
        #     pset = '|'.join(v.valstr.replace("'", "''") for v in self.tanker.var2ptoset[var])
        #     self.execute("INSERT INTO {tname} "
        #                  "VALUES ('{var}', '{pset}')".format(tname=self.psettabname,
        #                                                      var=var, pset=pset))
        #     if i % 1000 == 0:
        #         logger.info('commit ptosets<' + str(i) + '>...')
        for i, var in enumerate(self.tanker.var2ptosymset):
            pset = '|'.join(','.join(str(x) for x in s if x != '').replace("'", "''")
                            for s in self.tanker.var2ptosymset[var])

            self.execute("INSERT INTO {tname} "
                         "VALUES ('{var}', '{pset}')".format(tname=self.psettabname,
                                                             var=var, pset=pset))
            if i % 1000 == 0:
                logger.debug('commit ptosets<' + str(i) + '>...')
        self.commit()

    def commit_attrs(self):
        for attr in self.tanker.attr2aid:
            self.execute("INSERT INTO {tname} "
                         "VALUES ('{attr}', {aid})".format(tname=self.attrtabname,
                                                           attr=attr,
                                                           aid=self.tanker.attr2aid[attr]))
        self.commit()

    def commit_types(self):
        for tsig in self.tanker.type2idx:
            tid = self.tanker.type2idx[tsig]
            mro = '|'.join(str(id_) for id_ in self.tanker.tid2mro.get(tid, ()))
            self.execute("INSERT INTO {tname} "
                         "VALUES ('{tsig}', '{mro}', "
                         "{tid})".format(tname=self.typetabname,
                                         tsig=tsig, mro=mro,
                                         tid=tid))
        self.commit()

    def commit_attrsets(self):
        for idx, aset in self.tanker.id2attrset.iteritems():
            aids = '|'.join(str(aid) for aid in aset)
            self.execute("INSERT INTO {tname} "
                         "VALUES ('{aids}', {id})".format(tname=self.attrsettabname,
                                                          aids=aids,
                                                          id=idx))
        self.commit()

    def commit_events(self):
        i = 0
        for e in self.tanker.gid2event.itervalues():
            e1 = {k: str(v).replace("'", "''") for k,v in e.iteritems()}
            self.execute("INSERT INTO {tname} "
                         "VALUES ({gid}, {thid}, {sid}, '{inst}',"
                         "{type}, {ext})".format(tname=self.eventtabname, **e1))
            i += 1
            if i > 1000:
                self.commit()
                i = 0
        self.commit()

    def dump_to_file(self):
        from ast import dump
        basedir = 'trace_dump/'
        if not os.path.exists(basedir):
            os.mkdir(basedir)
        with open(basedir + '_events', 'w') as f:
            for event in self.tanker.gid2event.itervalues():
                print >>f, ', '.join(str(k) + ': ' + str(v)
                                     for k, v in event.iteritems())
        with open(basedir + '_modules', 'w') as f:
            for mname, idx in self.tanker.mname2idx.iteritems():
                print >>f, mname, idx
        with open(basedir + '_stmt', 'w') as f:
            for stmt, idx in self.tanker.stmt2idx.iteritems():
                print >>f, stmt, idx
        with open(basedir + '_values', 'w') as f:
            for var, eval in self.tanker.var2eval.iteritems():
                print >>f, var, eval
        with open(basedir + '_psets', 'w') as f:
            for var, pset in self.tanker.var2ptosymset.iteritems():
                print >>f, var, pset
        with open(basedir + '_attrsets', 'w') as f:
            for id, attrset in self.tanker.id2attrset.iteritems():
                print >>f, id, attrset
        with open(basedir + '_attrs', 'w') as f:
            for attr, aid in self.tanker.attr2aid.iteritems():
                print >>f, attr, aid
        with open(basedir + '_types', 'w') as f:
            for type, id in self.tanker.type2idx.iteritems():
                print >>f, type, id
        idx2mname = {v: k for k, v in self.tanker.mname2idx.iteritems()}
        with open(basedir + 'source', 'w') as f:
            for stmt, idx in self.tanker.stmt2idx.iteritems():
                print >>f, idx, idx2mname.get(stmt[0]), stmt[1], stmt[2], dump(stmt[3])


class TraceDBReader(DBEngine):
    def __init__(self, projname, outdir=''):
        super(TraceDBReader, self).__init__(projname, TraceTanker(), outdir)

    def __enter__(self):
        self.connect_db(False)
        return self

    def __exit__(self, *_):
        self.close_db()

    def select_data(self, tname):
        self.execute("""
        SELECT * FROM {tabname}
        """.format(tabname=tname))
        return self.fetchall()

    def read_threads(self):
        data = self.select_data(self.thdtabname)
        for thd, idx in data:
            self.tanker.thd2idx[str(thd)] = int(idx)

    def read_modules(self):
        data = self.select_data(self.modtabname)
        for mname, mid in data:
            self.tanker.mname2idx[str(mname)] = int(mid)

    def read_stmts(self):
        data = self.select_data(self.stmttabname)
        for ssig, sid in data:
            mid, ol, nl, stmt = str(ssig).split('|')
            self.tanker.stmt2idx[(mid, ol, nl, stmt)] = int(sid)

    def read_types(self):
        data = self.select_data(self.typetabname)
        for tsig, mro, tid in data:
            self.tanker.type2idx[str(tsig)] = int(tid)
            self.tanker.tid2mro[int(tid)] = [int(s) for s in mro.split('|') if s != '']

    def read_values(self):
        data = self.select_data(self.valtabname)
        for var, val in data:
            items = str(val).split(',')
            if len(items) == 4:
                tid, valsig, osig, attrsetid = items
                ctid = -1
            else:
                tid, ctid, valsig, osig, attrsetid = items
            tid, ctid, attrsetid = int(tid), int(ctid), int(attrsetid)
            valsig = valsig if valsig.startswith('O') else int(valsig)
            self.tanker.var2eval[str(var)] = (tid, ctid, valsig, osig, attrsetid)

    def read_ptosets(self):
        data = self.select_data(self.psettabname)
        for var, pset in data:
            items = str(pset).split('|')
            pset = []
            for item in items:
                item = item.split(',')
                if len(item) == 4:
                    tid, valsig, osig, attrsetid = item
                    ctid = -1
                else:
                    tid, ctid, valsig, osig, attrsetid = item
                tid, ctid, attrsetid = int(tid), int(ctid), int(attrsetid)
                valsig = valsig if valsig.startswith('O') else int(valsig)
                pset.append((tid, ctid, valsig, osig, attrsetid))
            self.tanker.var2ptosymset[var] = pset

    def read_attrs(self):
        data = self.select_data(self.attrtabname)
        for attrsig, aid in data:
            self.tanker.attr2aid[str(attrsig)] = int(aid)

    def read_attrsets(self):
        data = self.select_data(self.attrsettabname)
        for aset, asid in data:
            aset = [int(aid) for aid in str(aset).split('|') if aid]
            self.tanker.id2attrset[int(asid)] = aset

    def read_events(self):
        data = self.select_data(self.eventtabname)
        for gid, thid, sid, inst, type, ext in data:
            dbevent = {'gid': int(gid), 'thid': int(thid),
                       'sid': int(sid), 'inst': str(inst),
                       'type': int(type), 'ext': int(ext)}
            analevent = AnalysisEvent(**dbevent)
            analevent.inst = InstParser.parse(analevent.type, analevent.inst)
            self.tanker.gid2event[int(gid)] = analevent

    def read_all(self):
        logger.info('read threads from the database...')
        self.read_threads()
        logger.info('read modules from the database...')
        self.read_modules()
        logger.info('read stmts from the database...')
        self.read_stmts()
        logger.info('read types from the database...')
        self.read_types()
        logger.info('read values from the database...')
        self.read_values()
        logger.info('read ptosets from the database...')
        self.read_ptosets()
        logger.info('read attrs from the database...')
        self.read_attrs()
        logger.info('read attrsets from the database...')
        self.read_attrsets()
        logger.info('read events from the database...')
        self.read_events()
        return self.tanker
