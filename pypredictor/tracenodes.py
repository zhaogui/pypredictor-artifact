#!/usr/bin/python
# _*_ coding:utf-8 _*_

import logging
from types import TypeType, ClassType, ModuleType
from inspect import getmro

from utils import builtintypes


logger = logging.getLogger('pytracer')


class Type(object):
    def __init__(self, name, attrs, supers):
        self.name = name
        self.attrs = attrs
        self.supers = supers


class Id(object):
    """
    name (str): the identifier
    value (HeapObject): the value
    """
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __repr__(self):
        return self.name


class Event(object):
    """
    fromexec: if it is an executed event.
    filename: the file name of source code.
    stmt: the static statement.
    nlineno: the 3-address based lineno.
    olineno: the lineno of the original source code.
    thdsig: the thread signature.
    """
    def __init__(self, fromexec=True):
        self.fromexec = fromexec
        self.filename = ''
        self.stmt = None
        self.nlineno = -1
        self.olineno = -1
        self.thdsig = ''

    def setinfo(self, filename, stmt, nlineno, olineno):
        self.filename = filename
        self.stmt = stmt
        self.nlineno = nlineno
        self.olineno = olineno

    @property
    def typeid(self):
        return TraceType.gettype(self)


class Allocate(Event):
    def __init__(self, target, value, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, HeapObject)
        self.target = target
        self.value = value
        super(Allocate, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={val}'.format(tgt=self.target.name,
                                    val=self.value.valstr)


class InfiniteRelaxAllocate(Event):
    def __init__(self, target, value, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, HeapObject)
        self.target = target
        self.value = value
        super(InfiniteRelaxAllocate, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={val}'.format(tgt=self.target.name,
                                    val=self.value.symstr)


class FiniteRelaxAllocate(Event):
    def __init__(self, target, values, fromexec=True):
        assert isinstance(target, Id)
        self.target = target
        self.values = values
        super(FiniteRelaxAllocate, self).__init__(fromexec)

    def __repr__(self):
        valstr = '|'.join(val.valstr for val in self.values)
        return '{tgt}={val}'.format(tgt=self.target.name,
                                    val=valstr)


class Assign(Event):
    def __init__(self, target, value, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, Id), \
            'not both target<{0}> and value<{1}> are Id'.format(target, value)
        self.target = target
        self.value = value
        super(Assign, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={val}'.format(tgt=self.target.name, val=self.value.name)


class AttrLoad(Event):
    def __init__(self, target, value, attr, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, Id)
        self.target = target
        self.value = value
        self.attr = attr
        self.encval = None
        super(AttrLoad, self).__init__(fromexec)

    # @property
    # def attrsig(self):
    #     return '{0}_${1}'.format(self.value.value.valstr, self.attr)

    def __repr__(self):
        return '{tgt}={val}.{attr}'.format(tgt=self.target.name,
                                           val=self.value.name,
                                           attr=self.attr)


class AttrStore(Event):
    def __init__(self, target, attr, value, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, Id)
        self.target = target
        self.attr = attr
        self.value = value
        self.enctgt = None
        super(AttrStore, self).__init__(fromexec)

    # @property
    # def attrsig(self):
    #     return '{0}_${1}'.format(self.target.value.valstr, self.attr)

    def __repr__(self):
        return '{tgt}.{attr}={val}'.format(tgt=self.target.name,
                                           attr=self.attr,
                                           val=self.value.name)


class SubLoad(Event):
    def __init__(self, target, value, index, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, Id) \
            and isinstance(index, Id)
        self.target = target
        self.value = value
        self.index = index
        self.encval = None
        super(SubLoad, self).__init__(fromexec)

    @property
    def subsig(self):
        return '{0}_${1}'.format(self.value.value.valstr,
                                 self.index.value.valstr)

    def __repr__(self):
        return '{tgt}={sub}[{idx}]'.format(tgt=self.target.name,
                                           sub=self.value.name,
                                           idx=self.index.name)


class SubStore(Event):
    def __init__(self, target, index, value, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, Id) \
            and isinstance(index, Id)
        self.target = target
        self.index = index
        self.value = value
        self.enctgt = None
        super(SubStore, self).__init__(fromexec)

    @property
    def subsig(self):
        return '{0}_${1}'.format(self.target.value.valstr,
                                 self.index.value.valstr)

    def __repr__(self):
        return '{sub}[{idx}]={val}'.format(sub=self.target.name,
                                           idx=self.index.name,
                                           val=self.value.name)


class BopAssign(Event):
    def __init__(self, target, value1, value2, op, fromexec=True):
        assert isinstance(target, Id) and isinstance(value1, Id) \
            and isinstance(value2, Id)
        self.target = target
        self.value1 = value1
        self.value2 = value2
        self.op = BinOperator.make(op)
        super(BopAssign, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={v1} {op} {v2}'.format(tgt=self.target.name,
                                             v1=self.value1.name,
                                             op=self.op,
                                             v2=self.value2.name)


class UopAssign(Event):
    def __init__(self, target, value, op, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, Id)
        self.target = target
        self.value = value
        self.op = UnaryOperator.make(op)
        super(UopAssign, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={op} {val}'.format(tgt=self.target.name,
                                         op=self.op,
                                         val=self.value.name)


class MethodInvoke(Event):
    def __init__(self, receiver, attr, fromexec=True):
        assert isinstance(receiver, Id)
        self.receiver = receiver
        self.attr = attr
        super(MethodInvoke, self).__init__(fromexec)

    def __repr__(self):
        return 'call {rec}.{attr}'.format(rec=self.receiver.name, attr=self.attr)


class FunctionCall(Event):
    def __init__(self, func, fromexec=True):
        assert isinstance(func, Id)
        self.func = func
        super(FunctionCall, self).__init__(fromexec)

    def __repr__(self):
        return 'call {func}'.format(func=self.func.name)


class TypeAssume(Event):
    def __init__(self, target, value, type, fromexec=True):
        assert isinstance(target, Id) and \
            isinstance(value, Id) and \
            isinstance(type, Id)
        self.target = target
        self.value = value
        self.type = type
        super(TypeAssume, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={val} {type}'.format(tgt=self.target.name,
                                           val=self.value.name,
                                           type=self.type.name)


class SubTypeAssume(Event):
    def __init__(self, target, value, type, fromexec=True):
        assert isinstance(target, Id) and \
            isinstance(value, Id) and \
            isinstance(type, Id)
        self.target = target
        self.value = value
        self.type = type
        super(SubTypeAssume, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={val} {type}'.format(tgt=self.target.name,
                                           val=self.value.name,
                                           type=self.type.name)


class HasAttrAssume(Event):
    def __init__(self, target, attr, value, fromexec=True):
        assert isinstance(target, Id) and isinstance(value, Id)
        self.target = target
        self.attr = attr
        self.value = value
        super(HasAttrAssume, self).__init__(fromexec)

    def __repr__(self):
        return '{tgt}={attr} {val}'.format(tgt=self.target,
                                           attr=repr(self.attr),
                                           val=self.value.name)


class Branch(Event):
    bid = 0L

    def __init__(self, test, execbranch=True, fromexec=True):
        self.bid = Branch.bid
        Branch.bid += 1
        self.test = test
        self.execbranch = execbranch
        self.resolved = False
        self._laststmtlineno = -1
        super(Branch, self).__init__(fromexec)

    @property
    def laststmtlineno(self):
        if self._laststmtlineno >= 0:
            return self._laststmtlineno
        if self.stmt:
            if self.stmt.orelse:
                self._laststmtlineno = self.stmt.orelse[-1].lineno
            else:
                self._laststmtlineno = self.stmt.body[-1].lineno
            return self._laststmtlineno

    def __repr__(self):
        return 'Branch<{bid}>|{test}|' \
               '{ebrch}|{res}'.format(bid=self.bid,
                                      test=self.test.name,
                                      ebrch=int(self.execbranch),
                                      res=int(self.resolved))


class ExecBranchEnd(Event):
    def __init__(self, branch):
        self.branch = branch
        super(ExecBranchEnd, self).__init__(False)

    @property
    def bid(self):
        return self.branch.bid

    def __repr__(self):
        return 'ExecBranchEnd<' + str(self.branch.bid) + '>'


class ExtBranchEnd(Event):
    def __init__(self, branch):
        self.branch = branch
        super(ExtBranchEnd, self).__init__(False)

    @property
    def bid(self):
        return self.branch.bid

    def __repr__(self):
        return 'ExtBranchEnd<' + str(self.branch.bid) + '>'


class PhiFunction(Event):
    def __init__(self, target, value1, value2, test=None, isvardef=True):
        if isvardef:
            assert isinstance(target, Id) and isinstance(value1, Id) \
                and isinstance(value2, Id)
        self.target = target
        self.value1 = value1
        self.value2 = value2
        self.test = test
        self.isvardef = isvardef
        super(PhiFunction, self).__init__(False)

    def __repr__(self):
        return '{isvardef}|{test}?{tgt}={val1}:' \
               '{val2}'.format(isvardef=int(self.isvardef),
                               test=self.test if self.test else '',
                               tgt=self.target,
                               val1=self.value1,
                               val2=self.value2)


class HeapObject(object):
    id = 0L
    oid2id = {}

    def __init__(self, liveobj):
        if isinstance(liveobj, int):
            self.liveobj = int(liveobj)
        else:
            self.liveobj = liveobj
        self._assymbolic = False
        self._osig = None

    @property
    def assymbolic(self):
        return self._assymbolic

    def set_assymbolic(self):
        self._assymbolic = True
        if not self._osig:
            self.__class__.id += 1
            nid = self.__class__.id
            self._osig = 'O' + str(nid)

    @property
    def type(self):
        try:
            return self.liveobj.__class__
        except AttributeError:
            return type(self.liveobj)

    @property
    def typeid(self):
        try:
            klass = self.liveobj.__class__
            kid = str(id(klass))
            return kid
        except AttributeError:
            type_ = type(self.liveobj)
            tid = str(id(type_))
            return tid

    @property
    def istype(self):
        return isinstance(self.liveobj, (TypeType, ClassType, ModuleType))

    @property
    def tname(self):
        try:
            klass = self.liveobj.__class__
            return klass.__name__
        except AttributeError:
            type_ = type(self.liveobj)
            return type_.__name__

    @property
    def isbuiltintypeinst(self):
        return (not self.istype) and self.type in builtintypes

    @property
    def isbuiltintype(self):
        return self.istype and (self.liveobj in builtintypes)

    @property
    def typeattrs(self):
        try:
            type_ = self.type
            aset = set(t for t in dir(type_) if not t.startswith('$'))
            if hasattr(type_, '__mro__'):
                aset.add('__mro__')
        except RuntimeError:
            return set()

    @property
    def attrs(self):
        try:
            aset = set(t for t in dir(self.liveobj) if not t.startswith('$'))
            if self.istype:
                if hasattr(self.liveobj, '__metaclass__'):
                    aset.update(dir(self.liveobj.__metaclass__))
                if hasattr(self.liveobj, '__mro__'):
                    aset.add('__mro__')
                if hasattr(self.liveobj, '__name__'):
                    aset.add('__name__')
            if hasattr(self.liveobj, '__dict__'):
                aset.add('__dict__')
            return aset
        except RuntimeError:    # still not clear why some cases may raise recursion exception
            # logger.warning('Occur RuntimeError during get '
            #                'attributes of objects <{}>'.format(getattr(self, 'value')))
            return set()

    @property
    def oid(self):
        return id(self.liveobj)

    @property
    def name(self):
        if self.istype:
            try:
                return self.liveobj.__name__
            except AttributeError:
                logger.warning('cannot find the name of this type object.')
                return ''
        raise ValueError('this object is not type, should not be called')

    @property
    def mro(self):
        try:
            if self.istype:
                mro = [m for m in getmro(self.liveobj) if m != self.liveobj]
            else:
                mro = [m for m in getmro(self.type) if m != self.type]
        except (AttributeError, RuntimeError):
            mro = ()
        return [HeapObject(klass) for klass in mro]

    @property
    def osig(self):
        if self._assymbolic:
            return self._osig
        # TODO: list is not hashable,but can be compared,
        # TODO: we need improve this in the future.
        if isinstance(self.liveobj, (unicode, str, tuple)):
            # fix for the same hash code of 0 and empty str ''.
            try:
                oid = hash(self.liveobj)
            except TypeError:
                oid = id(self.liveobj)
        else:
            oid = id(self.liveobj)
        if oid in self.__class__.oid2id:
            return 'O' + str(self.__class__.oid2id[oid])
        else:
            self.__class__.id += 1
            nid = self.__class__.id
            self.__class__.oid2id[oid] = nid
            return 'O' + str(nid)

    @property
    def valstr(self):
        if self._assymbolic:
            return self._osig
        elif isinstance(self.liveobj, int):
            return repr(self.liveobj)
        else:
            return self.osig

    @property
    def symstr(self):
        return self.osig

    def __repr__(self):
        return self.valstr

    def __str__(self):
        return '{type}{value}'.format(type=self.tname,
                                      value=self.valstr)

    def __hash__(self):
        return id(self.liveobj)/16

    def __eq__(self, other):
        return self.liveobj == other.liveobj


class BinOperator(object):
    astop2top = {'add': 'add', 'sub': 'sub', 'mult': 'mul',
                 'div': 'div', 'mod': 'mod', 'pow': 'pow',
                 'lt': 'lt', 'lte': 'le', 'gt': 'gt',
                 'gte': 'ge', 'eq': 'eq', 'noteq': 'ne',
                 'is': 'eq', 'isnot': 'ne',
                 'or': 'or', 'and': 'and'}

    @classmethod
    def make(cls, op):
        return cls.astop2top.get(op.__class__.__name__.lower())

    @classmethod
    def acceptable(cls, op):
        return op.__class__.__name__.lower() in cls.astop2top


class UnaryOperator(object):
    astop2top = {'not': 'not', 'uadd': 'pos', 'usub': 'neg'}

    @classmethod
    def make(cls, op):
        return cls.astop2top.get(op.__class__.__name__.lower())

    @classmethod
    def acceptable(cls, op):
        return op.__class__.__name__.lower() in cls.astop2top


class TraceType:
    ALLOCATE = 0
    ASSIGN = 1
    ATTRLOAD = 2
    ATTRSTORE = 3
    SUBLOAD = 4
    SUBSTORE = 5
    BOPASSIGN = 6
    UOPASSIGN = 7
    METHODINVOKE = 8
    FUNCTIONCALL = 9
    TYPEASSUME = 10
    SUBTYPEASSUME = 11
    HASATTRASSUME = 12
    BRANCH = 13
    EXTBRANCHEND = 14
    EXECBRANCHEND = 15
    PHIFUNCTION = 16
    INFINITERELAXALLOCATE = 17
    FINITERELAXALLOCATE = 18

    type2tsig = {}

    @classmethod
    def gettype(cls, event):
        try:
            return getattr(cls, event.__class__.__name__.upper())
        except AttributeError:
            return -1

    @classmethod
    def geteventsig(cls, type):
        assert isinstance(type, (int, long)), 'type must be an integer or long'
        if type in cls.type2tsig:
            return cls.type2tsig[type]
        for k, v in cls.__dict__.iteritems():
            if v == type:
                cls.type2tsig[v] = k
                return k
