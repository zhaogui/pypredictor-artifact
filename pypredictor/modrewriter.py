#!/usr/bin/python
# _*_ coding:utf-8 _*_

__author__ = 'gui'

import os
from ast import parse
from collections import namedtuple
from py_compile import wr_long, marshal, MAGIC
from shutil import copyfile

from astrewriter import ASTVisitor
from utils import encode_filename

try:
    import cPickle as pickle
except:
    import pickle

import astunparse
import logging


__all__ = ['rewrite']

logger = logging.getLogger('pyrewriter')

# NewASTInfo = namedtuple('NewASTInfo', 'filename lineno2stmt newlineno2oldlineno')


class NewASTInfo(object):
    def __init__(self, filename, lineno2stmt, newlineno2oldlineno):
        self.filename = filename
        self.lineno2stmt = lineno2stmt
        self.newlineno2oldlineno = newlineno2oldlineno


def recompile_pyfile(filename, tmpdir='tmp'):
    with open(filename) as pyfile:
        ori_ast = parse(pyfile.read(), filename, 'exec')
    visitor = ASTVisitor(nestedexpand=True)
    new_ast = visitor.visit(ori_ast)
    newast_info = NewASTInfo(filename, visitor.lineno2stmt, visitor.newlineno2oldlineno)
    # only for debug the astrewriter module
    cachepydir = os.path.join(tmpdir, 'cachepy')
    if not os.path.exists(cachepydir):
        os.makedirs(cachepydir)
    newsource_filename = os.path.join(cachepydir, encode_filename(filename))
    with open(newsource_filename, 'w') as sourcefile:
        source = astunparse.unparse(new_ast)
        if source.startswith('\n'):
            source = source[1:]
        sourcefile.write(source)

    # write the pickle files
    cachepklpath = os.path.join(tmpdir, 'cachepkl')
    if not os.path.exists(cachepklpath):
        os.mkdir(cachepklpath)
    pkl_filename = os.path.join(cachepklpath,
                                encode_filename(os.path.splitext(filename)[0])+'.pkl')
    with open(pkl_filename, 'wb') as pklfile:
        # print '{filename}\n ' \
        #       '[lineno2stmt]:\n' \
        #       '{ls}'.format(filename=newast_info.filename,
        #                     ls='\n'.join('{k}:{v}'.format(k=k,
        #                                                   v=dump(v))
        #                     for k, v in newast_info.lineno2stmt.iteritems()))
        pickle.dump(newast_info, pklfile)
    return new_ast


def pycgen(codeobject, filepath=None, tmpdir='tmp', doraise=False):
    timestamp = long(os.stat(filepath).st_mtime)
    filepath = filepath + (__debug__ and 'c' or 'o')
    # Atomically write the pyc/pyo file.  Issue #13146.
    # id() is used to generate a pseudo-random filename.
    path_tmp = '{}.{}'.format(filepath, id(filepath))
    try:
        with open(path_tmp, 'wb') as fc:
            fc.write('\0\0\0\0')
            wr_long(fc, timestamp)
            marshal.dump(codeobject, fc)
            fc.flush()
            fc.seek(0, 0)
            fc.write(MAGIC)
        os.rename(path_tmp, filepath)
    except OSError:
        try:
            os.unlink(path_tmp)
        except OSError:
            pass
        raise


def rewrite(recordfile, tmpdir='tmp', readfromcache=True):
    with open(recordfile) as rf:
        pyfnamelist = rf.read().splitlines()
        if '' in pyfnamelist:
            pyfnamelist.remove('')
        new_asts, new_cos, new_pycs = [], [], []
        for filename in pyfnamelist:
            logger.debug('rewrite ast of <{}>'.format(filename))
            if readfromcache:
                pycfilename = os.path.join(tmpdir, 'cachepyc', encode_filename(filename)) + 'c'
                if os.path.exists(pycfilename) and \
                                os.path.getmtime(pycfilename) >= os.path.getmtime(filename):
                    logger.debug('find cache of <{}>'.format(filename))
                    new_pycs.append((filename, pycfilename))
                else:
                    new_asts.append((filename, recompile_pyfile(filename, tmpdir)))
                    logger.debug('rewrite ast of <{}> done'.format(filename))
            else:
                new_asts.append((filename, recompile_pyfile(filename, tmpdir)))
                logger.debug('rewrite ast of <{}> done'.format(filename))

        for filename, new_ast in new_asts:
            if new_ast:
                logger.debug('recompile ast of <{}>'.format(filename))
                new_cos.append((filename, compile(new_ast, filename, 'exec')))
                logger.debug('recompile ast of <{}> done'.format(filename))

        for filename, new_co in new_cos:
            logger.debug('pycgen <{}>'.format(filename))
            #generage pyc file from the new code object.
            pycgen(new_co, filename)
            # cache the pyc files
            cachepycpath = os.path.join(tmpdir, 'cachepyc')
            pycfilename = filename + 'c'
            if not os.path.exists(cachepycpath):
                os.mkdir(cachepycpath)
            cacheepycfilename = os.path.join(cachepycpath, encode_filename(pycfilename))
            copyfile(pycfilename, cacheepycfilename)
            logger.debug('pycgen <{}> done'.format(filename))

        for filename, pycfilename in new_pycs:
            logger.debug('write cahce pyc <{}>'.format(filename))
            logger.debug(pycfilename)
            copyfile(pycfilename, filename + 'c')
            logger.debug('write cahce pyc <{}> done'.format(filename))


