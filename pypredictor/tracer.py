#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import logging
try:
    import cPickle as pickle
except:
    import pickle

from ast import ClassDef, Import, ImportFrom, dump
from os.path import join, abspath, splitext, realpath
from utils import getsourcefile, encode_filename, Task, preset_ignores
from pyinspector import PyInspector
from predtracer import PredictiveTracer
from tracemaker import TraceMaker
from Queue import Queue


__all__ = ['trace']

logger = logging.getLogger('pytracer')


_currdir = realpath('')


def trace(projname, scriptfile, entryfunc,
          ignores, recordfile,
          pkldir, outdir=''):
    with PredictiveTracer(projname, outdir) as analyzer:
        _trace(scriptfile, entryfunc,
               ignores, recordfile,
               pkldir, analyzer)


def _trace(scriptfile, entryfunc,
           ignores, recordfile,
           tmpdir, analyzer):
    logger.info('load the pickled module informations...')
    mod_newast_infos = _loadpkls(recordfile, tmpdir)
    logger.info('load finished, and now begin tracing...')

    try:
        import marshal
        scriptfile = abspath(scriptfile)
        with open(scriptfile + 'c', 'rb') as fp:
            code = marshal.loads(fp.read()[8:])
        # try to emulate __main__ namespace as much as possible
        globs = {
            '__file__': scriptfile,
            '__name__': '__main__',
            '__package__': None,
            '__cached__': None,
        }
        tracer = PyTracer(scriptfile=scriptfile,
                          entryfunc=entryfunc,
                          modinfos=mod_newast_infos,
                          ignores=ignores,
                          analyzer=analyzer)
        with PyInspector(tracer):
            exec code in globs, globs
            # try:
            #     exec code in globs, globs
            # except BaseException:
            #     import traceback
            #     traceback.print_exc(file=sys.stderr)
                # logger.info('The program raise:' + e.message)
        logger.info('tracing finished...')
        # logger.info('time elapsed: {}s'.format(tracer.elapsedtime))
    except IOError, err:
        logger.warning("Cannot run file %r because: %s" % (scriptfile, err))
        sys.exit(1)
    except SystemExit:
        pass


def _loadpkls(recordfile, tmpdir):
    mod_newast_infos = {}
    with open(recordfile) as mf:
        pyfnamelist = mf.read().splitlines()
        if '' in pyfnamelist:
            pyfnamelist.remove('')
        for filename in pyfnamelist:
            pkl_filename = join(tmpdir, 'cachepkl',
                                encode_filename(splitext(filename)[0])+'.pkl')
            logger.debug('load the pickled module {0}'.format(pkl_filename))
            with open(pkl_filename) as pkl:
                #pdb.set_trace()
                mod_newast_info = pickle.load(pkl)
                mod_newast_infos[mod_newast_info.filename] = mod_newast_info
    return mod_newast_infos


def _threaded_loadpkls(recordfile, tmpdir):
    mod_newast_infos = {}

    def loadpkl(pkl_filename):
        with open(pkl_filename) as pkl:
            #pdb.set_trace()
            mod_newast_info = pickle.load(pkl)
            mod_newast_infos[mod_newast_info.filename] = mod_newast_info

    with open(recordfile) as mf:
        pyfnamelist = mf.read().splitlines()
        if '' in pyfnamelist:
            pyfnamelist.remove('')
        queue = Queue()
        for x in range(8):
            task = Task(queue, loadpkl)
            task.daemon = True
            task.start()
        for filename in pyfnamelist:
            pkl_filename = join(tmpdir, 'cachepkl',
                                encode_filename(splitext(filename)[0])+'.pkl')
            logger.info('load the pickled module {0}'.format(pkl_filename))
            # loadpkl(pkl_filename)
            queue.put(pkl_filename)
        queue.join()
    return mod_newast_infos


class PyTracer(object):
    def __init__(self, scriptfile, entryfunc, modinfos, ignores, analyzer):
        self.scriptfile = scriptfile
        self.entryfunc = entryfunc
        self.modinfos = modinfos
        self.analyzer = analyzer
        self.frame2tmaker = {}
        self.retargets = set()   # linenos of stmt being the target of return (in other words, call-sites)
        self.exptargets = set()  # linenos of stmt occurring exceptions
        self.ignores = self.initial_ignores(ignores or [])
        self.notignorecache = set()
        self.ignorecache = set()
        self.ignoreframes = set()  # ignored frames
        self.startsignal = False
        self._isdebug = logger.isEnabledFor(logging.DEBUG)

    def initial_ignores(self, ignores):
        return filter(bool, set(ignores + preset_ignores))

    def get_tmaker(self, frame):
        return self.frame2tmaker.get(frame)

    def add_tmaker(self, tmaker):
        self.frame2tmaker[tmaker.aframe.eframe] = tmaker

    def get_backtmaker(self, frame):
        return self.frame2tmaker.get(frame.f_back) if frame.f_back else None

    def remove_tmaker(self, frame):
        tmaker = self.frame2tmaker.get(frame)
        if tmaker:
            del self.frame2tmaker[frame]
        return tmaker

    def check_ignore(self, filename, funcname):
        fullfuncname = (filename + '@' + funcname)
        if fullfuncname in self.notignorecache:
            return False
        if fullfuncname in self.ignorecache:
            return True
        for ignore in self.ignores:
            if ignore in fullfuncname:
                self.ignorecache.add(fullfuncname)
                return True
        self.notignorecache.add(fullfuncname)
        return False

    def check_entryfunc(self, filename, funcname):
        return filename.endswith(self.scriptfile) and (funcname == self.entryfunc)

    def add_return_target(self, eframe, lineno):
        self.retargets.add((eframe, lineno))

    def remove_return_target(self, frame, lineno):
        tp = (frame, lineno)
        if tp in self.retargets:
            self.retargets.remove(tp)

    def is_return_target(self, frame, lineno):
        return (frame, lineno) in self.retargets

    def add_exp_target(self, frame, lineno):
        self.exptargets.add((frame, lineno))

    def remove_exp_target(self, frame, lineno):
        tp = (frame, lineno)
        if tp in self.exptargets:
            self.exptargets.remove(tp)

    def is_exp_target(self, eframe, lineno):
        return (eframe, lineno) in self.exptargets

    def preset(self):
        self.analyzer.starttiming()

    def postset(self):
        self.analyzer.stoptiming()

    @property
    def elapsedtime(self):
        return self.analyzer.elapsedtime

    def handle_call(self, frame, event, arg):
        # ignore all the trace of non-main module execution
        if self.frame2tmaker and frame.f_code.co_name == '<module>':
            self.ignoreframes.add(frame)
            return False
        # ignore all the calls' trace generated by non-main module
        # add the frame of calls called by the non-main modules
        if frame.f_back and (frame.f_back in self.ignoreframes):
            self.ignoreframes.add(frame)
            return False
        # self.verboseframe(event, frame)
        # print threading.currentThread().name
        fullpath = getsourcefile(frame)
        if fullpath:
            if self.check_ignore(fullpath, frame.f_code.co_name):
                self.ignoreframes.add(frame)
                return False
            # get the information package of the source file
            info = self.modinfos.get(fullpath)
            # ignore when can not find the source information
            if not info:
                self.log_source_missing_error(frame)
                return False
            backtmaker = self.get_backtmaker(frame)
            backaframe = backtmaker.aframe if backtmaker else None
            l2s, n2o = info.lineno2stmt, info.newlineno2oldlineno
            entry = self.check_entryfunc(fullpath, frame.f_code.co_name)
            tmaker = TraceMaker(backaframe, frame, l2s, n2o, entry)
            self.add_tmaker(tmaker)
            # check if this call is the entry function
            if entry:
                self.startsignal = True
            # check whether the start signal has been set
            # (i.e. arriving the entry function)
            if not self.startsignal:
                return True
            if backtmaker is not None:
                if self._isdebug:
                    self.verbosestmt(event, backtmaker.aframe.currlineno,
                                     backtmaker.aframe.currstmt,
                                     frame.f_back.f_code.co_name,
                                     frame.f_back.f_code.co_filename)
                trace_ = tmaker.make_call()
                self.analyze('call', backtmaker.aframe, trace_)
                if self._isdebug:
                    self.verbosetrace(trace_)
            return True
        else:
            self.log_source_missing_error(frame)
            return False

    def handle_line(self, frame, event, arg):
        tmaker = self.get_tmaker(frame)
        # ignore if the tracing does not begin
        if tmaker is None:
            return
        curraframe = tmaker.aframe
        currlineno = curraframe.currlineno
        newlineno = frame.f_lineno
        # ignore if the start signal is false
        if not self.startsignal:
            curraframe.currlineno = newlineno
            return
        # ignore if it is before the very first line
        if currlineno == -1:
            curraframe.currlineno = newlineno
            return
        # ignore if it is the call-site statement(the return target)
        if self.is_return_target(frame, currlineno):
            self.remove_return_target(frame, currlineno)
            curraframe.currlineno = newlineno
            return
        # ignore if it is the statement occurring exception
        if self.is_exp_target(frame, currlineno):
            self.remove_exp_target(frame, currlineno)
            curraframe.currlineno = newlineno
            return
        # ignore if it the statement in the classdef
        if curraframe.back and \
                isinstance(curraframe.back.currstmt, ClassDef):
            curraframe.currlineno = newlineno
            return
        # begin tracing the current line(before the line of the new line)
        if curraframe.currstmt:
            # self.verboseframe(event, frame)
            try:
                if self._isdebug:
                    self.verbosestmt(event, curraframe.currlineno,
                                     curraframe.currstmt,
                                     frame.f_code.co_name,
                                     frame.f_code.co_filename)
                trace_ = tmaker.make_line()
                self.analyze('line', curraframe, trace_)
                if self._isdebug:
                    self.verbosetrace(trace_)
            except:
                # import traceback
                # print >>sys.stderr, traceback.format_exc()
                raise
        else:
            logger.warning('missing the lineno {0} in <{1}> '
                           'file <{2}>'.format(currlineno,
                                               frame.f_code.co_name,
                                               frame.f_code.co_filename))
        curraframe.currlineno = newlineno

    def handle_return(self, frame, event, arg):
        if frame in self.ignoreframes:
            self.ignoreframes.remove(frame)
            return
        # self.verboseframe(event, frame)
        self.handle_line(frame, 'line', arg)
        # handle the return of a function call
        # TODO: some problems caused by the yield statement (not a true return)
        tmaker = self.remove_tmaker(frame)
        # case  the return of the  __exit__ call in the 'with' stmt,
        # should not handle as true return
        # if (tmaker is None) or (frame.f_code.co_name == '__exit__'):
        #     return
        # assert ce_aframe is not None, 'the current analysis frame should not be none when return'
        assert tmaker, 'the current analysis frame of <{0}> ' \
                       'should not be None'.format(frame.f_code.co_name)
        if not self.startsignal:
            return
        # begin tracing return
        try:
            if self._isdebug:
                self.verbosestmt(event,
                                 tmaker.aframe.currlineno,
                                 tmaker.aframe.currstmt,
                                 frame.f_code.co_name,
                                 frame.f_code.co_filename)
            trace_ = tmaker.make_return()
            ca_aframe = tmaker.aframe.back
            self.analyze('return', ca_aframe, trace_)
            if self._isdebug:
                self.verbosetrace(trace_)
        except:
            # import traceback
            # print >>sys.stderr, traceback.format_exc()
            raise
        tmaker.aframe.currlineno = -1
        # filter out the classdef, import, importfrom or __exit__ of with as callsite
        # since the return are not actually returned out to the callsite
        if ca_aframe and not (isinstance(ca_aframe.currstmt, ClassDef) or
                              isinstance(ca_aframe.currstmt, Import) or
                              isinstance(ca_aframe.currstmt, ImportFrom) or
                              frame.f_code.co_name == '__exit__'):
            self.add_return_target(ca_aframe.eframe, ca_aframe.currlineno)

    def handle_except(self, frame, event, arg):
        # self.verbose(event, frame)
        self.analyze('exception', self.get_tmaker(frame).aframe, [])
        self.add_exp_target(frame, frame.f_lineno)

    def analyze(self, why, aframe, trace):
        self.analyzer.flowthrough(why, aframe, trace)

    def log_source_missing_error(self, frame):
        logger.debug('cannot locate the file {0} called by <{1}> '
                     'in file <{2}>'.format(frame.f_code.co_filename,
                                            frame.f_back.f_code.co_name,
                                            frame.f_back.f_code.co_filename))

    def verboseframe(self, event, frame):
        if event == 'call':
            logger.debug('=====>call <{0}> '
                         'in file <{1}>'.format(frame.f_code.co_name,
                                                frame.f_code.co_filename))
        elif event == 'line':
            logger.debug('+++++line [%d] in <%s>'
                         ' file <%s>:' % (frame.f_lineno,
                                          frame.f_code.co_name,
                                          frame.f_code.co_filename))
        elif event == 'return':
            logger.debug('<=====return line [%d] <%s> '
                         'file <%s>' % (frame.f_lineno,
                                        frame.f_code.co_name,
                                        frame.f_code.co_filename))
        elif event == 'except':
            logger.debug('*******exception {0} in <{1}> '
                         'file <{2}>'.format(frame.f_lineno,
                                             frame.f_code.co_name,
                                             frame.f_code.co_filename))

    def verbosestmt(self, event, lineno, stmt, funcname, filename):
        logger.debug('[{event}] [{lno}] in <{funcname}>'
                     'in file <{filename}>'.format(event=event.upper(), lno=lineno,
                                                   funcname=funcname,
                                                   filename=filename))
        logger.debug('-----[STMT] ' + (dump(stmt) if stmt is not None else 'None'))
        pass

    def verbosetrace(self, trace):
        logger.debug('=====[TRACE] ' + ', '.join(repr(t) for t in trace))
