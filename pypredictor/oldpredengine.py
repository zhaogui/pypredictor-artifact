#!/usr/bin/python
# _*_ coding:utf-8 _*_


#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import time
import logging
from multiprocessing import Process, Queue
from z3 import Solver, If, And, Not

from z3encoder import Z3Encoder
from traceslicer import TraceSlicer
from instparser import BranchInst, ExtBranchEndInst, ExecBranchEndInst
from tracenodes import TraceType
from dbengine import TraceDBReader


logger = logging.getLogger('pypredictor')

# logger.setLevel(logging.INFO)


def predict(projname, useslice=False, outdir='', start=0, stop=None):
    with TraceDBReader(projname, outdir) as reader:
        tanker = reader.read_all()
        z3engine = PredictiveEngine(tanker, useslice)
        starttime = time.time()
        z3engine.run(start, stop)
        stoptime = time.time()
        logger.info('the whole elapsed time: {0}s...'.format(stoptime - starttime))


class TypeInconsError(Exception):
    def __init__(self, message, solution):
        self.solution = solution
        super(TypeInconsError, self).__init__(message + ': ' + str(self.solution))


class AnalysisBranch(object):
    def __init__(self, branch):
        self.branch = branch  # event
        self.execbcons = []
        self.extbcons = []
        # 1 represents current branch is the executed branch
        # else means in the extended branch
        self.currbranch = 0


class PredictiveEngine(object):

    checktypes = (TraceType.ATTRLOAD, TraceType.SUBLOAD, TraceType.METHODINVOKE)

    def __init__(self, tracetanker, useslice=False):
        self.tanker = tracetanker
        self.z3encoder = Z3Encoder(tracetanker)
        self.branchstack = []
        self.encodeflag = True
        self.event2cons = {}
        self.branch2extend = {}
        self.branch2execend = {}
        self.branch2canflip = {}
        self.var2def = {}
        self.slicer = TraceSlicer(self.var2def, self.branch2extend, self.branch2execend)
        # self.initial_z3cons = self.z3encoder.encode_typevals()
        self.useslice = useslice
        self.cons_counter = 0

    def getcons(self, event):
        return self.event2cons.get(event, [])

    def pkgevents(self, event):
        nowevents = self.event2cons.keys()
        nowevents.sort(key=lambda e: e.gid)
        events = []
        for e in nowevents:
            if e.gid < event.gid:
                events.append(e)
            else:
                break
        events.sort(key=lambda e: e.gid)
        # logging.debug('++++++++++++++++++++++events number: {0}'.format(len(events)))
        return events

    def pkgcons(self, event, checkcons, dump=False):
        def appendcons(cons):
            if not isinstance(cons, list):
                cons = [cons]
            if branchstack:
                racybranch = branchstack[-1]
                if racybranch.currbranch:
                    racybranch.execbcons.extend(cons)
                else:
                    racybranch.extbcons.extend(cons)
            else:
                constraints.extend(cons)
            cons_num[0] += len(cons)
            if dump:
                dlogger.debug('\t--------->{}'.format(
                    [str(c).replace('\n', '').replace('  ', '') for c in cons]))
        if dump:
            dlogger = logging.getLogger('z3consdumper')
        cons_num = [len(checkcons)]
        # constraints = list(self.initial_z3cons)
        constraints = list(self.z3encoder.typevalcons)
        branchstack = []
        if self.useslice:
            segevents = self.slicer.fastslice(event)
            if dump:
                with open('sevents', 'w') as sefile:
                    for e in segevents:
                        print >>sefile, e
        else:
            segevents = self.pkgevents(event)
        for se in segevents:
            if dump:
                dlogger.debug('\t{0}'.format(se))
            if isinstance(se.inst, BranchInst):
                branchstack.append(AnalysisBranch(se))
            elif isinstance(se.inst, ExtBranchEndInst):
                assert branchstack, 'the branch stack should not be empty'
                racyanalbranch = branchstack[-1]
                assert racyanalbranch.branch.inst.bid == se.inst.bid, 'branch dismatch'
                racyanalbranch.currbranch = 1
            elif isinstance(se.inst, ExecBranchEndInst):
                assert branchstack, 'the branch stack should not be empty'
                analbranch = branchstack.pop()
                assert analbranch.branch.inst.bid == se.inst.bid, 'branch dismatch'
                testcons = self.event2cons[analbranch.branch][0]
                if analbranch.branch.inst.resolved and self.branch2canflip[analbranch.branch]:
                    appendcons(If(testcons, And(*analbranch.execbcons),
                                  And(*analbranch.extbcons) if analbranch.extbcons else True))
                else:
                    appendcons(And(testcons, *analbranch.execbcons))
            else:
                appendcons(self.getcons(se))
        appendcons(checkcons)
        for analbranch in branchstack:
            testcons = self.event2cons[analbranch.branch][0]
            if analbranch.currbranch:
                if analbranch.execbcons:
                    constraints.append(testcons)
                    constraints.extend(analbranch.execbcons)
            else:
                if analbranch.extbcons:
                    constraints.append(Not(testcons))
                    constraints.extend(analbranch.extbcons)
        if __debug__:
            logger.debug('\t=========>{}'.format(
                [str(c).replace('\n', '').replace('  ', '') for c in checkcons]))
        logger.info('++++++++++++++++++++++++++++++++')
        logger.info('Sliced Contraints number: {0}'.format(cons_num[0]))
        if dump:
            with open('cons', 'w') as f:
                print >>f, '\n'.join([str(c).replace('\n', '').replace('  ', '') for c in constraints])
        return constraints

    def run(self, start=0, stop=None):
        stop = stop if stop is not None else len(self.tanker.gid2event)
        starttime = time.time()
        for event in self.tanker.gid2event.itervalues():
            if start <= event.gid <= stop:
                try:
                    self.flowthrough(event)
                except TypeInconsError as error:
                    logger.info(error.message)
                    break
        stoptime = time.time()
        logger.info('The Total Time: {0}s...'.format(stoptime - starttime))

    def flowthrough(self, event):
        if self.branchstack:
            event.setctrls([self.branchstack[-1].branch])
        esig = TraceType.geteventsig(event.type).lower()
        if self.encodeflag or (event.type == TraceType.EXTBRANCHEND):
            if __debug__:
                logger.debug('flow through[' + str(event.gid) +
                             ']<' + esig + '> ' + str(event))
            if event.type in self.checktypes:
                self.apply_check(event, False)
            flowthrough = getattr(self, 'flowthrough_' + esig, self.default_flowthrough)
            cons = flowthrough(event)
            if __debug__:
                logger.debug('--------->{}'.format(
                             [str(c).replace('\n', '').replace('  ', '') for c in cons]))
            self.event2cons[event] = cons
            self.cons_counter += len(cons)
        if event.gid == 67407:
        # if (event.gid >= 5000) and (event.gid % 200 == 0):
            logger.debug('------------------------------------------')
            hassolution = self.timedsolve(self.pkgcons(event, [], False), True, 30)[0]
            logger.debug(hassolution)
            logger.debug('------------------------------------------')
            if not hassolution:
                sys.exit(1)

    @staticmethod
    def timedsolve(cons, needmodel=True, timeout=None):
        def wrappedsolve(c, m, q):
            q.put(PredictiveEngine.solve(c, m))
        queue = Queue()
        p = Process(target=wrappedsolve, args=(cons, needmodel, queue))
        p.start()
        p.join(timeout=timeout)
        if p.is_alive():
            p.terminate()
            return [-1, None]
        return queue.get()

    @staticmethod
    def solve(cons, needmodel=True):
        z3solver = Solver()
        z3solver.add(*cons)
        r = z3solver.check()
        if str(r) == 'sat':
            return True, z3solver.model() if needmodel else None
        if __debug__:
            logger.info('<check-pass>')
        return False, None

    def getsolution(self, z3model):
        # for x in z3model:
        #     if '\n' not in str(z3model[x]):
        #         print x, str(z3model[x]).replace('\n', ' ')
        # print z3model[Int('O23._cookies_#871')]
        # print z3model[Int('resp_#9723')]
        # print z3model[self.z3encoder.rosig2z3osigvar['TAO1358_#9963']]
        # print z3model[Int('$179_#993')]
        # print z3model[Int('$72_#3200')]

        # print z3model[self.z3encoder.getz3attrsetvar('TAO52_#77')]
        # print z3model[Int('$95_#131')]
        sol = {}
        for var in self.z3encoder.infiniteinputz3vars:
            sol[str(var)] = z3model[var]
        for var, val, idx in self.z3encoder.finiteinputz3vars:
            if val == int(str(z3model[var])):
                sol[str(var)] = 'idx-' + str(idx)
        return sol

    def apply_check(self, event, dumpcons=False):
        inextbranch = self.branchstack and (self.branchstack[-1].currbranch == 0)
        shouldcheck, constraints = self.z3encoder.encodecheck(inextbranch, event)
        if shouldcheck:
            self.cons_counter += len(constraints)
            starttime = time.time()
            r, m = self.solve(self.pkgcons(event, constraints, dumpcons))
            stoptime = time.time()
            logger.info('All Constraints number: {0}'.format(self.cons_counter))
            logger.info('Solving Time: {0}s...'.format(stoptime - starttime))
            logger.info('++++++++++++++++++++++++++++++++')
            self.cons_counter -= len(constraints)
            if r:
                raise TypeInconsError('Find a new type inconsistency',
                                      self.getsolution(m))

    def default_flowthrough(self, event):
        for var in event.getdefs():
            self.var2def[var] = event
        return self.z3encoder.encode(event)

    def flowthrough_branch(self, event):
        # checkcons = self.z3encoder.encodecheck(False, event)
        # r, _ = self.solve(self.pkgcons(event, checkcons), needmodel=False)
        r = True
        if not r:
            self.encodeflag = False
            self.branch2canflip[event] = False
        else:
            self.branch2canflip[event] = True
            logger.info('the branch<{0}> can be included'.format(event.inst.bid))
        self.branchstack.append(AnalysisBranch(event))
        return self.default_flowthrough(event)

    def flowthrough_extbranchend(self, event):
        assert self.branchstack, 'the branch stack should not be empty'
        racyanalbranch = self.branchstack[-1]
        assert racyanalbranch.branch.inst.bid == event.inst.bid, 'branch dismatch'
        racyanalbranch.currbranch = 1
        self.encodeflag = True
        self.branch2extend[racyanalbranch.branch] = event
        return []

    def flowthrough_execbranchend(self, event):
        assert self.branchstack, 'the branch stack should not be empty'
        racyanalbranch = self.branchstack[-1]
        assert racyanalbranch.branch.inst.bid == event.inst.bid, \
            'branch dismatch: ' + str(racyanalbranch.branch.inst.bid) + ', ' + str(event.inst.bid)
        self.branch2execend[racyanalbranch.branch] = event
        self.branchstack.pop()
        return []


class ProcessedSolver(Process):
    def __init__(self, *args, **kwargs):
        super(ProcessedSolver, self).__init__(*args, **kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)
            print self._return

    def join(self, *args, **kwargs):
        super(ProcessedSolver, self).join(*args, **kwargs)
        if self.is_alive():
            self.terminate()
        print self._return
        return self._return
