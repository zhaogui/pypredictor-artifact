#!/usr/bin/python
# _*_ coding:utf-8 _*_

from ast import *
import logging

import tracenodes as new
from basetracemaker import BaseTraceMaker


logger = logging.getLogger('pytracer')


class ReturnTraceMakerMixIn(object):
    def new_allocate(self, tname, value=None, aframe=None):
        if aframe is None:
            aframe = self.backaframe
        target = aframe.new_id(tname, value)
        return new.Allocate(target, target.value)

    def errorinfo(self, msg):
        return ('ReturnTrace- {0} [in <{1}> '
                'file: {2}]'.format(msg,
                                    self.backaframe.funcname
                                    if self.backaframe else None,
                                    self.backaframe.filename
                                    if self.backaframe else None))


class AssignTraceMakerMixIn(object):
    def make_assign(self, ce_return, ce_return_val, callsite):
        """Visit a node."""
        method = 'make_{0}_assign_{1}'.format(callsite.value.__class__.__name__.lower(),
                                              callsite.targets[0].__class__.__name__.lower())
        maker = getattr(self, method, self.default_make_assign)
        maker(ce_return, ce_return_val, callsite)

    def default_make_assign(self, ce_return, ce_return_val, callsite):
        ca_target_id = callsite.targets[0].id
        # explicit return
        if isinstance(ce_return, Return) and (ce_return.value is not None):
            ca_target = self.backaframe.new_id(ca_target_id, new.HeapObject(ce_return_val))
            ce_target = self.get_id(ce_return.value.id)
            # self.aframe.get_racyid(ce_return.value.id)
            self.append_trace(new.Assign(ca_target, ce_target))
        else:
            self.append_trace(self.new_allocate(ca_target_id, new.HeapObject(ce_return_val)))

    def make_call_assign_name(self, ce_return, ce_return_val, callsite):
        ca_target_id = callsite.targets[0].id
        # case the __init__ method for class instantiation, we assume the first argument
        # is hard code as 'self'
        frame = self.aframe.eframe
        if (frame.f_code.co_name == '__init__') and ('self' in frame.f_locals):
            self_val = new.HeapObject(self.liveobject('self'))
            self.append_trace(self.new_allocate(ca_target_id, self_val))
        else:
            #TODO: need to fix when the caller is not current function
            # e.g. names = filter(lambda x: x[0] != '.', names)
            # the return of lambda cannot be assigned to names
            self.default_make_assign(ce_return, ce_return_val, callsite)

    def make_unaryop_assign_name(self, ce_return, ce_return_val, callsite):
        if isinstance(callsite.value.op, Not):
            self.append_trace(self.new_allocate(callsite.targets[0].id,
                                                new.HeapObject(not ce_return_val)))
        else:
            self.default_make_assign(ce_return, ce_return_val, callsite)

    def make_binop_assign_name(self, ce_return, ce_return_val, callsite):
        self.default_make_assign(ce_return, ce_return_val, callsite)

    def make_compare_assign_name(self, ce_return, ce_return_val, callsite):
        # self.default_make_assign(ce_return, ce_return_val, callsite)
        # # Fix: meeting the cmpop 'isnot' and 'notin', we need revert the result
        # if isinstance(callsite.value.ops[0], (IsNot, NotIn)):
        #     old_target = self.backaframe.get_racyid(callsite.targets[0].id)
        #     new_target = self.backaframe.new_id(callsite.targets[0].id,
        #                                         new.HeapObject(not ce_return_val))
        #     self.append_trace(new.UopAssign(new_target, old_target, Not()))
        # # Fix: meeting the cmpop '>|<|>=|<=|==' overrided with __cmp__
        # if isinstance(callsite.value.ops[0], (Gt, GtE, Lt, LtE)) \
        #         and self.aframe.funcname == '__cmp__':
        #     #TODO: the logic of each cmpop with the return of __cmp__
        #     pass
        # ignore the return, the target will be created when it is used
        pass

    def make_attribute_assign_name(self, ce_return, ce_return_val, callsite):
        self.default_make_assign(ce_return, ce_return_val, callsite)

    def make_subscript_assign_name(self, ce_return, ce_return_val, callsite):
        self.default_make_assign(ce_return, ce_return_val, callsite)

    def make_name_assign_attribute(self, ce_return, ce_return_val, callsite):
        pass

    def make_name_assign_subscript(self, ce_return, ce_return_val, callsite):
        pass


class ReturnTraceMaker(BaseTraceMaker, ReturnTraceMakerMixIn, AssignTraceMakerMixIn):

    # def check_ifendofbranch(self, aframe):
    #     if aframe.branchstack:
    #         logger.debug(str(aframe.currstmt.lineno) + ', ' + str(aframe.branchstack[-1].laststmtlineno))
    #     return aframe.branchstack and \
    #         aframe.currstmt.lineno == aframe.branchstack[-1].laststmtlineno

    def make(self):
        self.results = []
        # case the return of the  __exit__ call in the 'with' stmt,
        # should not handle as true return
        if self.backaframe and self.aframe.funcname != '__exit__':
            callsite = self.backaframe.currstmt
            ce_return = self.aframe.currstmt
            ce_return_val = None
            if isinstance(ce_return, Return) and (ce_return.value is not None):
                ce_return_val = self.aframe.liveobject(ce_return.value.id)
            method = 'make_' + callsite.__class__.__name__.lower()
            maker = getattr(self, method, self.default_make)
            maker(ce_return, ce_return_val, callsite)
            self.bindinfo(self.results, self.backaframe)
        # TODO: some problems caused by the yield statement (not a true return)
        # if not(isinstance(ce_return, Expr)
        #        and isinstance(ce_return.value, Yield)):
        while self.aframe.branchstack:
            self.post_branchend(self.aframe)
        return self.results

    def default_make(self, ce_return, ce_return_val, callsite):
        # logger.warning(self.errorinfo('not implement this kind callsite:'
        #                               ' {0}'.format(dump(callsite))))
        pass

    def make_assign(self, ce_return, ce_return_val, callsite):
        super(ReturnTraceMaker, self).make_assign(ce_return, ce_return_val, callsite)

    def make_augassign(self, ce_return, ce_return_val, callsite):
        super(ReturnTraceMaker, self).default_make_assign(ce_return, ce_return_val, callsite)

    def make_with(self, ce_return, ce_return_val, callsite):
        if callsite.optional_vars is not None:
            ca_target_id = callsite.optional_vars.id
            # explicit return
            if isinstance(ce_return, Return) and (ce_return.value is not None):
                ca_target = self.backaframe.new_id(ca_target_id, new.HeapObject(ce_return_val))
                self.append_trace(new.Assign(ca_target, self.aframe.get_racyid(ce_return.value.id)))
            else:
                self.append_trace(self.new_allocate(ca_target_id, new.HeapObject(ce_return_val)))

    def make_for(self, ce_return, ce_return_val, callsite):
        ca_target_id = callsite.target.elts[1].id
        if isinstance(ce_return, Expr) \
                and isinstance(ce_return.value, Yield) \
                and (ce_return.value.value is not None):
            ca_target = self.backaframe.new_id(ca_target_id, new.HeapObject(ce_return_val))
            # here, we change self.aframe.get_racyid to self.get_id, which is caused by the issue
            # introduced by the yield(not true return)
            self.append_trace(new.Assign(ca_target, self.get_id(ce_return.value.value.id)))
        else:
            self.append_trace(self.new_allocate(ca_target_id, new.HeapObject(ce_return_val)))

    def make_if(self, ce_return, ce_return_val, callsite):
        # TODO: need to add the branch inst
        if isinstance(ce_return, Return) and (ce_return.value is not None):
            # branch = self.make_test(ce_return.value.id, ce_return_val)
            # self.append_trace(new.Allocate(branch.test, branch.test.value))
            self.append_trace(self.new_allocate(ce_return.value.id, new.HeapObject(ce_return_val)))
        else:
            # logger.debug(self.errorinfo('not return in make_if {0}'.format(dump(callsite))))
            tmpid = self.new_tmpid(ce_return_val)
            # branch = self.make_test(tmpid.name, ce_return_val)
            # self.append_trace(new.Allocate(branch.test, branch.test.value))
            self.append_trace(new.Allocate(tmpid, tmpid.value))

    def make_while(self, ce_return, ce_return_val, callsite):
        if isinstance(ce_return, Return) and (ce_return.value is not None):
            # branch = self.make_test(ce_return.value.id, ce_return_val)
            # self.append_trace(new.Allocate(branch.test, branch.test.value))
            self.append_trace(self.new_allocate(ce_return.value.id, new.HeapObject(ce_return_val)))
        else:
            # logger.debug(self.errorinfo('not return in make_while {0}'.format(dump(callsite))))
            tmpid = self.new_tmpid(ce_return_val)
            # branch = self.make_test(tmpid.name, ce_return_val)
            # self.append_trace(self.new_allocate(tmpid.name, branch.test.value))
            self.append_trace(new.Allocate(tmpid, tmpid.value))

    def make_functiondef(self, ce_return,  ce_return_val, callsite):
        self.append_trace(self.new_allocate(callsite.name, new.HeapObject(ce_return_val)))

    def make_classdef(self, ce_return, ce_return_val, callsite):
        self.append_trace(self.new_allocate(callsite.name, new.HeapObject(ce_return_val)))

    def make_expr(self, *_):
        pass

    def make_import(self, *_):
        pass

    def make_importfrom(self, *_):
        pass

    def make_classdef(self, *_):
        pass

    def make_delete(self, *_):
        pass