#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import logging
import logging.config
from os.path import dirname, join
from cStringIO import StringIO

from pypredictor import modrecoder


_currdir = dirname(__file__)

logging.config.fileConfig(join(_currdir, "logger", "pyrecorder-logger.conf"))


class _Outputer(object):
    def __enter__(self):
        self._stdout = sys.stdout
        self._content = ''
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self._content = self._stringio.getvalue()
        sys.stdout = self._stdout

    @property
    def content(self):
        return self._content

    def __eq__(self, other):
        return self.content == other.content

    def __repr__(self):
        return self.content
    __str__ = __repr__

if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) == 3:
        # script, record output, running result output
        scriptfile, recordfile, resultfile = args
        ignores = ''
    elif len(args) == 4:
        scriptfile, ignores, recordfile, resultfile = args
    else:
        raise ValueError('arguments do not match <scriptfile, recordfile, resultfile>')

    with _Outputer() as ori_out:
        modrecoder.record(scriptfile, recordfile, ignores=ignores.split('|'))
    with open(resultfile, 'w') as rfile:
        rfile.write(str(ori_out))
        rfile.flush()
    if ori_out.content:
        sys.stdout.write(ori_out.content)