#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import logging
import logging.config
from os.path import dirname, join

from pypredictor import modrewriter


_currdir = dirname(__file__)

logging.config.fileConfig(join(_currdir, "logger", "pyrewriter-logger.conf"))

# logging.getLogger('pytracer').setLevel(logging.INFO)

if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) != 2:
        raise ValueError('arguments do not match <recordfile>')
    recordfile, tmpdir = args
    modrewriter.rewrite(recordfile, tmpdir)
