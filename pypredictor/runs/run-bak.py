#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import logging
import logging.config
from os.path import dirname, join
from subprocess import Popen, PIPE

_currdir = dirname(__file__)

logging.config.fileConfig(join(_currdir, "logger.conf"))
logger = logging.getLogger('pytracer')

oldstdout = sys.stdout


def _runcmd(cmd):
    ''' read the output after it finished '''
    try:
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    except OSError, err:
        print >>sys.stderr,  err.child_traceback
    (stdout, stderr) = p.communicate()
    retcode = p.returncode
    return retcode, stdout, stderr


def _runcmd2(cmd):
    ''' read the output synchronized '''
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    for line in iter(p.stderr.readline, b''):
        print >>sys.stderr, line,
    p.stderr.close()
    p.wait()
    return p.returncode, '',


def runrecorder(scriptfile, recordfile, resultfile):
    cmd = 'python {script} {s} {r1} {r2}'.format(script=join(_currdir, 'runrecorder.py'),
                                                 s=scriptfile, r1=recordfile, r2=resultfile)
    return _runcmd(cmd)


def runrewriter(recordfile, tmpdir):
    cmd = 'python {script} {r} {t}'.format(script=join(_currdir, 'runrewriter.py'),
                                           r=recordfile, t=tmpdir)
    return _runcmd(cmd)


def runtracer(projname, scriptfile, recordfile, tmpdir):
    cmd = 'python {script} {p} {s} {r} {t}'.format(script=join(_currdir, 'runtracer.py'),
                                                   p=projname, s=scriptfile,
                                                   r=recordfile, t=tmpdir)
    return _runcmd2(cmd)


def runrecover(recordfile, tmpdir):
    cmd = 'python {script} {r} {t}'.format(script=join(_currdir, 'runrecover.py'),
                                           r=recordfile, t=tmpdir)
    return _runcmd(cmd)


def runall(projname, scriptfile, recordfile, resultfile, tmpdir):
    logger.info("PHASE 1: record all the modules during the run...")
    retcode, rec_stdout, rec_stderr = runrecorder(scriptfile, recordfile, resultfile)
    sys.stderr.write('=====================Recorder=====================\n')
    sys.stderr.write('[STDOUT]\n')
    sys.stderr.write(rec_stdout + '\n')
    sys.stderr.write('[STDERR]\n')
    sys.stderr.write(rec_stderr + '\n')
    if retcode:
        sys.exit(1)
    sys.stderr.flush()

    logger.info("PHASE 2: rewrite all the modules and generate pyc files...")
    retcode, rew_stdout, rew_stderr = runrewriter(recordfile, tmpdir)
    sys.stderr.write('====================AST Rewriter==================\n')
    sys.stderr.write('[STDOUT]\n')
    sys.stderr.write(rew_stdout + '\n')
    sys.stderr.write('[STDERR]\n')
    print >>sys.stderr, rew_stderr
    if retcode:
        sys.exit(1)
    sys.stderr.flush()

    try:
        logger.info("PHASE 4: trace the code...")
        retcode, trace_stdout, trace_stderr = runtracer(projname, scriptfile, recordfile, tmpdir)
        sys.stderr.write('=======================Tracer====================\n')
        sys.stderr.write('[STDOUT]\n')
        sys.stderr.write(trace_stdout + '\n')
        sys.stderr.write('[STDERR]\n')
        sys.stderr.write(trace_stderr + '\n')
        sys.stderr.flush()
        if retcode:
            sys.exit(1)
    finally:

        logger.info("PHASE 5: recover all the modified modules...")
        retcode, reco_stdout, reco_stderr = runrecover(recordfile, tmpdir)
        sys.stderr.write('====================Recover======================\n')
        sys.stderr.write('[STDOUT]\n')
        sys.stderr.write(reco_stdout + '\n')
        sys.stderr.write('[STDERR]\n')
        sys.stderr.write(reco_stderr + '\n')
        if retcode:
            sys.exit(1)
        sys.stderr.flush()
    sys.stderr.write('=====================TRACE RESULT====================\n')
    if rec_stdout == trace_stdout:
        sys.stderr.write('PASS:)\n')
    else:
        sys.stderr.write('[Recorder Output]\n')
        sys.stderr.write(rec_stdout + '\n')
        sys.stderr.write('[Tracer Output]\n')
        sys.stderr.write(trace_stdout + '\n')
        sys.stderr.write('FAILED:(\n')
    sys.stderr.flush()
    logger.info("finished...")


def runall(projname, scriptfile, recordfile, resultfile, tmpdir):
    logger.info("PHASE 1: record all the modules during the run...")
    retcode, rec_stdout, rec_stderr = runrecorder(scriptfile, recordfile, resultfile)
    sys.stderr.write('=====================Recorder=====================\n')
    sys.stderr.write('[STDOUT]\n')
    sys.stderr.write(rec_stdout + '\n')
    sys.stderr.write('[STDERR]\n')
    sys.stderr.write(rec_stderr + '\n')
    if retcode:
        sys.exit(1)
    sys.stderr.flush()

    logger.info("PHASE 2: rewrite all the modules and generate pyc files...")
    retcode, rew_stdout, rew_stderr = runrewriter(recordfile, tmpdir)
    sys.stderr.write('====================AST Rewriter==================\n')
    sys.stderr.write('[STDOUT]\n')
    sys.stderr.write(rew_stdout + '\n')
    sys.stderr.write('[STDERR]\n')
    print >>sys.stderr, rew_stderr
    if retcode:
        sys.exit(1)
    sys.stderr.flush()

    try:
        logger.info("PHASE 4: trace the code...")
        retcode, trace_stdout, trace_stderr = runtracer(projname, scriptfile, recordfile, tmpdir)
        sys.stderr.write('=======================Tracer====================\n')
        sys.stderr.write('[STDOUT]\n')
        sys.stderr.write(trace_stdout + '\n')
        sys.stderr.write('[STDERR]\n')
        sys.stderr.write(trace_stderr + '\n')
        sys.stderr.flush()
        if retcode:
            sys.exit(1)
    finally:

        logger.info("PHASE 5: recover all the modified modules...")
        retcode, reco_stdout, reco_stderr = runrecover(recordfile, tmpdir)
        sys.stderr.write('====================Recover======================\n')
        sys.stderr.write('[STDOUT]\n')
        sys.stderr.write(reco_stdout + '\n')
        sys.stderr.write('[STDERR]\n')
        sys.stderr.write(reco_stderr + '\n')
        if retcode:
            sys.exit(1)
        sys.stderr.flush()
    sys.stderr.write('=====================TRACE RESULT====================\n')
    if rec_stdout == trace_stdout:
        sys.stderr.write('PASS:)\n')
    else:
        sys.stderr.write('[Recorder Output]\n')
        sys.stderr.write(rec_stdout + '\n')
        sys.stderr.write('[Tracer Output]\n')
        sys.stderr.write(trace_stdout + '\n')
        sys.stderr.write('FAILED:(\n')
    sys.stderr.flush()
    logger.info("finished...")


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) != 5:
        raise ValueError('arguments do not match <scriptfile, recordfile, resultfile>')
    runall(*args)



