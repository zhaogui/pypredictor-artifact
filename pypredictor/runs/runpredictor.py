#!/usr/bin/python
# _*_ coding:utf-8 _*_


import sys
import logging
import logging.config
from os.path import dirname, join
from pypredictor import predengine


_currdir = dirname(__file__)

logging.config.fileConfig(join(_currdir, 'logger', 'pypredictor-logger.conf'))


if __name__ == '__main__':
    args = sys.argv[1:]
    print >>sys.stderr, args
    if len(args) != 2:
        raise ValueError('arguments do not match <projname, outdir>')
    projname, outdir = args
    try:
        predengine.predict(projname, outdir)
    except:
        import traceback
        print >>sys.stderr, traceback.format_exc()
        raise
