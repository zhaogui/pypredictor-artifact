#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import logging
import logging.config
from os.path import dirname, join, abspath
from subprocess import Popen, PIPE


_currdir = dirname(abspath(__file__))

# logging.config.fileConfig(join(_currdir, "logger", "root-logger.conf"))


oldstdout = sys.stdout

# PYINTERPRETER = '/home/gui/Pythons/virtualenvs/python2.7.1/bin/python'
PYINTERPRETER = 'python'


def _runcmd(cmd):
    ''' read the output after it finished '''
    try:
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    except OSError, err:
        print >>sys.stderr,  err.child_traceback
    (stdout, stderr) = p.communicate()
    retcode = p.returncode
    return retcode, stdout, stderr


def _runcmd2(cmd, stderr=sys.stderr, stdout=sys.stderr):
    ''' read the output synchronized '''
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    if stderr:
        print >>stderr, '********[LOG INFO]********'
    for line in iter(p.stderr.readline, b''):
        if stderr:
            print >>stderr, line,
    p.stderr.close()
    #with open('xxxx', 'a') as f:
    #p.wait()
    stdout_str = p.stdout.read()
    if stdout_str and stdout:
        print >>stdout, '*********[STDOUT]*********'
        print >>stdout, stdout_str
    return p.returncode, stdout_str


def runrecorder(scriptfile, ignores, recordfile, resultfile):
    # print >>sys.stderr, '=====================Recorder====================='
    cmd = '{python} {script} {s} {i} {r1} {r2}'.format(python=PYINTERPRETER,
                                                       script=join(_currdir, 'runrecorder.py'),
                                                       s=repr(scriptfile), i=repr(ignores) if ignores else '',
                                                       r1=repr(recordfile), r2=repr(resultfile))
    return _runcmd2(cmd)


def runrewriter(recordfile, tmpdir):
    # print >>sys.stderr, '==================AST Rewriter==================='
    cmd = '{python} {script} {r} {t}'.format(python=PYINTERPRETER,
                                             script=join(_currdir, 'runrewriter.py'),
                                             r=repr(recordfile), t=repr(tmpdir))
    return _runcmd2(cmd)[0]


def runtracer(projname, scriptfile, entryfunc,
              ignores, recordfile, tmpdir, outdir):
    # print >>sys.stderr, '=====================Tracer======================'
    cmd = '{python} {script} {p} {s} {e} {i} {r} {t} {o}'.format(python=PYINTERPRETER,
                                                                 script=repr(join(_currdir, 'runtracer.py')),
                                                                 p=repr(projname), s=repr(scriptfile),
                                                                 e=repr(entryfunc), i=repr(ignores) if ignores else '',
                                                                 r=repr(recordfile), t=repr(tmpdir),
                                                                 o=repr(outdir))
    return _runcmd2(cmd)


def runrecover(recordfile, tmpdir):
    # print >>sys.stderr, '====================Recover======================'
    cmd = '{python} {script} {r} {t}'.format(python=PYINTERPRETER, script=join(_currdir, 'runrecover.py'),
                                             r=repr(recordfile), t=repr(tmpdir))
    return _runcmd2(cmd)[0]


def runpredictor(projname, basedir):
    # print >>sys.stderr, '====================Predictor===================='
    cmd = '{python} {script} {p} {b}'.format(python=PYINTERPRETER,
                                             script=join(_currdir, 'runpredictor.py'),
                                             p=repr(projname), b=repr(basedir))
    return _runcmd2(cmd)[0]


def runotfpredictor(projname, scriptfile, entryfunc,
                    ignores, recordfile, tmpdir, outdir):
    print >>sys.stderr, '==================OTFPredictor==================='
    cmd = '{python} {script} {p} {s} {e} {i} {r} {t} {o}'.format(python=PYINTERPRETER,
                                                                 script=repr(join(_currdir, 'runotfpredictor.py')),
                                                                 p=repr(projname), s=repr(scriptfile),
                                                                 e=repr(entryfunc), i=repr(ignores),
                                                                 r=repr(recordfile), t=repr(tmpdir),
                                                                 o=repr(outdir))
    return _runcmd2(cmd)


def runtracephase(projname, scriptfile, entryfunc,
                  recordfile, resultfile,
                  tmpdir, outdir='output', ignores=None):
    logging.config.fileConfig(join(_currdir, "logger", "pypredictor-logger.conf"))
    logger = logging.getLogger('root')
    print(">>>>>>[PHASE 1] record all the modules during the run...")
    retcode, rec_stdout = runrecorder(scriptfile, ignores, recordfile, resultfile)
    if retcode:
        sys.exit(1)
    print(">>>>>>[PHASE 2] rewrite all the modules and generate pyc files...")
    retcode = runrewriter(recordfile, tmpdir)
    if retcode:
        sys.exit(1)
    try:
        print(">>>>>>[PHASE 3] trace the code...")
        retcode, trace_stdout = runtracer(projname, scriptfile, entryfunc,
                                          ignores, recordfile, tmpdir, outdir)
        if retcode:
            sys.exit(1)
    finally:
        print(">>>>>>[PHASE 4] recover all the modified modules...")
        retcode = runrecover(recordfile, tmpdir)
        if retcode:
            sys.exit(1)

    # sys.stderr.write('=================TRACING FINISHED==============\n')
    if rec_stdout == trace_stdout:
        sys.stderr.write('PASS:)\n')
    else:
        sys.stderr.write('[Recorder Output]\n')
        sys.stderr.write(rec_stdout + '\n')
        sys.stderr.write('[Tracer Output]\n')
        sys.stderr.write(trace_stdout + '\n')
        sys.stderr.write('FAILED:(\n')
    sys.stderr.flush()


def runpredictphase(projname,  outdir='output', useslice=False, lazyencoding=False, start=0, stop=None, dump_trace_log=False):
    logging.config.fileConfig(join(_currdir, "logger", "pypredictor-logger.conf"))
    logger = logging.getLogger('root')
    from pypredictor import predengine
    # sys.stderr.write('=====================Predictor====================\n')
    print(">>>>>>[PHASE 5]: analyze the comprehensive trace...")
    predengine.predict(projname=projname, outdir=outdir,
                       useslice=useslice, lazyencoding=lazyencoding,
                       start=start, stop=stop, dump_trace_log=dump_trace_log)
    # retcode = runpredictor(projname, outdir)
    # if retcode:
    #     sys.exit(1)


# def runotfpredictphase(projname, scriptfile, entryfunc,
#                        ignores, recordfile, resultfile,
#                        tmpdir, outdir=''):
#     logging.config.fileConfig(join(_currdir, "logger", "pyotfpredictor-logger.conf"))
#     logger = logging.getLogger('root')
#     logger.info("PHASE 1: record all the modules during the run...")
#     retcode, rec_stdout = runrecorder(scriptfile, ignores, recordfile, resultfile)
#     if retcode:
#         sys.exit(1)
#
#     logger.info("PHASE 2: rewrite all the modules and generate pyc files...")
#     retcode = runrewriter(recordfile, tmpdir)
#     if retcode:
#         sys.exit(1)
#
#     try:
#         logger.info("PHASE 4: trace the execution and predict bugs on the fly...")
#         retcode, trace_stdout = runotfpredictor(projname, scriptfile, entryfunc,
#                                                 ignores, recordfile, tmpdir, outdir)
#         if retcode:
#             sys.exit(1)
#     finally:
#         logger.info("PHASE 5: recover all the modified modules...")
#         retcode = runrecover(recordfile, tmpdir)
#         if retcode:
#             sys.exit(1)


def runall(projname, scriptfile, entryfunc, ignores,
           recordfile, resultfile, tmpdir,
           outdir='', start=0, stop=None):
    logging.config.fileConfig(join(_currdir, "logger", "pypredictor-logger.conf"))
    logger = logging.getLogger('root')
    logger.info('PHASE ONE: BEGIN THE TRACE PHASE...')
    runtracephase(projname, scriptfile, entryfunc, ignores,
                  recordfile, resultfile, tmpdir, outdir)
    logger.info('PHASE TWO: BEGIN THE PREDICT PHASE...')
    runpredictphase(projname, start=start, stop=stop)
    logger.info("finished...")


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) != 5:
        raise ValueError('arguments do not match <scriptfile, recordfile, resultfile>')
    runall(*args)



