#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import logging
import logging.config
from os.path import dirname, join

from pypredictor import tracer


_currdir = dirname(__file__)

logging.config.fileConfig(join(_currdir, "logger", "pytracer-logger.conf"))


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) == 6:
        projname, scriptfile, entryfunc, recordfile, tmpdir, outdir = args
        ignores = ''
    elif len(args) == 7:
        projname, scriptfile, entryfunc, ignores, recordfile, tmpdir, outdir = args
    else:
        raise ValueError('arguments do not match <projname, scriptfile, recordfile>')

    try:
        tracer.trace(projname=projname,
                     scriptfile=scriptfile,
                     entryfunc=entryfunc,
                     recordfile=recordfile,
                     ignores=ignores.split('|'),
                     pkldir=tmpdir,
                     outdir=outdir)
    except:
        import traceback
        print >>sys.stderr, traceback.format_exc()
        raise

