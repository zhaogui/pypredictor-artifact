#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import logging
import logging.config
from os.path import dirname, join

from pypredictor import modrecover


_currdir = dirname(__file__)

logging.config.fileConfig(join(_currdir, "logger", "pyrecover-logger.conf"))


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) != 2:
        raise ValueError('arguments do not match <recordfile>')
    recordfile, tmpdir = args[0], args[1]
    modrecover.recover(recordfile, tmpdir)