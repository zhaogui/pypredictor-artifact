#!/usr/bin/python
# _*_ coding:utf-8 _*_

#!/usr/bin/python
# _*_ coding:utf-8 _*_

import logging
from threading import Lock

import tracenodes as new
import analysisnodes as analysis
from pypredictor.dbengine import TraceTanker
from tracemaker import NameCreator
from exttracemaker import ExtTraceMaker
from analysisnodes import Value2ROsig as V2RO


logger = logging.getLogger('pytracer')


class PtoSetAnalyzer(object):
    def __init__(self, tanker):
        self.tkr = tanker
        self.var2ptoset = {}

    def save_ptoset(self, var, ptoset):
        # var -> tid,[ctid,] valsig, osig, attrsetid|tid,[ctid,] valsig, osig, attrsetid|...
        var = var.name
        if var in self.var2ptoset:
            self.var2ptoset[var].update(ptoset)
        else:
            self.var2ptoset[var] = ptoset
        self.tkr.save_ptoset(var, ptoset)

    def prop_ptoset(self, val, tgt):
        val, tgt = val.name, tgt
        if val in self.var2ptoset:
            self.save_ptoset(tgt, set(self.var2ptoset[val]))

    def get_ptoset(self, var):
        return self.var2ptoset.get(var, set())


class ConsVarAnalyzer(object):
    def __init__(self, tanker):
        self.tkr = tanker
        self.consvars = set()

    def save_consvar(self, var):
        self.consvars.add(var)

    def prop_consvar(self, vals, tgt):
        if not hasattr(vals, '__iter__'):
            vals = (vals,)
        for val in vals:
            if val not in self.consvars:
                break
        else:
            self.save_consvar(tgt)

    def check_consvar(self, var):
        return var in self.consvars


class DefUseReconstructor(object):
    def __init__(self):
        pass


class TraceAnalyzer(object):
    def __init__(self, tracetanker):
        self.tracetanker = tracetanker
        self.psetanalyer = PtoSetAnalyzer(tracetanker)
        self.cvaranalyer = ConsVarAnalyzer(tracetanker)
        self.branchstack = []
        self.heap_name2racyid = {}
        self.aset_osig2racyosig = {}
        self.curraframe = None
        self.why = ''
        self.lock = Lock()    # lock for atomicity
        self.predef_heapname = set()

    def new_analevent(self, event, inst):
        thid, sid = self.tracetanker.get_thid_sid(event)
        return analysis.EventMaker.make(thid=thid, sid=sid,
                                        inst=inst, type=event.typeid,
                                        ext=(not event.fromexec))

    def new_racyosig(self, osig):
        racyosig = NameCreator.new_osig(osig)
        self.update_racyosig(osig, racyosig)
        return racyosig

    def update_racyosig(self, osig, racyosig):
        if self.branchstack:
            self.branchstack[-1].update_aset_osig(osig, racyosig)
        else:
            self.aset_osig2racyosig[osig] = racyosig

    def get_racyosig(self, osig):
        for i in range(len(self.branchstack)-1, -1, -1):
            racyosig = self.branchstack[i].get_aset_racyosig(osig)
            if racyosig:
                return racyosig
        return self.aset_osig2racyosig.get(osig)

    def new_heap_racyid(self, heaploc, value):
        if not isinstance(value, new.HeapObject):
            value = new.HeapObject(value)
        racyid = new.Id(NameCreator.new_name(heaploc), value)
        self.update_heap_racyid(heaploc, racyid)
        return racyid

    def update_heap_racyid(self, heaploc, racyid):
        if self.branchstack:
            self.branchstack[-1].update_heap_racyid(heaploc, racyid)
        else:
            self.heap_name2racyid[heaploc] = racyid

    def get_heap_racyid(self, heaploc):
        for i in range(len(self.branchstack)-1, -1, -1):
            racyid = self.branchstack[i].get_heap_racyid(heaploc)
            if racyid:
                return racyid
        return self.heap_name2racyid.get(heaploc)

    def confirm_heaplocdef(self, traces, heaploc, heapval):
        racyid = self.get_heap_racyid(heaploc)
        inptoset = False
        if racyid:
            try:
                for v in self.psetanalyer.get_ptoset(racyid.name):
                    if v.liveobj is heapval.liveobj:
                        inptoset = True
                        break
            except RuntimeError:  # here may occur maxium recursion exception
                inptoset = False
        if not (racyid and inptoset):
            if not isinstance(heapval, new.HeapObject):
                heapval = new.HeapObject(heapval)
            racyid = self.new_heap_racyid(heaploc, heapval)
            traces.append(self.visit(new.Allocate(racyid, heapval)))
            self.predef_heapname.add(racyid.name)
        return racyid

    def flowthrough(self, why, aframe, events):
        self.lock.acquire()
        self.why = why
        self.curraframe = aframe
        analevents = []
        for t in events:
            e = self.visit(t)
            if isinstance(e, list):
                analevents.extend(e)
            elif e:
                analevents.append(e)
        self.lock.release()
        return analevents

    def visit(self, event):
        methodname = 'visit_' + event.__class__.__name__.lower()
        visit = getattr(self, methodname, self.default_visit)
        return visit(event)

    def default_visit(self, event):
        return self.new_analevent(event)

    def visit_allocate(self, event):
        tgt, val = event.target, event.value
        self.tracetanker.save_var(tgt)
        self.psetanalyer.save_ptoset(tgt, {val})
        self.cvaranalyer.save_consvar(tgt)
        # inst = repr(event) + ',' + self.new_racyosig(event.value.symstr)
        inst = analysis.AllocateInst(tgt.name,
                                     V2RO(val.valstr,
                                          self.new_racyosig(val.symstr)))
        return self.new_analevent(event, inst)

    def visit_infiniterelaxallocate(self, event):
        tgt, val = event.target, event.value
        self.tracetanker.save_var(tgt)
        self.psetanalyer.save_ptoset(tgt, {val})
        # inst = repr(event) + ',' + self.new_racyosig(event.value.symstr)
        inst = analysis.InfiniteRelaxAllocateInst(tgt.name,
                                                  V2RO(val.valstr,
                                                  self.new_racyosig(val.symstr)))
        return self.new_analevent(event, inst)

    def visit_finiterelaxallocate(self, event):
        tgt, vals = event.target, event.values
        self.tracetanker.save_var(tgt)
        self.psetanalyer.save_ptoset(tgt, vals)
        # valstr = '|'.join(val.valstr + ',' + self.new_racyosig(val.symstr)
        #                   for val in event.values)
        # sig = event.target.name + '=' + valstr
        values = [V2RO(val.valstr, self.new_racyosig(val.symstr)) for val in vals]
        inst = analysis.FiniteRelaxAllocateInst(tgt.name, values)
        return self.new_analevent(event, inst)

    def visit_assign(self, event):
        tgt, val = event.target, event.value
        self.tracetanker.save_var(tgt)
        self.psetanalyer.prop_ptoset(val, tgt)
        self.cvaranalyer.prop_consvar(val, tgt)
        inst = analysis.AssignInst(tgt.name, val.name)
        return self.new_analevent(event, inst)

    def visit_attrload(self, event):
        tgt, val, attr = event.target, event.value, event.attr
        self.tracetanker.save_var(tgt)
        self.tracetanker.save_attrs([attr])
        traces = []
        # estr = repr(event)
        ptoset = self.psetanalyer.get_ptoset(val.name)
        base2oattr = {}
        for o in ptoset:
            # ostr = '{val}={vsig},{racyosig}?'.format(val=val.name, vsig=o.valstr,
            #                                          racyosig=self.get_racyosig(o.symstr))
            if hasattr(o.liveobj, event.attr):
                # here, not all attr objects have the same id
                # (i.e. instance methods or builtinfunctions)
                # get the value of the heap location
                if o.liveobj is val.value.liveobj:
                    oattrval = tgt.value.liveobj
                else:
                    oattrval = getattr(o.liveobj, attr)
                heaploc = NameCreator.attrsig(o, attr)
                heaploc_racyid = self.confirm_heaplocdef(traces, heaploc, new.HeapObject(oattrval))
                # ostr += '{tgt}={oattr}'.format(tgt=tgt.name, oattr=heaploc_racyid.name)
                self.psetanalyer.prop_ptoset(heaploc_racyid, tgt)
                if len(ptoset) == 1:
                    self.cvaranalyer.prop_consvar(heaploc_racyid, tgt)
                oattr = heaploc_racyid.name
            else:
                logger.debug('can not find the attribute {attr} of '
                               'value {value} in file<{file}>'.format(attr=attr, value=o,
                                                                      file=event.filename))
                oattr = ''
            # estr += ('|' + ostr)
            base = V2RO(o.valstr, self.get_racyosig(o.symstr))
            base2oattr[base] = oattr
        inst = analysis.AttrLoadInst(tgt.name, val.name, attr, base2oattr)
        traces.append(self.new_analevent(event, inst))
        return traces

    def visit_attrstore(self, event):
        tgt, attr, val = event.target, event.attr, event.value
        self.tracetanker.save_attrs([attr])
        traces = []
        # estr = repr(event)
        base2target = {}
        base2preval = {}
        base2newosig = {}
        for tv in self.psetanalyer.get_ptoset(tgt.name):
            heaploc = NameCreator.attrsig(tv, attr)
            preracyheaploc = self.get_heap_racyid(heaploc)
            newracyheaploc = self.new_heap_racyid(heaploc, val.value)
            self.tracetanker.save_var(newracyheaploc)
            self.psetanalyer.prop_ptoset(val, newracyheaploc)
            self.cvaranalyer.prop_consvar(val, newracyheaploc)
            # ostr = '{tgt}={vsig}?{oattr}={val}' \
            #        ':{orioattr}'.format(tgt=tgt.name, vsig=tv.valstr,
            #                             oattr=newracyheaploc.name, val=val.name,
            #                             orioattr=preracyheaploc.name if preracyheaploc else '')
            # ostr += ',{osig1}->{osig2}'.format(osig1=self.get_racyosig(tv.symstr),
            #                                    osig2=self.new_racyosig(tv.symstr))
            # estr += ('|' + ostr)

            base = V2RO(tv.valstr, self.get_racyosig(tv.symstr))
            base2target[base] = newracyheaploc.name  # new heap location
            base2preval[base] = preracyheaploc.name if preracyheaploc else ''  # old racy heap location
            base2newosig[base] = self.new_racyosig(tv.symstr)  # new racy object signature
        inst = analysis.AttrStoreInst(tgt.name, attr, val.name, base2target, base2preval, base2newosig)
        traces.append(self.new_analevent(event, inst))
        return traces

    def visit_subload(self, event):
        tgt, val, idx = event.target, event.value, event.index
        idx_val = idx.value.liveobj
        self.tracetanker.save_var(tgt)
        traces = []
        # estr = repr(event)
        ptoset = self.psetanalyer.get_ptoset(val.name)
        base2osub = {}
        for v in ptoset:
            # ostr = '{val}={vsig},{racyosig}?'.format(val=val.name, vsig=v.valstr,
            #                                          racyosig=self.get_racyosig(v.symstr))
            try:
                oval = v.liveobj[idx_val]
                heaploc = NameCreator.subsig(v, idx.value)
                oidx_id = self.confirm_heaplocdef(traces, heaploc, new.HeapObject(oval))
                # ostr += '{tgt}={oidx}'.format(tgt=tgt.name, oidx=oidx_id.name)
                self.psetanalyer.prop_ptoset(oidx_id, tgt)
                if len(ptoset) == 1:
                    self.cvaranalyer.prop_consvar(oidx_id, tgt)
                # import sys
                # print >>sys.stderr, tgt.name, \
                #     'subsig:=>', self.subsig(v, idx.value),  self.name2racyid.get(self.subsig(v, idx.value)), \
                #     'tgt_val:=>', tgt.value.liveobj, \
                #     'subval pset:=>', list(self.get_ptoset(oidx_id.name))[0].liveobj, \
                #     'subval val:=>', oval
                osub = oidx_id.name
            except (TypeError, KeyError, IndexError, AttributeError):
                logger.debug('can not find the attribute __getitem__ of '
                               'value {value} in file<{file}>'.format(value=v.valstr,
                                                                      file=event.filename))
                osub = ''
            # finally:
            #     estr += ('|' + ostr)
            base = V2RO(v.valstr, self.get_racyosig(v.symstr))
            base2osub[base] = osub
        inst = analysis.SubLoadInst(tgt.name, val.name, idx.name, base2osub)
        traces.append(self.new_analevent(event, inst))
        return traces

    def visit_substore(self, event):
        tgt, idx, val = event.target, event.index, event.value
        traces = []
        # estr = repr(event)
        base2target = {}
        base2preval = {}
        ptoset = self.psetanalyer.get_ptoset(tgt.name)
        for tv in ptoset:
            subsig = NameCreator.subsig(tv, idx.value)
            preosubid = self.get_heap_racyid(subsig)
            osubid = self.new_heap_racyid(subsig, val.value)
            self.tracetanker.save_var(osubid)
            # ostr = '{tgt}={vsig},{racyosig}?{oattr}={val}' \
            #        ':{orioattr}'.format(tgt=tgt.name, vsig=tv.valstr,
            #                             racyosig=self.get_racyosig(tv.symstr),
            #                             oattr=osubid.name, val=val.name,
            #                             orioattr=preosubid.name if preosubid else '')
            # estr += ('|' + ostr)
            self.psetanalyer.prop_ptoset(val, osubid)
            if len(ptoset) == 1:
                self.cvaranalyer.prop_consvar(val, osubid)
            base = V2RO(tv.valstr, self.get_racyosig(tv.symstr))
            base2target[base] = osubid.name
            base2preval[base] = preosubid.name if preosubid else ''
        inst = analysis.SubStoreInst(tgt.name, idx.name, val.name, base2target, base2preval)
        traces.append(self.new_analevent(event, inst))
        return traces

    def visit_bopassign(self, event):
        tgt, val1, val2, op = event.target, event.value1, event.value2, event.op
        self.tracetanker.save_var(tgt)
        self.psetanalyer.save_ptoset(tgt, {tgt.value})
        self.cvaranalyer.prop_consvar((val1, val2), tgt)
        inst = analysis.BopAssignInst(tgt.name, val1.name, val2.name, op)
        return self.new_analevent(event, inst)

    def visit_uopassign(self, event):
        tgt, val, op = event.target, event.value, event.op
        self.tracetanker.save_var(tgt)
        self.psetanalyer.save_ptoset(tgt, {tgt.value})
        self.cvaranalyer.prop_consvar(val, tgt)
        inst = analysis.UopAssignInst(tgt.name, val.name, op)
        return self.new_analevent(event, inst)

    def visit_methodinvoke(self, event):
        rec, attr = event.receiver, event.attr
        pset = self.psetanalyer.get_ptoset(rec.name)
        # print >>sys.stderr, [v.symstr for v in pset], repr(event)
        execval = rec.value.liveobj
        try:
            execmthd = getattr(execval, attr)
            funcobj = getattr(execmthd, 'im_func')
        except AttributeError:
            funcobj = None
        axy = []
        for p in pset:
            if p.liveobj is execval:
                same_func = 1
            else:
                try:
                    p_execmthd = getattr(p.liveobj, attr)
                    p_funcobj = getattr(p_execmthd, 'im_func')
                    if funcobj and (funcobj is p_funcobj):
                        same_func = 1
                    else:
                        same_func = 0
                except AttributeError:
                    same_func = 0
            # axy.append('{},{},{}'.format(same_func, p.valstr, self.get_racyosig(p.symstr)))
            axy.append((same_func, V2RO(p.valstr, self.get_racyosig(p.symstr))))
        # estr = repr(event) + '|' + '|'.join(axy)
        inst = analysis.MethodInvokeInst(rec.name, attr, axy)
        return self.new_analevent(event, inst)

    def visit_functioncall(self, event):
        pset = self.psetanalyer.get_ptoset(event.func.name)
        # estr = repr(event) + '|' + '|'.join(v.valstr + ',' +
        #                                     self.get_racyosig(v.symstr) for v in pset)
        vals = [V2RO(v.valstr, self.get_racyosig(v.symstr)) for v in pset]
        inst = analysis.FunctionCallInst(event.func.name, vals)
        return self.new_analevent(event, inst)

    def visit_typeassume(self, event):
        tgt, val, ty = event.target, event.value, event.type
        self.tracetanker.save_var(tgt)
        self.psetanalyer.save_ptoset(tgt, {tgt.value})
        self.cvaranalyer.prop_consvar((val, ty), tgt)
        inst = analysis.TypeCheckInst(tgt.name, val.name, ty.name)
        return self.new_analevent(event, inst)

    def visit_subtypeassume(self, event):
        tgt, val, ty = event.target, event.value, event.type
        self.tracetanker.save_var(tgt)
        self.psetanalyer.save_ptoset(tgt, {tgt.value})
        self.cvaranalyer.prop_consvar((val, ty), tgt)
        inst = analysis.SubTypeCheckInst(tgt.name, val.name, ty.name)
        return self.new_analevent(event, inst)

    def visit_hasattrassume(self, event):
        tgt, val, attr = event.target, event.value, event.attr
        self.tracetanker.save_var(tgt)
        self.tracetanker.save_attrs([attr])
        self.psetanalyer.save_ptoset(tgt, {tgt.value})
        self.cvaranalyer.prop_consvar(val, tgt)
        pset = self.psetanalyer.get_ptoset(val.name)
        # estr = repr(event) + '|' + ','.join(v.symstr + '->' +
        #                                     self.get_racyosig(v.symstr) for v in pset)
        values = [V2RO(v.symstr, self.get_racyosig(v.symstr)) for v in pset]
        inst = analysis.HasAttrCheckInst(tgt.name, val.name, attr, values)
        return self.new_analevent(event, inst)

    def visit_phifunction(self, event):
        tgt, test, val1, val2 = event.target, event.test, event.value1, event.value2
        if event.isvardef:
            self.tracetanker.save_var(tgt)
            self.psetanalyer.prop_ptoset(val1, tgt)
            self.psetanalyer.prop_ptoset(val2, tgt)
            inst = analysis.PhiFunctionInst(event.isvardef, test.name,
                                            tgt.name, val1.name, val2.name)
        else:
            inst = analysis.PhiFunctionInst(event.isvardef, test.name, tgt, val1, val2)
        return self.new_analevent(event, inst)

    def visit_branch(self, event):
        # if not self.cvaranalyer.check_consvar(event.test):
        if True:
            abranch = Branch(event)
            self.curraframe.push_branch(abranch)
            self.branchstack.append(abranch)
            if abranch.execbranch:
                stmts = self.curraframe.currstmt.orelse
            else:
                stmts = self.curraframe.currstmt.body
            extevents = ExtTraceMaker(self.curraframe).make(abranch, stmts) or []
            extevents.append(new.ExtBranchEnd(abranch))
            # It should be appended after including extended branch
            # We change the original value just for the logging for tracer.py
            event.resolved = abranch.resolved
            inst = analysis.BranchInst(abranch.bid, abranch.test.name,
                                       abranch.execbranch, abranch.resolved)
            brevent = self.new_analevent(abranch, inst)
            events = [brevent]
            for event in extevents:
                dbevent = self.visit(event)
                if isinstance(dbevent, list):
                    events.extend(dbevent)
                else:
                    events.append(dbevent)
            return events

    def visit_execbranchend(self, event):
        inst = analysis.ExecBranchEndInst(event.bid)
        events = [self.new_analevent(event, inst)]
        if self.branchstack and event.branch is self.branchstack[-1]:
            racyanalbranch = self.branchstack.pop()
            events.extend(self.merge_oattrvar(racyanalbranch))
            events.extend(self.merge_osig(racyanalbranch))
        return events

    def visit_extbranchend(self, event):
        if self.branchstack and event.branch is self.branchstack[-1]:
            # set the branch as the executed branch
            self.branchstack[-1].currbranch = 1
        inst = analysis.ExtBranchEndInst(event.bid)
        return self.new_analevent(event, inst)

    def merge_oattrvar(self, racyanalbranch):
        traces = []
        execbname2racyid = racyanalbranch.heap_execb_name2racyid
        extbname2racyid = racyanalbranch.heap_extb_name2racyid
        if racyanalbranch.resolved:
            execnames = set(execbname2racyid.iterkeys())
            extnames = set(extbname2racyid.iterkeys())
            commnames = execnames.intersection(extnames)
            subexecnames = execnames - commnames
            subextnames = extnames - commnames
            # for common names (appear in both exec and extend branches)
            for name in commnames:
                execid = execbname2racyid[name]
                extid = extbname2racyid[name]
                if racyanalbranch.execbranch:
                    phi = new.PhiFunction(self.new_heap_racyid(name, execid.value),
                                          execid, extid, racyanalbranch.test)
                else:
                    phi = new.PhiFunction(self.new_heap_racyid(name, execid.value),
                                          extid, execid, racyanalbranch.test)
                traces.append(self.visit(phi))
            for name in subexecnames:
                newid = execbname2racyid[name]
                oldid = self.get_heap_racyid(name)
                if oldid:
                    if racyanalbranch.execbranch:
                        phi = new.PhiFunction(self.new_heap_racyid(name, newid.value),
                                              newid, oldid, racyanalbranch.test)
                    else:
                        phi = new.PhiFunction(self.new_heap_racyid(name, newid.value),
                                              oldid, newid, racyanalbranch.test)
                    traces.append(self.visit(phi))
                else:
                    #TODO: here may introduce some problems, need to confirm in the future
                    # We just use a conservative way to decide whether we should include
                    # those outside undefined heap locations
                    if name not in self.predef_heapname and (not extnames):
                        self.update_heap_racyid(name, newid)
            for name in subextnames:
                newid = extbname2racyid[name]
                oldid = self.get_heap_racyid(name)
                if oldid:
                    if racyanalbranch.execbranch:
                        phi = new.PhiFunction(self.new_heap_racyid(name, oldid.value),
                                              oldid, newid, racyanalbranch.test)
                    else:
                        phi = new.PhiFunction(self.new_heap_racyid(name, oldid.value),
                                              newid, oldid, racyanalbranch.test)
                    traces.append(self.visit(phi))
        else:
            for name in execbname2racyid:
                self.update_heap_racyid(name, execbname2racyid[name])
        return traces

    def merge_osig(self, racyanalbranch):
        traces = []
        execbosig2racyosig = racyanalbranch.aset_execb_osig2racyosig
        extbosig2racyosig = racyanalbranch.aset_extb_osig2racyosig
        if racyanalbranch.resolved:
            execosigs = set(execbosig2racyosig.iterkeys())
            extosigs = set(extbosig2racyosig.iterkeys())
            commosigs = execosigs.intersection(extosigs)
            subexecosigs = execosigs - commosigs
            subextosigs = extosigs - commosigs
            # for common names (appear in both exec and extend branches)
            for osig in commosigs:
                execracyosig = execbosig2racyosig[osig]
                extracyosig = extbosig2racyosig[osig]
                if racyanalbranch.execbranch:
                    phi = new.PhiFunction(self.new_racyosig(osig),
                                          execracyosig, extracyosig, racyanalbranch.test, False)
                else:
                    phi = new.PhiFunction(self.new_racyosig(osig),
                                          extracyosig, execracyosig, racyanalbranch.test, False)
                traces.append(self.visit(phi))
            for osig in subexecosigs:
                newsig = execbosig2racyosig[osig]
                oldsig = self.get_racyosig(osig)
                # logger.info(oldsig)
                if oldsig:
                    if racyanalbranch.execbranch:
                        phi = new.PhiFunction(self.new_racyosig(osig),
                                              newsig, oldsig, racyanalbranch.test, False)
                    else:
                        phi = new.PhiFunction(self.new_racyosig(osig),
                                              oldsig, newsig, racyanalbranch.test, False)
                    traces.append(self.visit(phi))
                else:
                    self.update_racyosig(osig, newsig)
            for osig in subextosigs:
                newsig = extbosig2racyosig[osig]
                oldsig = self.get_racyosig(osig)
                if oldsig:
                    if racyanalbranch.execbranch:
                        phi = new.PhiFunction(self.new_racyosig(osig),
                                              oldsig, newsig, racyanalbranch.test, False)
                    else:
                        phi = new.PhiFunction(self.new_racyosig(osig),
                                              newsig, oldsig, racyanalbranch.test, False)
                    traces.append(self.visit(phi))
                else:
                    self.update_racyosig(osig, newsig)
        else:
            for osig in execbosig2racyosig:
                self.update_racyosig(osig, execbosig2racyosig[osig])
        return traces


class Branch(new.Branch):
    """
    aframe: the analysis frame this branch belongs to.
    branch: the branch event of this analysis branch.
    currbranch: signal of current branch, 0 as extbranch, 1 as execbranch
    ssatkr: all the ssa names stored in this branch.
    """
    def __init__(self, branch):
        self.__dict__.update(branch.__dict__)
        self.currbranch = 0
        self.local_execb_name2racyid = {}
        self.local_extb_name2racyid = {}
        self.heap_execb_name2racyid = {}
        self.heap_extb_name2racyid = {}
        self.aset_execb_osig2racyosig = {}
        self.aset_extb_osig2racyosig = {}

    def set_local_extb_name2racyid(self, res):
        self.local_extb_name2racyid = res

    def update_local_racyid(self, name, nid):
        if self.currbranch:
            self.local_execb_name2racyid[name] = nid
        else:
            self.local_extb_name2racyid[name] = nid

    def get_local_racyid(self, name):
        if self.currbranch:
            return self.local_execb_name2racyid.get(name)
        else:
            return self.local_extb_name2racyid.get(name)

    def exist_local_name(self, name):
        if self.currbranch:
            return name in self.local_execb_name2racyid
        else:
            return name in self.local_extb_name2racyid

    def update_heap_racyid(self, name, nid):
        if self.currbranch:
            self.heap_execb_name2racyid[name] = nid
        else:
            self.heap_extb_name2racyid[name] = nid

    def get_heap_racyid(self, name):
        if self.currbranch:
            return self.heap_execb_name2racyid.get(name)
        else:
            return self.heap_extb_name2racyid.get(name)

    def update_aset_osig(self, osig, racyosig):
        if self.currbranch:
            self.aset_execb_osig2racyosig[osig] = racyosig
        else:
            self.aset_extb_osig2racyosig[osig] = racyosig

    def get_aset_racyosig(self, osig):
        if self.currbranch:
            return self.aset_execb_osig2racyosig.get(osig)
        else:
            return self.aset_extb_osig2racyosig.get(osig)


