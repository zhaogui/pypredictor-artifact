#!/usr/bin/python
# _*_ coding:utf-8 _*_

import logging
import time
from dbengine import TraceTanker
from traceanalyzer import TraceAnalyzer
from tracematrixer import TraceMatrixer
from predengine import PredictiveEngine, TypeInconsError
from tracer import _trace


logger = logging.getLogger('pypredictor')


def predict(projname, scriptfile, entryfunc,
            ignores, recordfile,
            pkldir, outdir=''):
    try:
        with OTFPredictiveEngine(projname, outdir) as analyzer:
            _trace(scriptfile, entryfunc,
                   ignores, recordfile,
                   pkldir, analyzer)
    except TypeInconsError as e:
        logger.info(e.message)
        logger.info(':)\n')


class OTFPredictiveEngine(object):
    def __init__(self, projname, outdir):
        self.tracetanker = TraceTanker()
        self.traceanalyzer = TraceAnalyzer(self.tracetanker)
        self.matrixer = TraceMatrixer(self.tracetanker)
        self.predegine = PredictiveEngine(self.tracetanker, True, True)
        self._starttime = 0
        self._stoptime = 0

    def flowthrough(self, why, aframe, events):
        aevents = self.traceanalyzer.flowthrough(why, aframe, events)
        for event in aevents:
            self.tracetanker.save_event(event)
            self.predegine.flowthrough(event)

    def starttiming(self):
        self._starttime = time.time()

    def stoptiming(self):
        self._stoptime = time.time()

    @property
    def elapsedtime(self):
        return self._stoptime - self._starttime

    def matrixtime(self):
        logger.info('All Elapsed Time: {}'.format(self.elapsedtime))

    def __enter__(self):
        return self

    def __exit__(self, ex_type, *_):
        if ex_type == TypeInconsError:
            self.matrixtime()
            self.matrixer.run()