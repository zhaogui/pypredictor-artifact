#!/usr/bin/python
# _*_ coding:utf-8 _*_

from collections import namedtuple

from pypredictor.tracenodes import TraceType


class EventMaker(object):
    gid = 1L

    @classmethod
    def make(cls, **kwargs):
        return AnalysisEvent(gid=cls.newgid(), **kwargs)

    @classmethod
    def newgid(cls):
        gid = cls.gid
        cls.gid += 1
        return gid


class AnalysisEvent(object):

    def __init__(self, gid, thid, sid, inst, type, ext):
        self.gid = gid
        self.thid = thid
        self.sid = sid
        self.inst = inst
        self.type = type
        self.ext = ext

    def getdefs(self):
        return self.inst.getdefs()

    def getuses(self):
        return self.inst.getuses()

    def getctrls(self):
        return getattr(self, '_ctrls', [])

    def setctrls(self, ctrls):
        setattr(self, '_ctrls', ctrls)

    def geteventsig(self):
        return TraceType.geteventsig(self.type)

    @property
    def isbranch(self):
        return TraceType.BRANCH == self.type
    
    def __getitem__(self, index):
        return getattr(self, index)
    
    def __iter__(self):
        return iter(self.to_dict())
    
    def __hash__(self):
        return hash(self.gid)

    def __eq__(self, other):
        return self.gid == other.gid

    def __repr__(self):
        # return '{{{0}}}'.format(', '.join(repr(k) + ': ' + str(v)
        #                                   for k, v in self.to_dict().iteritems()))
        return '{' + ', '.join([repr('gid') + ':' + str(self.gid),
                                repr('inst') + ':' + str(self.inst),
                                repr('ext') + ':' + str(self.ext),
                                repr('sid') + ':' + str(self.sid),
                                repr('type') + ':' + str(self.type),
                                repr('thid') + ':' + str(self.thid),
                                ]) + '}'

    __str__ = __repr__

    def to_dict(self):
        return {'gid': self.gid, 'thid': self.thid, 'sid': self.sid,
                'inst': self.inst, 'type': self.type, 'ext': int(self.ext)}


Value2ROsig = namedtuple('Value2ROsig', ['value', 'rosig'])


class Inst(object):

    def getdefs(self):
        return []

    def getuses(self):
        return []

    def __str__(self):
        return str(self.__dict__)


class AllocateInst(Inst):
    def __init__(self, target, value):
        self.target = target
        self.value = value

    def getdefs(self):
        return [self.target, self.value.rosig]

    def __str__(self):
        return '{tgt}={val},{rosig}'.format(tgt=self.target,
                                            val=self.value.value,
                                            rosig=self.value.rosig)


class InfiniteRelaxAllocateInst(Inst):
    def __init__(self, target, value):
        self.target = target
        self.value = value

    def getdefs(self):
        return [self.target, self.value.rosig]

    def __str__(self):
        return '{tgt}={val},{rosig}'.format(tgt=self.target,
                                            val=self.value.value,
                                            rosig=self.value.rosig)


class FiniteRelaxAllocateInst(Inst):
    def __init__(self, target, values):
        self.target = target
        self.values = values

    def getdefs(self):
        defs = [self.target]
        defs.extend(val.rosig for val in self.values)
        return defs

    def __str__(self):
        return '{tgt}={vals}'.format(tgt=self.target,
                                     vals='|'.join((v.value + ',' + v.rosig)
                                                   for v in self.values))


class AssignInst(Inst):
    def __init__(self, target, value):
        self.target = target
        self.value = value

    def getdefs(self):
        return [self.target]

    def getuses(self):
        return [self.value]

    def __str__(self):
        return '{tgt}={val}'.format(tgt=self.target, val=self.value)


class AttrLoadInst(Inst):
    # y=x.f|x=o,o1?y1=o.f|...
    def __init__(self, target, basevar, attr, base2value):
        self.target = target
        self.basevar = basevar
        self.attr = attr
        # dict of (baseval2rosig -> value)
        self.base2value = base2value

    def getdefs(self):
        return [self.target]

    def getuses(self):
        uses = [self.basevar]
        uses.extend(baseval.rosig for baseval in self.base2value.iterkeys())
        uses.extend(val for val in self.base2value.itervalues() if val != '')
        return uses

    @property
    def receivers(self):
        return self.base2value.keys()

    def __str__(self):
        basepart = '{tgt}={val}.{attr}'.format(tgt=self.target,
                                               val=self.basevar,
                                               attr=self.attr)
        extendpart = '|'.join('{base}={bval},{rosig}?'
                              '{tgt}={oattr}'.format(base=self.basevar,
                                                     bval=base.value,
                                                     rosig=base.rosig,
                                                     tgt=self.target,
                                                     oattr=oattr)
                                                     for base, oattr in self.base2value.iteritems())
        return '{base}|{ext}'.format(base=basepart, ext=extendpart)


class AttrStoreInst(Inst):
    # y.f=x|y=o?o.f2=x:o.f1,o1->o2|...
    def __init__(self, basevar,
                 attr, value,
                 base2target,
                 base2preval,
                 base2newosig):
        self.basevar = basevar
        self.attr = attr
        self.value = value
        # dict of ((basevsig, baserosig) -> target|preval|newosig)
        self.base2target = base2target  # new heap location
        self.base2preval = base2preval  # old racy heap location
        self.base2newosig = base2newosig  # new racy object signature

    def getdefs(self):
        defs = [tgt for tgt in self.base2target.itervalues()]
        defs.extend(osig for osig in self.base2newosig.itervalues())
        return defs

    def getuses(self):
        uses = [self.basevar, self.value]
        uses.extend(preval for preval in self.base2preval.itervalues()
                    if preval != '')
        uses.extend(basevar.rosig for basevar in self.base2target.iterkeys())
        return uses

    @property
    def receivers(self):
        return self.base2target.keys()

    def __str__(self):
        basepart = '{tgt}.{attr}={val}'.format(tgt=self.basevar,
                                               attr=self.attr,
                                               val=self.value)
        items = zip(self.base2target.keys(), self.base2target.values(),
                    self.base2preval.values(), self.base2newosig.values())
        extpart = '|'.join('{bvar}={bval}?' \
                           '{oattr}={val}:{orioattr},' \
                           '{osig1}->{osig2}'.format(bvar=self.basevar, bval=bval,
                                                     oattr=oattr, val=self.value,
                                                     orioattr=orioattr, osig1=oldosig,
                                                     osig2=newosig)
                                                     for (bval, oldosig), oattr,
                                                         orioattr, newosig in items)
        return '{base}|{ext}'.format(base=basepart, ext=extpart)


class SubLoadInst(Inst):
    # x=y[0]|y=o,o1?y1=o_0_1|...
    def __init__(self, target, basevar, index, base2value):
        self.basevar = basevar
        self.target = target
        self.index = index
        self.base2value = base2value

    def getdefs(self):
        return [self.target]

    def getuses(self):
        uses = [self.basevar]
        uses.extend(val for val in self.base2value.itervalues() if val != '')
        uses.extend(basevar.rosig for basevar in self.base2value.iterkeys())
        return uses

    @property
    def receivers(self):
        return self.base2value.keys()

    def __str__(self):
        basepart = '{tgt}={val}[{idx}]'.format(tgt=self.target,
                                               val=self.basevar,
                                               idx=self.index)
        extendpart = '|'.join('{base}={bval},{rosig}?'
                              '{tgt}={oattr}'.format(base=self.basevar, bval=base.value,
                                                     rosig=base.rosig, tgt=self.target,
                                                     oattr=oattr)
                                                     for base, oattr in self.base2value.iteritems())
        return '{base}|{ext}'.format(base=basepart, ext=extendpart)


class SubStoreInst(Inst):
    # x[0]=y|x=o,o1?o_0_2=y:o_0_1|...s
    def __init__(self, basevar, index, value, base2target, base2preval):
        self.basevar = basevar
        self.index = index
        self.value = value
        self.base2target = base2target
        self.base2preval = base2preval

    def getdefs(self):
        return [tgt for tgt in self.base2target.itervalues()]

    def getuses(self):
        uses = [self.basevar, self.value]
        uses.extend(preval for preval in self.base2preval.itervalues()
                    if preval != '')
        uses.extend(basevar.rosig for basevar in self.base2target.iterkeys())
        return uses

    @property
    def receivers(self):
        return self.base2target.keys()

    def __str__(self):
        basepart = '{tgt}[{idx}]={val}'.format(tgt=self.basevar,
                                               idx=self.index,
                                               val=self.value)
        items = zip(self.base2target.keys(), self.base2target.values(),
                    self.base2preval.values())
        extpart = '|'.join('{bvar}={bval},{rosig}?' \
                           '{oattr}={val}:{orioattr}'.format(bvar=self.basevar, bval=bval, rosig=rosig,
                                                             oattr=oattr, val=self.value, orioattr=orioattr)
                                                       for (bval, rosig), oattr, orioattr in items)
        return '{base}|{ext}'.format(base=basepart, ext=extpart)


class BopAssignInst(Inst):
    def __init__(self, target, value1, value2, op):
        self.target = target
        self.value1 = value1
        self.value2 = value2
        self.op = op

    def getdefs(self):
        return [self.target]

    def getuses(self):
        return [self.value1, self.value2]

    def __str__(self):
        return '{tgt}={v1} {op} {v2}'.format(tgt=self.target,
                                             v1=self.value1,
                                             op=self.op,
                                             v2=self.value2)


class UopAssignInst(Inst):
    def __init__(self, target, value, op):
        self.target = target
        self.value = value
        self.op = op

    def getdefs(self):
        return [self.target]

    def getuses(self):
        return [self.value]

    def __str__(self):
        return '{tgt}={op} {val}'.format(tgt=self.target,
                                         op=self.op,
                                         val=self.value)


class MethodInvokeInst(Inst):
    def __init__(self, base, attr, basevalues):
        self.basevar = base
        self.attr = attr
        # (same_func, Value2ROsig)
        self.basevalues = basevalues

    def getuses(self):
        uses = [self.basevar]
        uses.extend(base.rosig for _, base in self.basevalues)
        return uses

    @property
    def receivers(self):
        return self.basevalues

    def __str__(self):
        basepart = 'call {rec}.{attr}'.format(rec=self.basevar, attr=self.attr)
        extpart = '|'.join('{isvardef},{val},{rosig}'.format(isvardef=isvardef, val=val, rosig=rosig)
                                                             for isvardef, (val, rosig) in self.basevalues)
        return '{base}|{ext}'.format(base=basepart, ext=extpart)


class FunctionCallInst(Inst):
    def __init__(self, func, funcvalues):
        self.func = func
        self.funcvalues = funcvalues

    def getuses(self):
        uses = [self.func]
        uses.extend(base.rosig for base in self.funcvalues)
        return uses

    def __str__(self):
        basepart = 'call {func}'.format(func=self.func)
        extpart = '|'.join('{val},{rosig}'.format(val=val, rosig=rosig)
                                                for val, rosig in self.funcvalues)
        return '{base}|{ext}'.format(base=basepart, ext=extpart)


class TypeCheckInst(Inst):
    def __init__(self, target, value, type):
        self.target = target
        self.value = value
        self.type = type

    def getdefs(self):
        return [self.target]

    def getuses(self):
        return [self.value, self.type]

    def __str__(self):
        return '{tgt}={val} {type}'.format(tgt=self.target,
                                           val=self.value,
                                           type=self.type)


class SubTypeCheckInst(Inst):
    def __init__(self, target, value, type):
        self.target = target
        self.value = value
        self.type = type

    def getdefs(self):
        return [self.target]

    def getuses(self):
        return [self.value, self.type]

    def __str__(self):
        return '{tgt}={val} {type}'.format(tgt=self.target,
                                           val=self.value,
                                           type=self.type)


class HasAttrCheckInst(Inst):
    def __init__(self, target, valuevar, attr, values):
        self.target = target
        self.valuevar = valuevar
        self.attr = attr
        self.values = values

    def getdefs(self):
        return [self.target]

    def getuses(self):
        uses = [self.valuevar]
        uses.extend(val.rosig for val in self.values)
        return uses

    def __str__(self):
        base = '{tgt}={attr} {val}'.format(tgt=self.target,
                                           attr=repr(self.attr),
                                           val=self.valuevar)
        ext = ','.join('{val}->{rosig}'.format(val=val, rosig=rosig)
                       for val, rosig in self.values)
        return '{base}|{ext}'.format(base=base, ext=ext)


class PhiFunctionInst(Inst):
    def __init__(self, isvardef, test, target, tvalue, fvalue):
        self.isvardef = isvardef
        self.test = test
        self.target = target
        self.tvalue = tvalue
        self.fvalue = fvalue

    def getdefs(self):
        return [self.target]

    def getuses(self):
        return [self.tvalue, self.fvalue]

    def __str__(self):
        return '{isvardef}|{test}?{tgt}={val1}:' \
               '{val2}'.format(isvardef=int(self.isvardef),
                               test=self.test if self.test else '',
                               tgt=self.target,
                               val1=self.tvalue,
                               val2=self.fvalue)


class BranchInst(Inst):
    def __init__(self, bid, test, execbranch, resolved):
        self.bid = bid
        self.test = test
        self.execbranch = execbranch
        self.resolved = resolved
        self.feasible = False

    def getuses(self):
        return [self.test]

    def __str__(self):
        return 'Branch<{bid}>|{test}|' \
               '{ebrch}|{res}'.format(bid=self.bid,
                                      test=self.test,
                                      ebrch=int(self.execbranch),
                                      res=int(self.resolved))


class ExtBranchEndInst(Inst):
    def __init__(self, bid):
        self.bid = bid

    def __str__(self):
        return 'ExtBranchEnd<' + str(self.bid) + '>'


class ExecBranchEndInst(Inst):
    def __init__(self, bid):
        self.bid = bid

    def __str__(self):
        return 'ExecBranchEnd<' + str(self.bid) + '>'
