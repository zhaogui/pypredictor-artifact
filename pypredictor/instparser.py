#!/usr/bin/python
# _*_ coding:utf-8 _*_

from analysisnodes import *


class InstParser:
    @classmethod
    def parse(cls, type, inst):
        return getattr(cls, 'parse_' + TraceType.geteventsig(type).lower())(inst)

    @staticmethod
    def parse_allocate(inst):
        # y=1,o1_1
        tgt, val = inst.split('=')
        val, racyosig = val.split(',')
        return AllocateInst(tgt, Value2ROsig(val, racyosig))

    @staticmethod
    def parse_infiniterelaxallocate(inst):
        # y=o1,o1_1
        tgt, val = inst.split('=')
        val, racyosig = val.split(',')
        return AllocateInst(tgt, Value2ROsig(val, racyosig))

    @staticmethod
    def parse_finiterelaxallocate(inst):
        # y=o1,o1_1|2,o2_1|...
        tgt, val = inst.split('=')
        values = []
        for item in val.split('|'):
            valsig1, racyosig = item.split(',')
            values.append(Value2ROsig(valsig1, racyosig))
        return FiniteRelaxAllocateInst(tgt, values)

    @staticmethod
    def parse_assign(inst):
        # y=x
        tgt, val = inst.split('=')
        return AssignInst(tgt, val)

    @staticmethod
    def parse_attrload(inst):
        # y=x.f|x=o,o1?y1=o.f|...
        assigns = inst.split('|')
        baseassign, hpassigns = assigns[0], assigns[1:]
        basevar, attr = baseassign.split('=')[1].split('.')
        base2value = {}
        for hpa in hpassigns:
            condi, assign = hpa.split('?')
            condi_left, condi_right = condi.split('=')
            condi_right_vsig, condi_right_racyosig = condi_right.split(',')
            if assign != '':
                ass_left, ass_right = assign.split('=')
                base2value[Value2ROsig(condi_right_vsig, condi_right_racyosig)] = ass_right
            else:
                base2value[Value2ROsig(condi_right_vsig, condi_right_racyosig)] = ''
        return AttrLoadInst(baseassign.split('=')[0], basevar, attr, base2value)

    @staticmethod
    def parse_attrstore(inst):
        # y.f=x|y=o?o.f2=x:o.f1,o1->o2|...
        assigns = inst.split('|')
        baseassign, hpassigns = assigns[0], assigns[1:]
        basevar, attr = baseassign.split('=')[0].split('.')
        value = baseassign.split('=')[1]
        base2target = {}
        base2preval = {}
        base2newosig = {}
        for hpa in hpassigns:
            condi, assign = hpa.split('?')
            condi_left, condi_right = condi.split('=')
            varassign, osigassign = assign.split(',')
            ass_left, ass_right = varassign.split('=')
            tass_right, fass_right = ass_right.split(':')
            oldosig, newosig = osigassign.split('->')

            basevalosig = Value2ROsig(condi_right, oldosig)
            base2target[basevalosig] = ass_left
            base2preval[basevalosig] = fass_right
            base2newosig[basevalosig] = newosig
        return AttrStoreInst(basevar, attr, value, base2target, base2preval, base2newosig)

    @staticmethod
    def parse_subload(inst):
        # x=y[0]|y=o,o1?y1=o_0_1|...
        assigns = inst.split('|')
        baseassign, hpassigns = assigns[0], assigns[1:]
        basevar, index = baseassign.split('=')[1].split('[')
        index = index[0:-1]
        base2value = {}
        for hpa in hpassigns:
            condi, assign = hpa.split('?')
            condi_left, condi_right = condi.split('=')
            condi_right_vsig, condi_right_racyosig = condi_right.split(',')
            if assign != '':
                ass_left, ass_right = assign.split('=')
                base2value[Value2ROsig(condi_right_vsig, condi_right_racyosig)] = ass_right
            else:
                base2value[Value2ROsig(condi_right_vsig, condi_right_racyosig)] = ''
        return SubLoadInst(baseassign.split('=')[0], basevar, index, base2value)

    @staticmethod
    def parse_substore(inst):
        # x[0]=y|x=o,o1?o_0_2=y:o_0_1|...
        assigns = inst.split('|')
        baseassign, hpassigns = assigns[0], assigns[1:]
        basevar, index = baseassign.split('=')[0].split('[')
        index = index[0:-1]
        value = baseassign.split('=')[1]
        base2target = {}
        base2preval = {}
        for hpa in hpassigns:
            condi, assign = hpa.split('?')
            condi_left, condi_right = condi.split('=')
            condi_right_vsig, condi_right_racyosig = condi_right.split(',')
            ass_left, ass_right = assign.split('=')
            tass_right, fass_right = ass_right.split(':')

            basevalosig = Value2ROsig(condi_right_vsig, condi_right_racyosig)
            base2target[basevalosig] = ass_left
            base2preval[basevalosig] = fass_right
        return SubStoreInst(basevar, index, value, base2target, base2preval)

    @staticmethod
    def parse_bopassign(inst):
        # z=x+y or z = x>y
        target, right = inst.split('=')
        value1, op, value2 = right.split(' ')
        return BopAssignInst(target, value1, value2, op)

    @staticmethod
    def parse_uopassign(inst):
        # y=-x or y=not x
        target, right = inst.split('=')
        op, value = right.split(' ')
        return UopAssignInst(target, value, op)

    @staticmethod
    def parse_typeassume(inst):
        # t=x int
        target, right = inst.split('=')
        value, typevar = right.split(' ')
        return TypeCheckInst(target, value, typevar)

    @staticmethod
    def parse_subtypeassume(inst):
        # t=x int
        target, right = inst.split('=')
        value, typevar = right.split(' ')
        return SubTypeCheckInst(target, value, typevar)

    @staticmethod
    def parse_hasattrassume(inst):
        #t='f' x|o1->o1_1, o2->o2_1...
        target, right = inst.split('=')
        attr_val, oset = right.split('|')
        oset = oset.split(',')
        attr, value = attr_val.split(' ')
        attr = attr.lstrip("'").rstrip("'")
        values = []
        for o in oset:
            osig, racyosig = o.split('->')
            values.append(Value2ROsig(osig, racyosig))
        return HasAttrCheckInst(target, value, attr, values)

    @staticmethod
    def parse_methodinvoke(inst):
        # call x.f|o1,o1_1|o2,o2_2|...
        tmpholder = inst.split('|')
        base, attr = tmpholder[0].replace('call ', '', 1).split('.')
        values = []
        for item in tmpholder[1:]:
            elts = item.split(',')
            if len(elts) == 3:
                flag, osym, rosig = elts
            elif len(elts) == 2:
                flag, (osym, rosig) = 1, elts
            else:
                flag, osym, rosig = 0, None, None
            values.append((int(flag), Value2ROsig(osym, rosig)))

        return MethodInvokeInst(base, attr, values)

    @staticmethod
    def parse_functioncall(inst):
        #call f|1,o1,o1_1,|0,o2,o2_2...
        tmpholder = inst.split('|')
        func = tmpholder[0].replace('call ', '', 1)
        values = []
        for item in tmpholder[1:]:
            osym, rosig = item.split(',')
            values.append(Value2ROsig(osym, rosig))
        return FunctionCallInst(func, values)

    @staticmethod
    def parse_phifunction(inst):
        # 1|x?x3=x1:x2 or 0|o_3=o_1:o_2
        vardef, right = inst.split('|')
        condi, assign = right.split('?')
        ass_left, ass_right = assign.split('=')
        tass_right, fass_right = ass_right.split(':')
        return PhiFunctionInst(vardef, condi, ass_left, tass_right, fass_right)

    @staticmethod
    def parse_branch(inst):
        # Branch<1>
        bbase, test, execb, resolved = inst.split('|')
        bid = int(bbase.lstrip('Branch')[1:-1])
        return BranchInst(bid, test, int(execb), int(resolved))

    @staticmethod
    def parse_extbranchend(inst):
        # ExtBranchEnd<1>
        branchsig = inst.lstrip('ExtBranchEnd')
        bid = int(branchsig[1:-1])
        return ExtBranchEndInst(bid)

    @staticmethod
    def parse_execbranchend(inst):
        # ExecBranchEnd<1>
        branchsig = inst.lstrip('ExexBranchEnd')
        bid = int(branchsig[1:-1])
        return ExecBranchEndInst(bid)



