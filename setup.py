import os
from setuptools import setup, find_packages

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pypredictor",
    version = "0.0.1",
    author = "Zhaogui Xu",
    author_email = "xzhg71107214@gmail.com",
    description = ("A predictive analysis tool for detecting Python bugs"),
    license = "Apache 2.0",
    keywords = "Python predictive bug",
    url = "https://bitbucket.org/zhaogui/pypredictor-artifact",
    packages=['pypredictor', 'pypredictor.runs', 'pypredictor.runs.logger'],
    package_data={'pypredictor.runs.logger':['*']},
    long_description=read('README'),
    classifiers=[
        "Development Status :: 1 - Alpha",
        "Topic :: Python analysis",
        "License :: OSI Approved :: Apache License Version 2.0",
    ],
    install_requires=[
        "pyyaml", "astunparse"
    ],
)
